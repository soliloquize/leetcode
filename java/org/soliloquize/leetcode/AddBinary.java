package org.soliloquize.leetcode;

import java.util.Arrays;

public class AddBinary {
    /**
     http://leetcode.com/oldoj#question_67

     Given two binary strings, return their sum (also a binary string).

     For example,
     a = "11"
     b = "1"
     Return "100".
     */

    public static void main(String[] args) {
        AddBinary s = new AddBinary();
        System.out.println(s.addBinary("11", "1"));
        System.out.println(s.addBinary2("11", "1"));
    }

    public String addBinary2(String a, String b) {
        int length = Math.max(a.length(), b.length());
        int[] ac = new int[length];
        int[] bc = new int[length];
        int[] rc = new int[length];
        int carry = 0;

        for (int i = a.length()-1, j = ac.length-1; i >= 0; i--, j--) {
            ac[j] = a.charAt(i) - '0';
        }

        for (int i = b.length()-1, j = bc.length-1; i >= 0; i--, j--) {
            bc[j] = b.charAt(i) - '0';
        }

        for (int i = length - 1; i >= 0; i--) {
            int tmp = ac[i] + bc[i] + carry;
            rc[i] = tmp % 2;
            carry = tmp /= 2;
        }

        StringBuilder builder = new StringBuilder();
        if (carry > 0) {
            builder.append('1');
        }
        for (int i = 0; i < length; i++) {
            builder.append((char)('0' + rc[i]));
        }
        return builder.toString();
    }

    public String addBinary(String a, String b) {
        char[] ac = a.toCharArray();
        char[] bc = b.toCharArray();
        char[] rc = new char[ac.length + bc.length + 1];
        Arrays.fill(rc, '0');

        int i = ac.length - 1;
        int j = bc.length - 1;
        int k = rc.length - 1;
        int carry = 0;
        for (; i >= 0 && j >= 0 && k >= 0; i--, j--, k--) {
            int s = ac[i] - '0' + bc[j] - '0' + carry;
            rc[k] = (char) ('0' + s % 2);
            carry = s / 2;
        }

        for (; i >= 0; i--, k--) {
            int s = ac[i] - '0' + carry;
            rc[k] = (char) ('0' + s % 2);
            carry = s / 2;
        }

        for (; j >= 0; j--, k--) {
            int s = bc[j] - '0' + carry;
            rc[k] = (char) ('0' + s % 2);
            carry = s / 2;
        }

        if (carry > 0) {
            rc[k] = (char) (carry + '0');
        }

        StringBuilder builder = new StringBuilder();
        boolean start = false;
        for (int p = 0; p < rc.length; p++) {
            if (rc[p] == '1') {
                start = true;
            }
            if (start) {
                builder.append(rc[p]);
            }
        }
        return builder.toString().length() > 0 ? builder.toString() : "0";
    }
}
