package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class AddTwoNumbers {

    /**
     http://leetcode.com/onlinejudge#question_2

     You are given two linked lists representing two non-negative numbers.
     The digits are stored in reverse order and each of their nodes contain a single digit.
     Add the two numbers and return it as a linked list.

     Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
     Output: 7 -> 0 -> 8
     */

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode a = l1;
        ListNode b = l2;
        ListNode res = new ListNode(0);
        ListNode head = res;
        int carry = 0;
        while (a != null && b != null) {
            int tmp = a.val + b.val + carry;
            res.next = new ListNode(tmp % 10);
            res = res.next;
            carry = tmp / 10;
            a = a.next;
            b = b.next;
        }
        while (a != null) {
            int tmp = a.val + carry;
            res.next = new ListNode(tmp % 10);
            res = res.next;
            carry = tmp /10;
            a = a.next;
        }
        while (b != null) {
            int tmp = b.val + carry;
            res.next = new ListNode(tmp % 10);
            res = res.next;
            carry = tmp /10;
            b = b.next;
        }
        if (carry != 0) {
            res.next = new ListNode(carry);
        }
        return head.next;
    }
}
