package org.soliloquize.leetcode;

import java.util.*;

public class Anagrams {

    /**
     http://leetcode.com/onlinejudge#question_49

     Given an array of strings, return all groups of strings that are anagrams.

     Note: All inputs will be in lower-case.
     */
    public ArrayList<String> anagrams2(String[] strs) {
        Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = new String(chars);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<String>());
            }
            map.get(key).add(str);
        }
        ArrayList<String> res = new ArrayList<String>();
        for (ArrayList<String> item : map.values()) {
            if (item.size() > 1) {
                res.addAll(item);
            }
        }
        return res;
    }


    public ArrayList<String> anagrams(String[] strs) {
        Map<String, ArrayList<String>> map = new HashMap<String, ArrayList<String>>();
        for (String str : strs) {
            List<Character> charList = new ArrayList<Character>(str.length());
            for (int i = 0; i < str.length(); i++) {
                charList.add(str.charAt(i));
            }
            Collections.sort(charList);
            StringBuilder builder = new StringBuilder();
            for (char c : charList) {
                builder.append(c);
            }
            String key = builder.toString();
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<String>());
            }
            map.get(key).add(str);
        }

        ArrayList<String> result = new ArrayList<String>();
        for (ArrayList<String> item : map.values()) {
            if (item.size() > 1) {
                result.addAll(item);
            }
        }
        return result;
    }
}
