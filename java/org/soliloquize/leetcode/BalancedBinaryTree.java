package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class BalancedBinaryTree {

    /**
     http://leetcode.com/onlinejudge#question_110

     Given a binary tree, determine if it is height-balanced.

     For this problem, a height-balanced binary tree is defined as a binary tree
     in which the depth of the two subtrees of every node never differ by more than 1.
     */

    public static void main(String[] args) {
        TreeNode root = new ConvertSortedArrayToBinarySearchTree().sortedArrayToBST(new int[] {1, 2, 3, 4, 5, 6, 7});
        BalancedBinaryTree s = new BalancedBinaryTree();

        TreeNode root1 = new TreeNode(1);
        root1.left = new TreeNode(1);
        root1.left.left = new TreeNode(1);
        System.out.println(s.isBalanced(root));
        System.out.println(s.isBalanced(root1));
        System.out.println(s.isBalanced2(root));
        System.out.println(s.isBalanced2(root1));
    }

    public boolean isBalanced2(TreeNode root) {
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            root.val = 1;
            return true;
        }
        int left = 1;
        int right = 1;
        if (root.left != null) {
            boolean flag = isBalanced2(root.left);
            if (!flag) {
                return false;
            }
            left = 1 + root.left.val;
        }
        if (root.right != null) {
            boolean flag = isBalanced2(root.right);
            if (!flag) {
                return false;
            }
            right = 1 + root.right.val;
        }
        if (Math.abs(right - left) > 1) {
            return false;
        }
        root.val = Math.max(right, left);
        return true;
    }


    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }

        if (root.left == null && root.right == null) {
            root.val = 1;
        } else if (root.left != null && root.right == null) {
            boolean flag = isBalanced(root.left);
            if (!flag) {
                return flag;
            }
            if (root.left.val > 1) {
                return false;
            }
            root.val = 1 + root.left.val;
        } else if (root.right != null && root.left == null) {
            boolean flag = isBalanced(root.right);
            if (!flag) {
                return flag;
            }
            if (root.right.val > 1) {
                return false;
            }
            root.val = 1 + root.right.val;
        } else {
            boolean flag = isBalanced(root.left);
            if (!flag) {
                return flag;
            }
            flag = isBalanced(root.right);
            if (!flag) {
                return flag;
            }
            if (Math.abs(root.left.val - root.right.val) > 1) {
                return false;
            }
            root.val = 1 + Math.max(root.left.val, root.right.val);
        }
        return true;
    }
}
