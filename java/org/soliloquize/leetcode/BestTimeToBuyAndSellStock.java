package org.soliloquize.leetcode;

public class BestTimeToBuyAndSellStock {
    /**
     http://leetcode.com/onlinejudge#question_121

     Say you have an array for which the ith element is the price of a given stock on day i.

     If you were only permitted to complete at most one transaction (ie,
     buy one and sell one share of the stock), design an algorithm to find the maximum profit.
     */
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }

        int profit = Integer.MIN_VALUE;
        int min = prices[0];
        int minPos = 0;
        for (int i = 0; i < prices.length; i++) {
            // 峰值拐点
            if (i > 0 && prices[i] < prices[i-1] && (i - 1 > minPos)) {
                int diff = prices[i-1] - min;
                profit = profit < diff ? diff : profit;
            }
            // 最后一天
            if (i == prices.length - 1) {
                int diff = prices[i] - min;
                profit = profit < diff ? diff : profit;
            }
            // 记录最低点
            if (min > prices[i]) {
                min = prices[i];
                minPos = i;
            }
        }

        return profit > 0 ? profit : 0;
    }
}
