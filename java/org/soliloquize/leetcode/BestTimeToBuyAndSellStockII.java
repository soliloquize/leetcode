package org.soliloquize.leetcode;

public class BestTimeToBuyAndSellStockII {

    /**
     http://leetcode.com/onlinejudge#question_122

     Say you have an array for which the ith element is the price of a given stock on day i.

     Design an algorithm to find the maximum profit. You may complete as many transactions as you like (ie, buy one and sell one share of the stock multiple times).
     However, you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
     */
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int total = 0;
        // 记录变化状态，上涨下跌
        boolean status = false;
        // 记录买入时候的低价
        int min = Integer.MIN_VALUE;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i-1]) {
                if (status == false) {
                    min = prices[i-1];
                    status = true;
                }
            }
            else if (prices[i] < prices[i-1]) {
                if (status == true) {
                    total += prices[i-1] - min;
                    status = false;
                    min = Integer.MIN_VALUE;
                }
            }
            if (i == prices.length - 1) {
                if (min != Integer.MIN_VALUE && prices[i] > min) {
                    total += prices[i] - min;
                }
            }
        }
        return total;
    }
}
