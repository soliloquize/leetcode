package org.soliloquize.leetcode;

public class BestTimeToBuyAndSellStockIII {
    /**
     http://oj.leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/

     Say you have an array for which the ith element is the price of a given stock on day i.

     Design an algorithm to find the maximum profit. You may complete at most two transactions.

     Note:
     You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
     */
    public int maxProfit(int[] prices) {
        if (prices.length == 0) {
            return 0;
        }
        int [] x = profit(prices);
        int [] y = reverseProfit(prices);

        int max = Integer.MIN_VALUE;
        for (int i = 0; i < prices.length; i++) {
            if (i == 0) {
                max = Math.max(x[i], max);
            } else if (i == prices.length - 1) {
                max = Math.max(y[i], max);
            } else {
                max = Math.max(x[i] + y[i-1], max);
            }
        }
        return max;
    }

    private int[] profit(int[] prices) {
        int[] max = new int[prices.length];
        int current = Integer.MIN_VALUE;
        for (int i = prices.length-1; i >= 0; i--) {
            current = Math.max(current, prices[i]);
            max[i] = current;
        }

        int[] p = new int[prices.length];
        for (int i = 0; i < prices.length; i++) {
            p[i] = Math.max(0, max[i] - prices[i]);
        }

        int[] x = new int[prices.length];
        current = Integer.MIN_VALUE;
        for (int i = prices.length-1; i >= 0; i--) {
            current = Math.max(current, p[i]);
            x[i] = current;
        }
        return x;
    }

    private int[] reverseProfit(int [] prices) {
        int[] min = new int[prices.length];
        int current = Integer.MAX_VALUE;
        for (int i = 0; i < prices.length; i++) {
            current = Math.min(current, prices[i]);
            min[i] = current;
        }

        int[] q = new int[prices.length];
        for (int i = 0; i < prices.length; i++) {
            q[i] = Math.max(0, prices[i] - min[i]);
        }

        int[] y = new int[prices.length];
        current = Integer.MIN_VALUE;
        for (int i = 0; i < prices.length; i++) {
            current = Math.max(current, q[i]);
            y[i] = current;
        }
        return y;
    }

    public static void main(String[] args) {
        BestTimeToBuyAndSellStockIII s = new BestTimeToBuyAndSellStockIII();
        int p = s.maxProfit(new int [] {1, 2, 1, 2, 4, 3});
        System.out.println(p);
    }
}
