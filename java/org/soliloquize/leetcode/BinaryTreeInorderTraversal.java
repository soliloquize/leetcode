package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.Constructor;
import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class BinaryTreeInorderTraversal {
    /**
     http://leetcode.com/onlinejudge#question_94

     Given a binary tree, return the inorder traversal of its nodes' values.

     For example:
     Given binary tree {1,#,2,3},

     1
     \
     2
     /
     3
     return [1,3,2].

     Note: Recursive solution is trivial, could you do it iteratively?

     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
     */

    public static void main(String[] args) {
        BinaryTreeInorderTraversal s = new BinaryTreeInorderTraversal();
        System.out.println(s.inorderTraversal(Constructor.buildTree(new int[] {1, Integer.MIN_VALUE, 2, Integer.MIN_VALUE, 3})));
    }


    public ArrayList<Integer> inorderTraversal(TreeNode root) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        TreeNode node = root;
        while (node != null) {
            stack.push(node);
            if (node.left != null) {
                node = node.left;
            } else {
                while (!stack.isEmpty()) {
                    TreeNode top = stack.pop();
                    result.add(top.val);
                    node = top.right;
                    if (node != null) {
                        break;
                    }
                }
            }
        }
        return result;
    }
}
