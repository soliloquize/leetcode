package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class BinaryTreeLevelOrderTraversal {

    /**
     http://leetcode.com/onlinejudge#question_102

     Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).

     For example:
     Given binary tree {3,9,20,#,#,15,7},

     3
     / \
     9  20
     /  \
     15   7
     return its level order traversal as:

     [
     [3],
     [9,20],
     [15,7]
     ]
     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
     */

    public static void main(String[] args) {
    }

    public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        if (root == null) {
            return result;
        }

        Deque<TreeNode> queue = new ArrayDeque<TreeNode>();
        queue.add(root);
        TreeNode levelEnd = root;
        ArrayList<Integer> current = new ArrayList<Integer>();
        while (!queue.isEmpty()) {
            TreeNode top = queue.poll();
            if (top.left != null) {
                queue.add(top.left);
            }
            if (top.right != null) {
                queue.add(top.right);
            }

            current.add(top.val);
            if (top == levelEnd) {
                result.add(new ArrayList<Integer>(current));
                current.clear();
                if (!queue.isEmpty()) {
                    levelEnd = queue.getLast();
                }
            }
        }

        return result;
    }
}
