package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class BinaryTreeMaximumPathSum {

    /**
     http://leetcode.com/onlinejudge#question_124

     Given a binary tree, find the maximum path sum.

     The path may start and end at any node in the tree.

     For example:
     Given the below binary tree,

     5
     / \
     4   8
     /   / \
     11  13  4
     /  \      \
     7    2      1
     */

    public static void main(String[] args) {
        BinaryTreeMaximumPathSum s = new BinaryTreeMaximumPathSum();
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(4);
        root.left.left = new TreeNode(11);
        root.left.left.left = new TreeNode(7);
        root.left.left.right = new TreeNode(2);
        root.right = new TreeNode(8);
        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(4);
        root.right.right.right = new TreeNode(1);

        System.out.println(s.maxPathSum2(root));
        System.out.println(s.maxPathSum(root));
    }

    public int maxPathSum2(TreeNode root) {
        max = Integer.MIN_VALUE;
        search(root);
        return max;
    }

    private int search(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return root.val;
        }
        int left = 0;
        int right = 0;
        if (root.left != null) {
            left = search(root.left);
        }
        if (root.right != null) {
            right = search(root.right);
        }
        max = Math.max(max, root.val + left + right);
        return Math.max(root.val + left, root.val + right);
    }

    private int max = Integer.MIN_VALUE;

    public int maxPathSum(TreeNode root) {
        max = Integer.MIN_VALUE;
        travel(root);
        return max;
    }

    private void travel(TreeNode root) {
        if (root.left == null && root.right == null) {
            max = max < root.val ? root.val : max;
        } else if (root.left != null && root.right == null) {
            // 只有左子树，只要左子树返回正值就更新节点
            travel(root.left);
            if (root.left.val > 0) {
                root.val += root.left.val;
            }
            max = max < root.val ? root.val : max;
        } else if (root.right != null && root.left == null) {
            // 只有右子树
            travel(root.right);
            if (root.right.val > 0) {
                root.val += root.right.val;
            }
            max = max < root.val ? root.val : max;
        } else {
            travel(root.left);
            travel(root.right);
            // 如果最长路径存在在当前子树当中
            max = max < root.left.val + root.right.val + root.val ? root.left.val + root.right.val + root.val : max;

            // 更新父节点，只取更大的一个
            int tmp = root.right.val > root.left.val ? root.right.val : root.left.val;
            if (tmp > 0) {
                root.val += tmp;
            }
            max = max < root.val ? root.val : max;
        }
    }
}