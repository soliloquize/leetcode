package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.Constructor;
import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Stack;

public class BinaryTreePostorderTraversal {
    /**
     * http://oj.leetcode.com/problems/binary-tree-postorder-traversal/
     * <p/>
     * Given a binary tree, return the postorder traversal of its nodes' values.
     * <p/>
     * For example:
     * Given binary tree {1,#,2,3},
     * 1
     * \
     * 2
     * /
     * 3
     * return [3,2,1].
     * <p/>
     * Note: Recursive solution is trivial, could you do it iteratively?
     */
    public ArrayList<Integer> postorderTraversal(TreeNode root) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        TreeNode node = root;
        while (node != null) {
            stack.push(node);
            if (node.left != null) {
                node = node.left;
            } else {
                node = null;
                while (!stack.isEmpty()) {
                    // 如果弹出的节点有右子树则将右子树压栈，但若右子树是刚访问过的，则将当前元素弹出
                    TreeNode top = stack.pop();
                    if (top.right == null) {
                        result.add(top.val);
                    } else {
                        // 后序遍历已经访问过右子树，则弹出
                        if (node == top.right) {
                            result.add(top.val);
                        } else {
                            // 将弹出的元素放回去
                            stack.push(top);
                            node = top.right;
                            break;
                        }
                    }
                    node = top;
                }
                // 已经遍历完成
                if (stack.isEmpty()) {
                    break;
                }
            }
        }

        return result;
    }


    public static void main(String[] args) {
        BinaryTreePostorderTraversal s = new BinaryTreePostorderTraversal();
        System.out.println(s.postorderTraversal(Constructor.buildTree(new int[]{5, 4, 8, 11, Integer.MIN_VALUE, 13, 4, 7, 2, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, 1})));
    }
}
