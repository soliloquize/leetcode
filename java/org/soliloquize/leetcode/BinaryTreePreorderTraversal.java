package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.Constructor;
import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Stack;

public class BinaryTreePreorderTraversal {
    /**
     http://oj.leetcode.com/problems/binary-tree-preorder-traversal/

     Given a binary tree, return the preorder traversal of its nodes' values.

     For example:
     Given binary tree {1,#,2,3},
     1
     \
     2
     /
     3
     return [1,2,3].

     Note: Recursive solution is trivial, could you do it iteratively?
     */
    public ArrayList<Integer> preorderTraversal(TreeNode root) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        Deque<TreeNode> stack = new ArrayDeque<TreeNode>();
        stack.push(root);
        while (!stack.isEmpty()) {
            TreeNode p = stack.pop();
            result.add(p.val);
            if (p.right != null) {
                stack.push(p.right);
            }
            if (p.left != null) {
                stack.push(p.left);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode root = Constructor.buildTree(new int[] {1, Integer.MIN_VALUE, 2, 3});
        BinaryTreePreorderTraversal s = new BinaryTreePreorderTraversal();
        System.out.println(s.preorderTraversal(root));
    }
}
