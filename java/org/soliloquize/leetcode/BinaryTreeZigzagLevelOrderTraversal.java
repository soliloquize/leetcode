package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;

public class BinaryTreeZigzagLevelOrderTraversal {
    /**
     http://leetcode.com/onlinejudge#question_103

     Given a binary tree, return the zigzag level order traversal of its nodes' values.
     (ie, from left to right, then right to left for the next level and alternate between).

     For example:
     Given binary tree {3,9,20,#,#,15,7},

     3
     / \
     9  20
     /  \
     15   7
     return its zigzag level order traversal as:

     [
     [3],
     [20,9],
     [15,7]
     ]
     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
     */
    public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
        if (root == null) {
            return new ArrayList<ArrayList<Integer>>();
        }
        ArrayList<ArrayList<Integer>> result = levelOrder(root);
        for (int i = 1; i < result.size(); i += 2) {
            Collections.reverse(result.get(i));
        }
        return result;
    }

    public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        if (root == null) {
            return result;
        }
        ArrayDeque<Object> deque = new ArrayDeque<Object>();
        deque.add(root);
        // 用空串标识一层结束
        deque.add("");
        ArrayList<Integer> row = new ArrayList<Integer>();
        while (deque.size() > 0) {
            Object obj = deque.pollFirst();
            if (obj instanceof String) {
                result.add(row);
                row = new ArrayList<Integer>();
                // 增加新的分层标识
                if (deque.size() > 0) {
                    deque.add("");
                }
                continue;
            }
            TreeNode node = (TreeNode) obj;
            row.add(node.val);
            if (node.left != null) {
                deque.add(node.left);
            }
            if (node.right != null) {
                deque.add(node.right);
            }
        }
        return result;
    }
}
