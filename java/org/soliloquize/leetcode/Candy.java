package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Candy {
    /**
     http://oj.leetcode.com/problems/candy/

     There are N children standing in a line. Each child is assigned a rating value.

     You are giving candies to these children subjected to the following requirements:

     Each child must have at least one candy.
     Children with a higher rating get more candies than their neighbors.
     What is the minimum candies you must give?
     */

    class Pair implements Comparable<Pair> {
        public int val;
        public int pos;
        public Pair(int val, int pos) {
            this.val = val;
            this.pos = pos;
        }

        @Override
        public int compareTo(Pair o) {
            return val - o.val;
        }
    }

    public int candy2(int[] ratings) {
        if (ratings.length == 0) {
            return 0;
        }

        List<Pair> pairs = new ArrayList<Pair>();
        for (int i = 0; i < ratings.length; i++) {
            pairs.add(new Pair(ratings[i], i));
        }

        Collections.sort(pairs);

        int result = 0;
        int[] candy = new int[ratings.length];
        for (int i = 0; i < pairs.size(); i++) {
            int p = pairs.get(i).pos;
            int tmp = 0;
            if (p + 1 < candy.length && ratings[p] > ratings[p+1]) {
                tmp = Math.max(tmp, candy[p + 1]);
            }
            if (p - 1 >= 0 && ratings[p] > ratings[p-1]) {
                tmp = Math.max(tmp, candy[p - 1]);
            }
            candy[p] = tmp + 1;
            result += candy[p];
        }
        return result;
    }

    public int candy(int[] ratings) {
        if (ratings.length == 0) {
            return 0;
        }
        if (ratings.length == 1) {
            return 1;
        }
        List<Pair> pairs = new ArrayList<Pair>();
        for (int i = 0; i < ratings.length; i++) {
            pairs.add(new Pair(ratings[i], i));
        }
        Collections.sort(pairs);

        int[] candies = new int[ratings.length];
        Arrays.fill(candies, 0);

        for (int i = 0; i < pairs.size(); i++) {
            int index = pairs.get(i).pos;

            if (index == 0) {
                if (ratings[index + 1] < ratings[index]) {
                    candies[index] = candies[index+1] + 1;
                } else {
                    candies[index] = 1;
                }
            } else if (index == ratings.length - 1) {
                if (ratings[index - 1] < ratings[index]) {
                    candies[index] = candies[index-1] + 1;
                } else {
                    candies[index] = 1;
                }
            } else {
                int tmp1 = 0;
                int tmp2 = 0;
                if (ratings[index - 1] < ratings[index]) {
                    tmp1 = candies[index - 1] + 1;
                }
                if (ratings[index + 1] < ratings[index]) {
                    tmp2 = candies[index + 1] + 1;
                }
                candies[index] = Math.max(1, Math.max(tmp1, tmp2));
            }
        }

        int result = 0;
        for (int i = 0; i < ratings.length; i++) {
            result += candies[i];
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        Candy candy = new Candy();
        System.out.println(candy.candy(new int[] {1, 1, 1}));
        System.out.println(candy.candy2(new int[] {1, 2, 1}));
    }
}
