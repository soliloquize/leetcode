package org.soliloquize.leetcode;

public class ClimbingStairs {
    /**
     http://leetcode.com/oldoj#question_70

     You are climbing a stair case. It takes n steps to reach to the top.

     Each time you can either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
     */
    public int climbStairs(int n) {
        // f[x] = f[x-1] + f[x-2]
        int[] f = new int[n+1];

        f[0] = 1;
        for (int i = 1; i <= n; i++) {
            if (i == 1) {
                f[i] = f[0];
            } else {
                f[i] = f[i-1] + f[i-2];
            }
        }

        return f[n];
    }

    public int climbStairs2(int n) {
        int[] p = new int[n+1];
        p[0] = 1;
        p[1] = 1;
        for (int i = 2; i <= n; i++) {
            p[i] = p[i-1] + p[i-2];
        }
        return p[n];
    }

    public static void main(String[] args) {
        ClimbingStairs s = new ClimbingStairs();
        System.out.println(s.climbStairs(10));
        System.out.println(s.climbStairs2(10));
    }
}
