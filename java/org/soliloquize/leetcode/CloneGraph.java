package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.UndirectedGraphNode;

import java.util.*;

public class CloneGraph {
    /**
     http://oj.leetcode.com/problems/clone-graph/

     Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.


     OJ's undirected graph serialization:
     Nodes are labeled uniquely.

     We use # as a separator for each node, and , as a separator for node label and each neighbor of the node.
     As an example, consider the serialized graph {0,1,2#1,2#2,2}.

     The graph has a total of three nodes, and therefore contains three parts as separated by #.

     First node is labeled as 0. Connect node 0 to both nodes 1 and 2.
     Second node is labeled as 1. Connect node 1 to node 2.
     Third node is labeled as 2. Connect node 2 to node 2 (itself), thus forming a self-cycle.
     Visually, the graph looks like the following:

     1
     / \
     /   \
     0 --- 2
     / \
     \_/
     */

    public static void main(String[] args) {
        CloneGraph s = new CloneGraph();
    }

    public UndirectedGraphNode cloneGraph2(UndirectedGraphNode node) {
        if (node == null) {
            return node;
        }
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();
        UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
        map.put(node, newNode);
        Deque<UndirectedGraphNode> deque = new ArrayDeque<UndirectedGraphNode>();
        Set<UndirectedGraphNode> visited = new HashSet<UndirectedGraphNode>();
        deque.add(node);
        while (!deque.isEmpty()) {
            UndirectedGraphNode first = deque.poll();
            if (visited.contains(first)) {
                continue;
            }
            visited.add(first);
            UndirectedGraphNode correspond = map.get(first);
            for (UndirectedGraphNode tmp : first.neighbors) {
                if (map.containsKey(tmp)) {
                    correspond.neighbors.add(map.get(tmp));
                } else {
                    map.put(tmp, new UndirectedGraphNode(tmp.label));
                    correspond.neighbors.add(map.get(tmp));
                }
                deque.add(tmp);
            }
        }
        return node;
    }

    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null) {
            return null;
        }
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();

        UndirectedGraphNode root = new UndirectedGraphNode(node.label);
        map.put(node, root);
        LinkedList<UndirectedGraphNode> list = new LinkedList<UndirectedGraphNode>();
        Set<UndirectedGraphNode> visited = new HashSet<UndirectedGraphNode>();
        list.add(node);
        while (list.size() > 0) {
            UndirectedGraphNode first = list.poll();
            if (visited.contains(first)) {
                continue;
            }
            visited.add(first);
            UndirectedGraphNode correspond = map.get(first);
            for (UndirectedGraphNode connect : first.neighbors) {
                if (map.containsKey(connect)) {
                    correspond.neighbors.add(map.get(connect));
                } else {
                    UndirectedGraphNode tmp = new UndirectedGraphNode(connect.label);
                    map.put(connect, tmp);
                    correspond.neighbors.add(tmp);
                }
                list.add(connect);
            }
        }
        return root;
    }
}
