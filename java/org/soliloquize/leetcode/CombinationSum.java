package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class CombinationSum {
    /**
     http://leetcode.com/oldoj#question_39

     Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

     The same repeated number may be chosen from C unlimited number of times.

     Note:

     All numbers (including target) will be positive integers.
     Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 ? a2 ? � ? ak).
     The solution set must not contain duplicate combinations.
     For example, given candidate set 2,3,6,7 and target 7,
     A solution set is:
     [7]
     [2, 2, 3]
     */

    public static void main(String[] args) {
        CombinationSum s = new CombinationSum();
        System.out.println(s.combinationSum(new int[] {2, 3, 6, 7}, 7));
        System.out.println(s.combinationSum2(new int[]{2, 3, 6, 7}, 7));
    }

    public ArrayList<ArrayList<Integer>> combinationSum2(int[] candidates, int target) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        search(candidates, 0, target, new ArrayList<Integer>(), result);
        return result;
    }

    private void search(int[] candidates, int index, int target, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (target == 0) {
            result.add(new ArrayList<Integer>(current));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (target >= candidates[i]) {
                current.add(candidates[i]);
                search(candidates, i, target - candidates[i], current, result);
                current.remove(current.size()-1);
            }
        }
    }

    public ArrayList<ArrayList<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        calculation(candidates, 0, target, new ArrayList<Integer>(), result);
        return result;
    }

    private void calculation(int[] candidates, int index, int target, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (target == 0) {
            result.add(new ArrayList<Integer>(current));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            if (target < candidates[i]) {
                return;
            }
            int tmp = target - candidates[i];
            current.add(candidates[i]);
            calculation(candidates, i, tmp, current, result);
            current.remove(current.size()-1);
        }
    }
}
