package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class CombinationSumII {

    /**
     http://leetcode.com/oldoj#question_40

     Given a collection of candidate numbers (C) and a target number (T), find all unique combinations in C where the candidate numbers sums to T.

     Each number in C may only be used once in the combination.

     Note:

     All numbers (including target) will be positive integers.
     Elements in a combination (a1, a2, � , ak) must be in non-descending order. (ie, a1 ? a2 ? � ? ak).
     The solution set must not contain duplicate combinations.
     For example, given candidate set 10,1,2,7,6,1,5 and target 8,
     A solution set is:
     [1, 7]
     [1, 2, 5]
     [2, 6]
     [1, 1, 6]
     */
    public static void main(String[] args) {
        CombinationSumII s = new CombinationSumII();
        System.out.println(s.combinationSum2(new int[] {10,1,2,7,6,1,5}, 8));
        System.out.println(s.combinationSum22(new int[]{10, 1, 2, 7, 6, 1, 5}, 8));
    }

    public ArrayList<ArrayList<Integer>> combinationSum22(int[] num, int target) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(num);
        search(num, 0, target, new ArrayList<Integer>(), result);
        return result;
    }

    private void search(int[] num, int index, int target, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (target == 0) {
            result.add(new ArrayList<Integer>(current));
        }
        for (int i = index; i < num.length; i++) {
            if (target < num[i]) {
                return;
            }
            current.add(num[i]);
            search(num, i + 1, target - num[i], current, result);
            current.remove(current.size()-1);
            while (i + 1 < num.length && num[i] == num[i+1]) {
                i += 1;
            }
        }
    }

    public ArrayList<ArrayList<Integer>> combinationSum2(int[] num, int target) {
        Arrays.sort(num);
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        calculation(num, 0, target, new ArrayList<Integer>(), result);
        return result;
    }

    public void calculation(int[] num, int index, int target, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (target == 0) {
            ArrayList<Integer> r = new ArrayList<Integer>(current);
            for (int i = 0; i < result.size(); i++) {
                if (r.equals(result.get(i))) {
                    return;
                }
            }
            result.add(r);
        }
        for (int i = index; i < num.length; i++) {
            if (target < num[i]) {
                return;
            }
            int tmp = target - num[i];
            current.add(num[i]);
            calculation(num, i + 1, tmp, current, result);
            current.remove(current.size()-1);
        }
    }
}
