package org.soliloquize.leetcode;

import java.util.ArrayList;

public class Combinations {
    /**
     http://leetcode.com/oldoj#question_77

     Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.

     For example,
     If n = 4 and k = 2, a solution is:

     [
     [2,4],
     [3,4],
     [2,3],
     [1,2],
     [1,3],
     [1,4],
     ]
     */

    public static void main(String[] args) {
        Combinations s = new Combinations();
        System.out.println(s.combine(4, 2));
        System.out.println(s.combine2(4, 2));
    }

    public ArrayList<ArrayList<Integer>> combine2(int n, int k) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = i + 1;
        }
        combine2(numbers, 0, k, new ArrayList<Integer>(), result);
        return result;
    }

    private void combine2(int[] numbers, int index, int k, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (k == 0) {
            result.add(new ArrayList<Integer>(current));
            return;
        }
        if (index >= numbers.length) {
            return;
        }
        for (int i = index; i < numbers.length; i++) {
            current.add(numbers[i]);
            combine2(numbers, i + 1, k - 1, current, result);
            current.remove(current.size()-1);
        }
    }

    public ArrayList<ArrayList<Integer>> combine(int n, int k) {
        int[] num = new int[n];
        for (int i = 0; i < num.length; i++) {
            num[i] = i + 1;
        }
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        calculate(num, 0, k, new ArrayList<Integer>(), result);
        return result;
    }

    private void calculate(int[] num, int index, int count, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        if (count == 0) {
            result.add(new ArrayList<Integer>(current));
            return;
        }
        if (index >= num.length) {
            return;
        }
        for (int i = index; i < num.length; i++) {
            current.add(num[i]);
            calculate(num, i + 1, count - 1, current, result);
            current.remove(current.size()-1);
        }
    }
}
