package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class ConstructBinaryTreeFromInorderAndPostorderTraversal {
    /**
     http://leetcode.com/oldoj#question_106

     Given inorder and postorder traversal of a tree, construct the binary tree.

     Note:
     You may assume that duplicates do not exist in the tree.
     */
    public static void main(String[] args) {
        ConstructBinaryTreeFromInorderAndPostorderTraversal s = new ConstructBinaryTreeFromInorderAndPostorderTraversal();
        BinaryTreeLevelOrderTraversal ls = new BinaryTreeLevelOrderTraversal();
        System.out.println(ls.levelOrder(s.buildTree(new int[] {4, 2, 5, 1, 3}, new int[] {4, 5, 2, 3, 1})));
        System.out.println(ls.levelOrder(s.buildTree2(new int[]{4, 2, 5, 1, 3}, new int[]{4, 5, 2, 3, 1})));
    }

    public TreeNode buildTree2(int[] inorder, int[] postorder) {
        if (inorder.length == 0) {
            return null;
        }
        return buildTree2(inorder, 0, inorder.length, postorder, 0, postorder.length);
    }

    private TreeNode buildTree2(int[] inorder, int istart, int iend, int[] postorder, int pstart, int pend) {
        int val = postorder[pend-1];

        int count = 0;
        for (int i = istart; i < iend; i++, count++) {
            if (inorder[i] == val) {
                break;
            }
        }

        TreeNode node = new TreeNode(val);
        if (count > 0) {
            node.left = buildTree2(inorder, istart, istart + count, postorder, pstart, pstart + count);
        }
        if (istart + count < iend - 1) {
            node.right = buildTree2(inorder, istart + count + 1, iend, postorder, pstart + count, pend - 1);
        }
        return node;
    }

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        if (postorder == null || postorder.length == 0) {
            return null;
        }
        TreeNode node = new TreeNode(postorder[postorder.length - 1]);
        int i = 0;
        for (; i < inorder.length; i++) {
            if (inorder[i] == node.val) {
                break;
            }
        }

        int[] newLeft = new int[i];
        System.arraycopy(inorder, 0, newLeft, 0, newLeft.length);

        int[] newRight = new int[inorder.length - i - 1];
        System.arraycopy(inorder, i + 1, newRight, 0, newRight.length);

        int[] newLeftPostOrder = new int[newLeft.length];
        System.arraycopy(postorder, 0, newLeftPostOrder, 0, newLeftPostOrder.length);

        int[] newRightPostOrder = new int[newRight.length];
        System.arraycopy(postorder, newLeftPostOrder.length, newRightPostOrder, 0, newRightPostOrder.length);

        node.left = buildTree(newLeft, newLeftPostOrder);
        node.right = buildTree(newRight, newRightPostOrder);

        return node;
    }
}
