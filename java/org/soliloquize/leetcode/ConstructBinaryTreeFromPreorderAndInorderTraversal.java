package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    /**
     http://leetcode.com/onlinejudge#question_105

     Given preorder and inorder traversal of a tree, construct the binary tree.

     Note:
     You may assume that duplicates do not exist in the tree.
     */
    public static void main(String[] args) {
        ConstructBinaryTreeFromPreorderAndInorderTraversal s = new ConstructBinaryTreeFromPreorderAndInorderTraversal();
        BinaryTreeLevelOrderTraversal ls = new BinaryTreeLevelOrderTraversal();
        System.out.println(ls.levelOrder(s.buildTree(new int[] {1, 2, 4, 5, 3}, new int [] {4, 2, 5, 1, 3})));
        System.out.println(ls.levelOrder(s.buildTree2(new int[]{1, 2, 4, 5, 3}, new int[]{4, 2, 5, 1, 3})));
    }

    public TreeNode buildTree2(int[] preorder, int[] inorder) {
        return buildTree2(preorder, 0, preorder.length, inorder, 0, inorder.length);
    }

    private TreeNode buildTree2(int[] preorder, int pstart, int pend, int[] inorder, int istart, int iend) {
        int val = preorder[pstart];

        int count = 0;
        for (int i = istart; i < iend; i++, count++) {
            if (inorder[i] == val) {
                break;
            }
        }

        TreeNode node = new TreeNode(val);
        if (count > 0) {
            node.left = buildTree2(preorder, pstart + 1, pstart + 1 + count, inorder, istart, istart + count);
        }
        if (istart + count < iend - 1) {
            node.right = buildTree2(preorder, pstart + 1 + count, pend, inorder, istart + count + 1, iend);
        }
        return node;
    }

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if (preorder == null || preorder.length == 0) {
            return null;
        }
        TreeNode node = new TreeNode(preorder[0]);
        int i = 0;
        for (; i < inorder.length; i++) {
            if (inorder[i] == preorder[0]) {
                break;
            }
        }
        int[] leftInorder = new int[i];
        System.arraycopy(inorder, 0, leftInorder, 0, leftInorder.length);
        int[] leftPreorder = new int[leftInorder.length];
        System.arraycopy(preorder, 1, leftPreorder, 0, leftInorder.length);
        node.left = buildTree(leftPreorder, leftInorder);

        int[] rightInorder = new int[inorder.length - i - 1];
        System.arraycopy(inorder, i+1, rightInorder, 0, rightInorder.length);
        int[] rightPreorder = new int[rightInorder.length];
        System.arraycopy(preorder, 1 + leftInorder.length, rightPreorder, 0, rightInorder.length);
        node.right = buildTree(rightPreorder, rightInorder);

        return node;
    }
}
