package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class ConvertSortedArrayToBinarySearchTree {
    /**
     http://leetcode.com/oldoj#question_108

     Given an array where elements are sorted in ascending order, convert it to a height balanced BST.
     */

    public static void main(String[] args) {
        ConvertSortedArrayToBinarySearchTree s = new ConvertSortedArrayToBinarySearchTree();
        BinaryTreeLevelOrderTraversal ls = new BinaryTreeLevelOrderTraversal();
        System.out.println(ls.levelOrder(s.sortedArrayToBST(new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9})));
        System.out.println(ls.levelOrder(s.sortedArrayToBST2(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9})));
    }

    public TreeNode sortedArrayToBST2(int[] num) {
        if (num.length == 0) {
            return null;
        }
        return sortedArrayToBST2(num, 0, num.length);
    }

    private TreeNode sortedArrayToBST2(int[] num, int start, int end) {
        int middle = (start + end) / 2;
        TreeNode node = new TreeNode(num[middle]);
        if (middle > start) {
            node.left = sortedArrayToBST2(num, start, middle);
        }
        if (middle < end - 1) {
            node.right = sortedArrayToBST2(num, middle + 1, end);
        }
        return node;
    }

    public TreeNode sortedArrayToBST(int[] num) {
        if (num.length == 0) {
            return null;
        }
        int middle = num.length / 2;
        TreeNode node = new TreeNode(num[middle]);
        if (middle > 0) {
            int[] left = new int[middle];
            System.arraycopy(num, 0, left, 0, left.length);
            node.left = sortedArrayToBST(left);
        }
        if (middle < num.length - 1) {
            int[] right = new int[num.length - 1 - middle];
            System.arraycopy(num, middle + 1, right, 0, right.length);
            node.right = sortedArrayToBST(right);
        }
        return node;
    }
}
