package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;
import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class ConvertSortedListToBinarySearchTree {
    /**
     http://leetcode.com/oldoj#question_109

     Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.
     */

    public static void main(String[] args) {
        int[] vals = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9};
        ListNode head = new ListNode(1);
        ListNode p = head;
        for (int i = 1; i < vals.length; i++) {
            p.next = new ListNode(vals[i]);
            p = p.next;
        }

        ConvertSortedListToBinarySearchTree s = new ConvertSortedListToBinarySearchTree();
        BinaryTreeLevelOrderTraversal ls = new BinaryTreeLevelOrderTraversal();
        System.out.println(ls.levelOrder(s.sortedListToBST(head)));
        System.out.println(ls.levelOrder(s.sortedListToBST2(head)));
    }

    public TreeNode sortedListToBST2(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode p = head;
        int length = 0;
        while (p != null) {
            p = p.next;
            length += 1;
        }
        return sortedListToBST2(head, 0, length);
    }

    private TreeNode sortedListToBST2(ListNode head, int start, int end) {
        int middle = (start + end) / 2;

        ListNode p = head;
        int j = 0;
        while (j < (middle - start)) {
            p = p.next;
            j += 1;
        }

        TreeNode node = new TreeNode(p.val);
        if (middle > start) {
            node.left = sortedListToBST2(head, start, middle);
        }
        if (middle < end - 1) {
            node.right = sortedListToBST2(p.next, middle + 1, end);
        }
        return node;
    }

    public TreeNode sortedListToBST(ListNode head) {
        List<Integer> nums = new ArrayList<Integer>();
        while (head != null) {
            nums.add(head.val);
            head = head.next;
        }
        int[] digits = new int[nums.size()];
        for (int i = 0; i < nums.size(); i++) {
            digits[i] = nums.get(i);
        }
        return sortedArrayToBST(digits);
    }

    private TreeNode sortedArrayToBST(int[] num) {
        if (num.length == 0) {
            return null;
        }
        int middle = num.length / 2;
        TreeNode node = new TreeNode(num[middle]);
        if (middle > 0) {
            int[] left = new int[middle];
            System.arraycopy(num, 0, left, 0, left.length);
            node.left = sortedArrayToBST(left);
        }
        if (middle < num.length - 1) {
            int[] right = new int[num.length - 1 - middle];
            System.arraycopy(num, middle + 1, right, 0, right.length);
            node.right = sortedArrayToBST(right);
        }
        return node;
    }
}
