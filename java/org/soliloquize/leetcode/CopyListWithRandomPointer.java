package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.RandomListNode;

import java.util.HashMap;
import java.util.Map;

public class CopyListWithRandomPointer {
    /**
     http://oj.leetcode.com/problems/copy-list-with-random-pointer/

     A linked list is given such that each node contains an additional random pointer which could point to any node in the list or null.

     Return a deep copy of the list.
     */
    public RandomListNode copyRandomList(RandomListNode head) {
        if (head == null) {
            return head;
        }
        RandomListNode p = head;
        // 新链表的首元素
        RandomListNode n = null;
        // 记录循环过程中前次元素
        RandomListNode z = null;
        Map<RandomListNode, RandomListNode> map = new HashMap<RandomListNode, RandomListNode>();
        // 拷贝顺序元素
        while (p != null) {
            RandomListNode x = new RandomListNode(p.label);
            if (z != null) {
                z.next = x;
            }
            if (n == null) {
                n = x;
            }
            z = x;
            map.put(p, z);
            p = p.next;
        }

        p = head;
        RandomListNode q = n;
        while (p != null) {
            if (p.random != null) {
                q.random = map.get(p.random);
            }
            p = p.next;
            q = q.next;
        }

        return n;
    }

    public static void main(String[] args) {
        CopyListWithRandomPointer s = new CopyListWithRandomPointer();
        RandomListNode head = new RandomListNode(-1);

        RandomListNode res = s.copyRandomList(head);
        System.out.println(res.label);
    }
}
