package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.List;

public class CountAndSay {
    /**
     http://oj.leetcode.com/problems/count-and-say/

     The count-and-say sequence is the sequence of integers beginning as follows:
     1, 11, 21, 1211, 111221, ...

     1 is read off as "one 1" or 11.
     11 is read off as "two 1s" or 21.
     21 is read off as "one 2, then one 1" or 1211.
     Given an integer n, generate the nth sequence.

     Note: The sequence of integers will be represented as a string.
     */
    public String countAndSay(int n) {
        List<Integer> a = new ArrayList<Integer>();
        List<Integer> b = new ArrayList<Integer>();
        List<Integer> c = null;
        a.add(1);
        n -= 1;
        for (int k = 0; k < n; k++) {
            for (int i = 0; i < a.size(); i++) {
                int count = 0;
                int j = i;
                for (; j < a.size(); j++) {
                    if (a.get(i) == a.get(j)) {
                        count += 1;
                    } else {
                        break;
                    }
                }
                i = j - 1;
                b.add(count);
                b.add(a.get(i));
            }
            c = a;
            a = b;
            b = c;
            b.clear();
        }

        StringBuilder builder = new StringBuilder();
        for (int i : a) {
            builder.append((char)('0' + i));
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        CountAndSay s = new CountAndSay();
        System.out.println(s.countAndSay(1));
        System.out.println(s.countAndSay(2));
        System.out.println(s.countAndSay(3));
        System.out.println(s.countAndSay(4));
    }
}
