package org.soliloquize.leetcode;

import java.util.Arrays;

public class DecodeWays {
    /**
     http://leetcode.com/oldoj#question_91

     A message containing letters from A-Z is being encoded to numbers using the following mapping:

     'A' -> 1
     'B' -> 2
     ...
     'Z' -> 26
     Given an encoded message containing digits, determine the total number of ways to decode it.

     For example,
     Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).

     The number of ways decoding "12" is 2.
     */
    public int numDecodings(String s) {
        // p[i] = p[i-1] + p[i-2], if s[i-1,i] can be encoded
        int [] p = new int[s.length() + 1];
        Arrays.fill(p, 0);
        if (s.length() == 0) {
            return 0;
        }
        if (s.charAt(0) == '0') {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        p[0] = 1;
        p[1] = 1;
        for (int i = 1; i < s.length(); i++) {
            StringBuilder builder = new StringBuilder();
            String code = builder.append(s.charAt(i-1)).append(s.charAt(i)).toString();
            int tmp = Integer.valueOf(code);
            if (s.charAt(i) == '0') {
                if (tmp != 10 && tmp != 20) {
                    return 0;
                }
                p[i+1] = p[i-1];
                continue;
            }
            if (tmp >= 10 && tmp <= 26) {
                p[i+1] = p[i] + p[i-1];
                continue;
            }
            p[i+1] = p[i];
        }
        return p[s.length()];
    }

    public int numDecodings2(String s) {
        if (s.length() == 0) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        if (s.charAt(0) == '0') {
            return 0;
        }
        int[] p = new int[s.length()+1];
        p[0] = 1;
        p[1] = 1;

        for (int i = 1, j = 2; i < s.length(); i++, j++) {
            if (s.charAt(i) == '0') {
                if (isChar(s, i-1, i+1)) {
                    p[j] = p[j-2];
                } else {
                    return 0;
                }
            } else {
                if (isChar(s, i-1, i+1)) {
                    p[j] = p[j-1] + p[j-2];
                } else {
                    p[j] = p[j-1];
                }
            }
        }
        return p[s.length()];
    }

    private boolean isChar(String s, int start, int end) {
        int res = Integer.valueOf(s.substring(start, end));
        return res <= 26 && res >= 10;
    }

    public static void main(String[] args) {
        DecodeWays s = new DecodeWays();
        System.out.println(s.numDecodings("121221"));
        System.out.println(s.numDecodings2("121221"));
    }
}
