package org.soliloquize.leetcode;

public class DivideTwoIntegers {

    /**
     http://leetcode.com/oldoj#question_29

     Divide two integers without using multiplication, division and mod operator.
     */
    public int divide2(int dividend, int divisor) {
        boolean negative = false;
        if ((dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0)) {
            negative = true;
        }
        long a = Math.abs((long) dividend);
        long b = Math.abs((long) divisor);

        long result = 0;
        while (a >= b) {
            for (long c = b, i = 0; c <= a; c <<= 1, i += 1) {
                a -= c;
                result += 1L << i;
            }
        }
        if (negative) {
            return (int)(-result);
        }
        return (int) result;
    }


    public int divide(int dividend, int divisor) {
        boolean negative = false;
        if ((dividend > 0 && divisor < 0) || (dividend < 0 && divisor > 0)) {
            negative = true;
        }
        long a = Math.abs((long)dividend);
        long b = Math.abs((long)divisor);

        long result = 0;
        while (a >= b) {
            long c = b;
            for (long i = 0; a >= c; c <<= 1, i++) {
                a -= c;
                result += 1L << i;
            }
        }
        if (negative) {
            return (int)(-result);
        }
        return (int) result;
    }

    public static void main(String[] args) {
        DivideTwoIntegers s = new DivideTwoIntegers();
        System.out.println(s.divide(-2147483648, 1));
        System.out.println(s.divide2(-2147483648, 1));
        System.out.println(s.divide2(10, 3));
    }
}
