package org.soliloquize.leetcode;

import java.util.Arrays;

public class EditDistance {
    /**
     http://oj.leetcode.com/problems/edit-distance/

     Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. (each operation is counted as 1 step.)

     You have the following 3 operations permitted on a word:

     a) Insert a character
     b) Delete a character
     c) Replace a character
     */
    public int minDistance(String word1, String word2) {
        // p[i][j] = p[i-1][j-1] if a[i] == b[j]
        //         = p[i-1][j] + 1, if delete a[i]
        //         = p[i][j-1] + 1, if insert at a[i]
        //         = p[i-1][j-1] + 1, if replace at a[i]
        int p[][] = new int[word1.length()+1][word2.length()+1];
        for (int i = 0; i < p.length; i++) {
            Arrays.fill(p[i], 0);
        }
        // 初始化
        for (int i = 0; i < p.length; i++) {
            p[i][0] = i;
        }
        for (int j = 0; j < p[0].length; j++) {
            p[0][j] = j;
        }
        for (int i = 1; i < p.length; i++) {
            for (int j = 1; j < p[i].length; j++) {
                if (word1.charAt(i-1) == word2.charAt(j-1)) {
                    p[i][j] = p[i-1][j-1];
                } else {
                    p[i][j] = Math.min(Math.min(p[i-1][j], p[i][j-1]), p[i-1][j-1]) + 1;
                }
            }
        }
        return p[word1.length()][word2.length()];
    }

    public int minDistance2(String word1, String word2) {
        int m = word1.length();
        int n = word2.length();
        int[][] p = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                boolean flag = word1.charAt(i) == word2.charAt(j);
                if (i == 0 || j == 0) {
                    if (flag) {
                        p[i][j] = Math.max(i, j);
                    } else {
                        p[i][j] = Math.max(i, j) + 1;
                    }
                } else {
                    if (flag) {
                        p[i][j] = p[i-1][j-1];
                    } else {
                        p[i][j] = Math.min(Math.min(p[i-1][j], p[i][j-1]), p[i-1][j-1]) + 1;
                    }
                }
            }
        }
        return p[m-1][n-1];
    }

    public static void main(String[] args) {
        EditDistance s = new EditDistance();
        System.out.println(s.minDistance("jinqiang", "stefanie"));
        System.out.println(s.minDistance2("jinqiang", "stefanie"));
    }
}
