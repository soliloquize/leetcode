package org.soliloquize.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class EvaluateReversePolishNotation {
    /**
     http://oj.leetcode.com/problems/evaluate-reverse-polish-notation/

     Evaluate the value of an arithmetic expression in Reverse Polish Notation.

     Valid operators are +, -, *, /. Each operand may be an integer or another expression.

     Some examples:
     ["2", "1", "+", "3", "*"] -> ((2 + 1) * 3) -> 9
     ["4", "13", "5", "/", "+"] -> (4 + (13 / 5)) -> 6
     */

    public static void main(String[] args) {
        EvaluateReversePolishNotation s = new EvaluateReversePolishNotation();
        System.out.println(s.evalRPN(new String[] {"2", "1", "+", "3", "*"}));
        System.out.println(s.evalRPN(new String[] {"4", "13", "5", "/", "+"}));
        System.out.println("->");
        System.out.println(s.evalRPN2(new String[] {"2", "1", "+", "3", "*"}));
        System.out.println(s.evalRPN2(new String[] {"4", "13", "5", "/", "+"}));
    }

    public int evalRPN2(String[] tokens) {
        Deque<Integer> stack = new ArrayDeque<Integer>();
        for (String str : tokens) {
            if (isOperator(str)) {
                int b = stack.pop();
                int a = stack.pop();
                if (str.equals("+")) {
                    stack.push(a + b);
                } else if (str.equals("-")) {
                    stack.push(a - b);
                } else if (str.equals("*")) {
                    stack.push(a * b);
                } else if (str.equals("/")) {
                    stack.push(a / b);
                }
            } else {
                stack.push(Integer.parseInt(str));
            }
        }
        return stack.pop();
    }

    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<Integer>();

        for (int i = 0; i < tokens.length; i++) {
            String str = tokens[i];
            if (!isOperator(str)) {
                stack.add(Integer.valueOf(str));
            } else {
                int a = stack.pop();
                int b = stack.pop();
                if (str.equals("+")) {
                    stack.push(b + a);
                } else if (str.equals("-")) {
                    stack.push(b - a);
                } else if (str.equals("*")) {
                    stack.push(b * a);
                } else {
                    stack.push(b / a);
                }
            }
        }
        return stack.pop();
    }

    private boolean isOperator(String str) {
        return str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/");
    }
}
