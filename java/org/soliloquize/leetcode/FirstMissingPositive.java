package org.soliloquize.leetcode;

public class FirstMissingPositive {

    /**
     http://leetcode.com/onlinejudge#question_41

     Given an unsorted integer array, find the first missing positive integer.

     For example,
     Given [1,2,0] return 3,
     and [3,4,-1,1] return 2.

     Your algorithm should run in O(n) time and uses constant space.
     */
    public int firstMissingPositive2(int[] A) {
        if (A.length == 0) {
            return 1;
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] <= 0) {
                A[i] = Integer.MAX_VALUE;
            }
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] < A.length && A[A[i]-1] > 0) {
                A[A[i]-1] *= -1;
            }
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] > 0) {
                return i + 1;
            }
        }
        return A.length + 1;
    }


    public int firstMissingPositive(int[] A) {
        if (A.length == 0) {
            return 1;
        }
        // 将负数标记为不可能出现的大数
        for (int i = 0; i < A.length; i++) {
            if (A[i] <= 0) {
                A[i] = Integer.MAX_VALUE;
            }
        }

        // 调整A[i]中的值，A[i] = -A[i] if i存在，最后遍历数组找到第一个正数的位置
        for (int i = 0; i < A.length; i++) {
            int tmp = Math.abs(A[i]) - 1;
            if (tmp >= A.length) {
                continue;
            }
            if (A[tmp] > 0) {
                A[tmp] *= -1;
            }
        }

        for (int i = 0; i < A.length; i++) {
            if (A[i] > 0) {
                return i + 1;
            }
        }
        return A.length + 1;
    }

    public static void main(String[] args) {
        FirstMissingPositive s = new FirstMissingPositive();
        System.out.println(s.firstMissingPositive2(new int[] {0}));
    }
}
