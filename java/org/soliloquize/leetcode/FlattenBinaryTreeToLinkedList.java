package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class FlattenBinaryTreeToLinkedList {
    /**
     http://leetcode.com/onlinejudge#question_114

     Given a binary tree, flatten it to a linked list in-place.

     For example,
     Given

     1
     / \
     2   5
     / \   \
     3   4   6
     The flattened tree should look like:
     1
     \
     2
     \
     3
     \
     4
     \
     5
     \
     6
     */

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(5);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        root.right.right = new TreeNode(6);

        FlattenBinaryTreeToLinkedList s = new FlattenBinaryTreeToLinkedList();
        s.flatten(root);
        TreeNode p = root;
        while (p != null) {
            System.out.println(p.val);
            p = p.right;
        }


        TreeNode root1 = new TreeNode(1);
        root1.left = new TreeNode(2);
        root1.right = new TreeNode(5);
        root1.left.left = new TreeNode(3);
        root1.left.right = new TreeNode(4);
        root1.right.right = new TreeNode(6);

        s.flatten2(root);
        p = root;
        while (p != null) {
            System.out.println(p.val);
            p = p.right;
        }
    }

    public void flatten2(TreeNode root) {
        if (root == null) {
            return;
        }
        last = null;
        search(root);
    }

    private void search(TreeNode root) {
        if (last != null) {
            last.right = root;
        }
        last = root;
        if (root.left != null) {
            search(root.left);
        }
        if (root.right != null) {
            search(root.right);
        }
    }

    private TreeNode last;

    public void flatten(TreeNode root) {
        if (root == null) {
            return;
        }
        travel(root);
        // 用右子树替换
        while (root.left != null) {
            root.right = root.left;
            root.left = null;
            root = root.right;
        }
    }

    /**
     * 递归访问，记录前一个节点的位置，用左节点保存指向关系
     */
    private void travel(TreeNode root) {
        last = root;
        if (root.left != null) {
            last.left = root.left;
            travel(root.left);
        }
        if (root.right != null) {
            last.left = root.right;
            travel(root.right);
        }
    }
}
