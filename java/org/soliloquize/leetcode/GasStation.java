package org.soliloquize.leetcode;

public class GasStation {
    /**
     http://oj.leetcode.com/problems/gas-station/

     There are N gas stations along a circular route, where the amount of gas at station i is gas[i].

     You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to its next station (i+1).
     You begin the journey with an empty tank at one of the gas stations.

     Return the starting gas station's index if you can travel around the circuit once, otherwise return -1.

     Note:
     The solution is guaranteed to be unique.
     */

    public int canCompleteCircuit(int[] gas, int[] cost) {
        int [] p = new int[gas.length * 2];
        for (int i = 0; i < gas.length; i++) {
            p[i] = gas[i] - cost[i];
            p[i + gas.length] = p[i];
        }

        int max = Integer.MIN_VALUE;
        int start = 0;
        int sum = 0;
        int current = -1;

        // 寻找最大连续子序列之和，从最长连续字段和的起点开始
        for (int i = 0; i < p.length; i++) {
            sum += p[i];
            if (sum < 0) {
                sum = 0;
                current = i;
                if (i >= gas.length) {
                    break;
                }
                continue;
            }
            if (max < sum) {
                max = sum;
                start = current + 1;
            }
        }

        sum = 0;
        for (int i = start; i < gas.length; i++) {
            sum += p[i];
            if (sum < 0) {
                return -1;
            }
        }
        for (int i = 0; i < start; i++) {
            sum += p[i];
            if (sum < 0) {
                return -1;
            }
        }
        return start;
    }

    public int canCompleteCircuit2(int[] gas, int[] cost) {
        int[] p = new int[gas.length*2];
        for (int i = 0; i < gas.length; i++) {
            p[i] = gas[i] - cost[i];
            p[i + gas.length] = p[i];
        }

        int max = Integer.MIN_VALUE;
        int pos = -1;
        int current = 0;

        for (int i = 0; i < p.length; i++) {
            current += p[i];
            if (max < current) {
                pos = i;
                max = current;
            }
            if (current < 0) {
                current = 0;
            }
        }

        int sum = 0;
        for (int i = pos; i < gas.length; i++) {
            sum += p[i];
            if (sum < 0) {
                return -1;
            }
        }
        for (int i = 0; i < pos; i++) {
            sum += p[i];
            if (sum < 0) {
                return -1;
            }
        }
        return pos;
    }

    public static void main(String[] args) {
        GasStation s = new GasStation();
        System.out.println(s.canCompleteCircuit(new int[] {1, 2}, new int[] {2, 1}));
        System.out.println(s.canCompleteCircuit2(new int[] {1, 2}, new int[] {2, 1}));
    }
}
