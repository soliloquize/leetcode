package org.soliloquize.leetcode;

import java.util.ArrayList;

public class GenerateParentheses {

    /**
     http://leetcode.com/onlinejudge#question_22

     Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

     For example, given n = 3, a solution set is:

     "((()))", "(()())", "(())()", "()(())", "()()()"
     */
    public ArrayList<String> generateParenthesis(int n) {
        ArrayList<String> result = new ArrayList<String>();
        generate("", n, n, result);
        return result;
    }

    private void generate(String current, int m, int n, ArrayList<String> result) {
        if (m == 0 && n == 0) {
            result.add(current);
        }
        if (m > 0) {
            generate(current + "(", m - 1, n, result);
        }
        if (n > 0 && n > m) {
            generate(current + ")", m, n - 1, result);
        }
    }


    public ArrayList<String> generateParenthesis2(int n) {
        ArrayList<String> result = new ArrayList<String>();
        generate(n, n, new StringBuilder(), result);
        return result;
    }

    private void generate(int left, int right, StringBuilder builder, ArrayList<String> result) {
        if (left == 0 && right == 0) {
            result.add(builder.toString());
        }

        if (left > 0) {
            builder.append('(');
            generate(left - 1, right, builder, result);
            builder.deleteCharAt(builder.length()-1);
        }

        if (right > left) {
            builder.append(')');
            generate(left, right - 1, builder, result);
            builder.deleteCharAt(builder.length()-1);
        }
    }

    public static void main(String[] args) {
        GenerateParentheses s = new GenerateParentheses();
        System.out.println(s.generateParenthesis(3));
        System.out.println(s.generateParenthesis(4));

        System.out.println(s.generateParenthesis2(3));
        System.out.println(s.generateParenthesis2(4));
    }
}