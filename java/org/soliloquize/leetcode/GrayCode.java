package org.soliloquize.leetcode;

import java.util.ArrayList;

public class GrayCode {

    /**
     http://oj.leetcode.com/problems/gray-code/

     参考：http://en.wikipedia.org/wiki/Gray_code
     第i个元素为: i^(i>>1)
     */
    public ArrayList<Integer> grayCode(int n) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < 1 << n; i++) {
            result.add(i ^ (i >> 1));
        }
        return result;
    }

    public ArrayList<Integer> grayCode_(int n) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        result.add(0);
        for (int i = 0; i < n; i++) {
            for (int j = result.size()-1; j >= 0; j--) {
                result.add(result.get(j) | (1 << i));
            }
        }
        return result;
    }

    public static void main(String[] args) {
        GrayCode s = new GrayCode();
        System.out.println(s.grayCode(3));
        System.out.println(s.grayCode_(1));
    }
}
