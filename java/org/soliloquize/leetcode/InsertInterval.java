package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class InsertInterval {
    /**
     http://leetcode.com/onlinejudge#question_57

     Given a set of non-overlapping intervals, insert a new interval into the intervals (merge if necessary).

     You may assume that the intervals were initially sorted according to their start times.

     Example 1:
     Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].

     Example 2:
     Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].

     This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
     */
    public class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }

    public ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        intervals.add(newInterval);
        return merge(intervals);
    }

    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                if (o1.start < o2.start) {
                    return -1;
                } else if (o1.start > o2.start) {
                    return 1;
                } else {
                    return o1.end - o2.end;
                }
            }
        });
        ArrayList<Interval> result = new ArrayList<Interval>();
        for (int i = 0; i < intervals.size(); ) {
            Interval a = intervals.get(i);
            int j = i + 1;
            while (j < intervals.size()) {
                Interval b = intervals.get(j);
                if (a.end >= b.start) {
                    a.end = a.end > b.end ? a.end : b.end;
                    j += 1;
                } else {
                    break;
                }
            }
            i = j;
            result.add(a);
        }
        return result;
    }
}
