package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class InsertionSortList {

    /**
     http://oj.leetcode.com/problems/insertion-sort-list/

     Sort a linked list using insertion sort.
     */
    public ListNode insertionSortList(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode p = head;
        p = p.next;
        // 新链表的起点
        ListNode newHead = head;
        newHead.next = null;

        while (p != null) {
            ListNode t = p.next;
            ListNode x = null;
            ListNode q = newHead;

            boolean inserted = false;
            while (q != null) {
                if (p.val >= q.val) {
                    x = q;
                    q = q.next;
                } else {
                    // 插入首部
                    if (x == null) {
                        p.next = newHead;
                        newHead = p;
                    } else {
                        x.next = p;
                        p.next = q;
                    }
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                x.next = p;
                p.next = null;
            }
            p = t;
        }
        return newHead;
    }

    public static void main(String[] args) {
        InsertionSortList s = new InsertionSortList();
        ListNode a = new ListNode(3);
        a.next = new ListNode(4);
        a.next.next = new ListNode(1);
        ListNode r = s.insertionSortList(a);
        System.out.println(r.val);
        System.out.println(r.next.val);
        System.out.println(r.next.next.val);
        System.out.println(r.next.next.next);
    }
}
