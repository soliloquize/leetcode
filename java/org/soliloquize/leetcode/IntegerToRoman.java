package org.soliloquize.leetcode;

import java.util.HashMap;
import java.util.Map;

public class IntegerToRoman {
    /**
     http://oj.leetcode.com/problems/integer-to-roman/

     Given an integer, convert it to a roman numeral.

     Input is guaranteed to be within the range from 1 to 3999.
     */

    private Map<Integer, String> map = new HashMap<Integer, String>();

    {
        map.put(1, "I");
        map.put(5, "V");
        map.put(10, "X");
        map.put(50, "L");
        map.put(100, "C");
        map.put(500, "D");
        map.put(1000, "M");
    }

    public String intToRoman(int num) {
        if (map.containsKey(num)) {
            return map.get(num);
        }

        String result = "";
        int low = 1;
        int mid = 5;
        int high = 10;
        while (num > 0) {
            int digit = num % 10;
            result = convert(digit, map.get(low), map.get(mid), map.get(high)) + result;
            num /= 10;
            low *= 10;
            mid *= 10;
            high *= 10;
        }
        return result;
    }


    private String convert(int num, String low, String mid, String high) {
        StringBuilder builder = new StringBuilder();
        if (num < 4) {
            for (int i = 0; i < num; i++) {
                builder.append(low);
            }
            return builder.toString();
        }
        if (num >= 4 && num <= 8) {
            for (int i = 0; i < 5 - num; i++) {
                builder.append(low);
            }
            builder.append(mid);
            for (int i = 0; i < num - 5; i++) {
                builder.append(low);
            }
            return builder.toString();
        }
        return builder.append(low).append(high).toString();
    }

    public static void main(String[] args) {
        IntegerToRoman s = new IntegerToRoman();
        System.out.println(s.intToRoman(3333));
    }

}
