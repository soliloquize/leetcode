package org.soliloquize.leetcode;

public class InterleavingString {
    /**
     http://oj.leetcode.com/problems/interleaving-string/

     Given s1, s2, s3, find whether s3 is formed by the interleaving of s1 and s2.

     For example,
     Given:
     s1 = "aabcc",
     s2 = "dbbca",

     When s3 = "aadbbcbcac", return true.
     When s3 = "aadbbbaccc", return false.
     */
    public boolean isInterleave(String s1, String s2, String s3) {
        // m[i][j] = true/false, true表示前i+j个字符能够被s1中前i个字符以及s2中前j个字符构建而成
        // m[i][j] = true if m[i-1][j] == true && s3[i+j-1] == s1[i-1]
        //                if m[i][j-1] == true && s3[i+j-1] == s2[j-1]
        if (s3.length() != (s1.length() + s2.length())) {
            return false;
        }

        if (s1.length() == 0) {
            return s2.equals(s3);
        }

        if (s2.length() == 0) {
            return s1.equals(s3);
        }

        boolean [][] m = new boolean[s1.length()+1][s2.length()+1];
        m[0][0] = true;

        for (int i = 1; i <= s1.length(); i++) {
            if (s3.charAt(i-1) != s1.charAt(i-1)) {
                break;
            }
            m[i][0] = true;
        }

        for (int j = 1; j <= s2.length(); j++) {
            if (s3.charAt(j-1) != s2.charAt(j-1)) {
                break;
            }
            m[0][j] = true;
        }

        for (int i = 1; i <= s1.length(); i++) {
            for (int j = 1; j <= s2.length(); j++) {
                m[i][j] = false;
                if (!m[i][j] && m[i-1][j] == true && s3.charAt(i+j-1) == s1.charAt(i-1)) {
                    m[i][j] = true;
                }
                if (!m[i][j] && m[i][j-1] == true && s3.charAt(i+j-1) == s2.charAt(j-1)) {
                    m[i][j] = true;
                }
            }
        }

        return m[s1.length()][s2.length()];
    }

    public static void main(String[] args) {
        InterleavingString s = new InterleavingString();
        boolean res = s.isInterleave("aa", "ab", "aaba");
        System.out.println(res);
    }
}
