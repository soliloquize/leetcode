package org.soliloquize.leetcode;

import java.util.*;

public class JumpGame {
    /**
     http://leetcode.com/oldoj#question_55

     Given an array of non-negative integers, you are initially positioned at the first index of the array.

     Each element in the array represents your maximum jump length at that position.

     Determine if you are able to reach the last index.

     For example:
     A = [2,3,1,1,4], return true.

     A = [3,2,1,0,4], return false.
     */

    public static void main(String[] args) {
        JumpGame s = new JumpGame();
        System.out.println(s.canJump(new int[]{2, 3, 1, 1, 4}));
        System.out.println(s.canJump(new int[] {3, 2, 1, 0, 4}));

        System.out.println(s.canJump2(new int[]{2, 3, 1, 1, 4}));
        System.out.println(s.canJump2(new int[] {3, 2, 1, 0, 4}));
    }

    public boolean canJump2(int[] A) {
        if (A.length == 0 || A.length == 1) {
            return true;
        }
        Deque<Integer> deque = new ArrayDeque<Integer>();
        Set<Integer> visited = new HashSet<Integer>();
        deque.add(0);
        while (!deque.isEmpty()) {
            int index = deque.poll();
            visited.add(index);

            if (index == A.length - 1) {
                return true;
            }
            int start = Math.max(0, index - A[index]);
            int end = Math.min(A.length - 1, index + A[index]);
            for (int i = start; i <= end; i++) {
                if (!visited.contains(i)) {
                    deque.add(i);
                }
            }
        }
        return false;
    }

    public boolean canJump(int[] A) {
        if (A == null || A.length == 0) {
            return false;
        }
        if (A.length == 1) {
            return true;
        }
        // 先判断是否存在可能性，如果每一个位置上最远都不能抵达最后一个元素，则无望
        boolean flag = false;
        for (int i = 0; i < A.length - 1; i++) {
            if (i + A[i] >= A.length - 1) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            return false;
        }
        boolean [] visited = new boolean[A.length];
        Arrays.fill(visited, false);
        ArrayDeque<Integer> deque = new ArrayDeque<Integer>(A.length);
        deque.add(0);
        while (deque.size() > 0) {
            int index = deque.pollFirst();
            visited[index] = true;
            int step = A[index];
            for (int i = 1; i <= step && index + i < A.length; i++) {
                int next = index + i;
                if (visited[next] == false) {
                    visited[next] = true;
                    deque.add(next);
                }
            }
            for (int i = 1; i <= step && index - i >= 0; i++) {
                int next = index - i;
                if (visited[next] == false) {
                    visited[next] = true;
                    deque.add(next);
                }
            }
            if (visited[A.length-1] == true) {
                break;
            }
        }
        return visited[A.length-1];
    }
}
