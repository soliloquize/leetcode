package org.soliloquize.leetcode;

import java.util.ArrayDeque;
import java.util.Arrays;

public class JumpGameII {

    /**
     http://leetcode.com/oldoj#question_45

     Given an array of non-negative integers, you are initially positioned at the first index of the array.

     Each element in the array represents your maximum jump length at that position.

     Your goal is to reach the last index in the minimum number of jumps.

     For example:
     Given array A = [2,3,1,1,4]

     The minimum number of jumps to reach the last index is 2. (Jump 1 step from index 0 to 1, then 3 steps to the last index.)
     */

    public static void main(String[] args) {
        JumpGameII s = new JumpGameII();
        System.out.println(s.jump(new int[] {2, 3, 1, 1, 4}));
        System.out.println(s.jump2(new int[] {2, 3, 1, 1, 4}));
    }

    public int jump2(int[] A) {
        int[] p = new int[A.length];
        p[0] = 0;

        for (int i = 1; i < A.length; i++) {
            p[i] = Integer.MAX_VALUE;
            for (int j = i - 1; j >= 0; j--) {
                if (A[j] + j >= i) {
                    p[i] = Math.min(p[i], p[j] + 1);
                }
            }
        }
        return p[A.length-1];
    }

    public class Item {
        int pos;
        int round;
        public Item(int pos, int round) {
            this.pos = pos;
            this.round = round;
        }
    }

    public int jump(int[] A) {
        if (A == null || A.length <= 1) {
            return 0;
        }
        boolean flag = false;
        for (int i = 0; i < A.length - 1; i++) {
            if (i + A[i] >= A.length - 1) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            return 0;
        }
        boolean [] visited = new boolean[A.length];
        Arrays.fill(visited, false);
        ArrayDeque<Item> deque = new ArrayDeque<Item>(A.length);
        deque.add(new Item(0, 0));
        while (deque.size() > 0) {
            Item item = deque.pollFirst();
            int index = item.pos;
            int round = item.round + 1;
            visited[index] = true;
            int step = A[index];
            int last = index + step;
            if (last >= A.length - 1) {
                last = A.length - 1;
            }
            for (int i = last; i > index; i--) {
                if (visited[i] == false) {
                    visited[i] = true;
                    deque.add(new Item(i, round));
                }
            }

            int start = index - step;
            if (start < 0) {
                start = 0;
            }
            for (int i = start; i < index; i++) {
                if (visited[i] == false) {
                    visited[i] = true;
                    deque.add(new Item(i, round));
                }
            }
            if (visited[A.length-1] == true) {
                return round;
            }
        }
        return 0;
    }
}
