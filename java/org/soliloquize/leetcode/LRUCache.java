package org.soliloquize.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LRUCache {
    /**
     http://oj.leetcode.com/problems/lru-cache/

     Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and set.

     get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
     set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
     */

    class Entry {
        public int k;
        public int v;
        public Entry before;
        public Entry after;
        Entry(int k, int v) {
            this.k = k;
            this.v = v;
        }
    }

    private int capacity;
    private Map<Integer, Entry> map;
    private Entry start;
    private Entry end;

    public LRUCache(int capacity) {
        this.capacity = capacity;
        this.map = new HashMap<Integer, Entry>();
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        Entry entry = map.get(key);
        recordAccess(entry);
        return entry.v;
    }

    public void set(int key, int value) {
        if (map.size() == capacity && !map.containsKey(key)) {
            removeElement(key, value);
        }
        if (!map.containsKey(key)) {
            Entry entry = new Entry(key, value);
            map.put(key, entry);
            recordAccess(entry);
        } else {
            Entry entry = map.get(key);
            entry.v = value;
            recordAccess(entry);
        }
    }

    private void removeElement(int key, int value) {
        int k = end.k;
        map.remove(k);
        end = end.before;
        if (end != null) {
            end.after = null;
        }
    }

    private void recordAccess(Entry entry) {
//        System.out.println(String.format("record entry %d", entry.k));
        if (start == null) {
            start = entry;
            end = entry;
            return;
        }

        if (entry == start) {
            return;
        }
        if (entry == end) {
            end = entry.before;
        }
        Entry b1 = entry.before;
        Entry a1 = entry.after;
        entry.before = null;
        entry.after = start;
        start.before = entry;
        start = entry;
        if (b1 != null) {
            b1.after = a1;
        }
        if (a1 != null) {
            a1.before = b1;
        }
    }

    public static void main(String[] args) {
        LRUCache s = new LRUCache(2);
        System.out.println(s.get(2));
        s.set(2, 6);
        System.out.println(s.get(1));
        s.set(1, 5);
        s.set(1, 2);
        System.out.println(s.get(1));
        System.out.println(s.get(2));
    }
}
