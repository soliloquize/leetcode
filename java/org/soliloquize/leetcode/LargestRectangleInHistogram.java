package org.soliloquize.leetcode;

public class LargestRectangleInHistogram {
    /**
     http://oj.leetcode.com/problems/largest-rectangle-in-histogram/
     Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, find the area of largest rectangle in the histogram.


     Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].


     The largest rectangle is shown in the shaded area, which has area = 10 unit.

     For example,
     Given height = [2,1,5,6,2,3],
     return 10.
     */

    public int largestRectangleArea(int[] height) {
        int max = 0;
        for (int i = 0; i < height.length; i++) {
            int right = 0;
            // 跳过连续相同的元素
            boolean stop = false;
            int equal = 0;
            for (int j = i + 1; j < height.length; j++, right += 1) {
                if (height[j] < height[i]) {
                    break;
                } else if (height[j] == height[i]) {
                    if (!stop) {
                        equal += 1;
                    }
                } else {
                    stop = true;
                }
            }
            int left = 0;
            for (int j = i - 1; j >= 0; j--, left += 1) {
                if (height[j] < height[i]) {
                    break;
                }
            }
            int tmp = (1 + left + right) * height[i];
            if (max < tmp) {
                max = tmp;
            }
            i += equal;
        }
        return max;
    }

    public static void main(String[] args)  {
        LargestRectangleInHistogram s = new LargestRectangleInHistogram();
        int res = s.largestRectangleArea(new int[] {0, 2, 0});
        System.out.println(res);
    }
}
