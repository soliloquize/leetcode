package org.soliloquize.leetcode;

public class LengthOfLastWord {

    /**
     http://leetcode.com/onlinejudge#question_58

     Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string.

     If the last word does not exist, return 0.

     Note: A word is defined as a character sequence consists of non-space characters only.

     For example,
     Given s = "Hello World",
     return 5.
     */
    public static void main(String[] args) {
        LengthOfLastWord s = new LengthOfLastWord();
        System.out.println(s.lengthOfLastWord2("Hello World"));
    }

    public int lengthOfLastWord2(String s) {
        int i = s.length() - 1;
        while (i >= 0 && !Character.isLetter(s.charAt(i))) {
            i -= 1;
        }
        int res = 0;
        while (i >= 0 && Character.isLetter(s.charAt(i))) {
            res += 1;
            i -= 1;
        }
        return res;
    }

    public int lengthOfLastWord(String s) {
        int result = 0;

        for (int i = s.length() - 1; i >= 0; i--) {
            if (Character.isSpaceChar(s.charAt(i))) {
                continue;
            } else {
                for (int j = i; j >= 0; j--) {
                    if (Character.isLetter(s.charAt(j))) {
                        result += 1;
                        continue;
                    } else {
                        break;
                    }
                }
                break;
            }
        }

        return result;
    }
}
