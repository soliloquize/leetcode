package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LetterCombinationsOfAPhoneNumber {
    /**
     http://leetcode.com/onlinejudge#question_17

     Given a digit string, return all possible letter combinations that the number could represent.

     A mapping of digit to letters (just like on the telephone buttons) is given below.



     Input:Digit string "23"
     Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
     Note:
     Although the above answer is in lexicographical order, your answer could be in any order you want.
     */

    private static Map<Character, String> map = new HashMap<Character, String>();

    static {
        map.put('2', "abc");
        map.put('3', "def");
        map.put('4', "ghi");
        map.put('5', "jkl");
        map.put('6', "mno");
        map.put('7', "pqrs");
        map.put('8', "tuv");
        map.put('9', "wxyz");
    }


    public static void main(String[] args) {
        LetterCombinationsOfAPhoneNumber s = new LetterCombinationsOfAPhoneNumber();
        System.out.println(s.letterCombinations("23"));
        System.out.println(s.letterCombinations2("23"));
    }

    public ArrayList<String> letterCombinations2(String digits) {
        ArrayList<String> result = new ArrayList<String>();
        letterCombinations2(digits, 0, new StringBuilder(), result);
        return result;
    }

    private void letterCombinations2(String digits, int index, StringBuilder current, ArrayList<String> result) {
        if (index == digits.length()) {
            result.add(current.toString());
            return;
        }
        char c = digits.charAt(index);
        if (!map.containsKey(c)) {
            return;
        }
        String chars = map.get(c);
        for (int j = 0; j < chars.length(); j++) {
            current.append(chars.charAt(j));
            letterCombinations2(digits, index + 1, current, result);
            current.deleteCharAt(current.length()-1);
        }
    }

    public ArrayList<String> letterCombinations(String digits) {
        ArrayList<String> result = new ArrayList<String>();
        travel(digits, 0, new StringBuilder(), result);
        return result;
    }

    private void travel(String digits, int index, StringBuilder current, ArrayList<String> result) {
        if (index == digits.length()) {
            String tmp = current.toString();
            if (tmp.length() == digits.length()) {
                result.add(tmp);
            }
            return;
        }
        for (int i = index; i < digits.length(); i++) {
            char c = digits.charAt(index);
            if (!map.containsKey(c)) {
                continue;
            }
            String choices = map.get(c);
            for (int j = 0; j < choices.length(); j++) {
                current.append(choices.charAt(j));
                travel(digits, i + 1, current, result);
                current.deleteCharAt(current.length()-1);
            }
        }
    }
}
