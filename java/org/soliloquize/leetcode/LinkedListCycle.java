package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class LinkedListCycle {
    /**
     http://oj.leetcode.com/problems/linked-list-cycle/

     Given a linked list, determine if it has a cycle in it.

     Follow up:
     Can you solve it without using extra space?
     */

    public boolean hasCycle2(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        ListNode p = head;
        ListNode q = head;

        while (q != null) {
            q = q.next;
            if (q == null) {
                break;
            }
            q = q.next;
            p = p.next;
            if (p == q) {
                return true;
            }
        }
        return false;
    }

    public boolean hasCycle(ListNode head) {
        ListNode p = head;
        ListNode q = head;

        int meet = 0;
        while (p != null && q != null) {
            if (p == q) {
                meet += 1;
            }
            if (meet == 2) {
                return true;
            }
            p = p.next;
            q = q.next;
            if (q != null) {
                q = q.next;
            }
        }
        return false;
    }
}
