package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class LinkedListCycleII {
    /**
     http://oj.leetcode.com/problems/linked-list-cycle-ii/

     Given a linked list, return the node where the cycle begins. If there is no cycle, return null.

     Follow up:
     Can you solve it without using extra space?
     */
    public ListNode detectCycle2(ListNode head) {
        ListNode p = head;
        ListNode q = head;

        while (q != null) {
            q = q.next;
            if (q == null) {
                return null;
            }
            q = q.next;
            p = p.next;
            if (p == q) {
                ListNode k = head;
                while (p != null) {
                    if (p == k) {
                        return p;
                    }
                    p = p.next;
                    k = k.next;
                }
            }
        }
        return null;
    }

    public ListNode detectCycle(ListNode head) {
        // head距离环开始节点距离为x
        // p行进距离为s：s = x + ｙ，（ｙ为在环中前进的距离）
        // q行进的距离为2s = x + ｙ + nr（q在环中必前进了n圈）
        // 则x = nr - y，那么从相遇之处在次出发，与起点处出发的必相交与环的起点
        ListNode p = head;
        ListNode q = head;

        int meet = 0;
        boolean flag = false;
        while (p != null && q != null) {
            if (p == q) {
                meet += 1;
            }
            if (meet == 2) {
                flag = true;
                break;
            }
            p = p.next;
            q = q.next;
            if (q != null) {
                q = q.next;
            }
        }
        if (!flag) {
            return null;
        }
        ListNode z = head;
        while (z != null && p != null) {
            if (z == p) {
                return z;
            }
            z = z.next;
            p = p.next;
        }
        return null;
    }
}
