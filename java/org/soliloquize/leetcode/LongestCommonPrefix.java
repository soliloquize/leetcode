package org.soliloquize.leetcode;

public class LongestCommonPrefix {

    /**
     http://leetcode.com/onlinejudge#question_14

     Write a function to find the longest common prefix string amongst an array of strings.
     */

    public static void main(String[] args) {
        LongestCommonPrefix s = new LongestCommonPrefix();
        System.out.println(s.longestCommonPrefix(new String[] {"jinqiang", "stefanie", "foobar"}));
    }

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        } else if (strs.length == 1) {
            return strs[0];
        } else if (strs.length == 2) {
            return longestCommonPrefix(strs[0], strs[1]);
        } else {
            String prefix = longestCommonPrefix(strs[0], strs[1]);
            for (int i = 2; i < strs.length; i++) {
                prefix = longestCommonPrefix(prefix, strs[i]);
            }
            return prefix;
        }
    }

    private String longestCommonPrefix(String a, String b) {
        int i = 0;
        while (i < a.length() && i < b.length()) {
            if (a.charAt(i) == b.charAt(i)) {
                i += 1;
            } else {
                break;
            }
        }
        return a.substring(0, i);
    }
}
