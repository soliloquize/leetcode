package org.soliloquize.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LongestConsecutiveSequence {
    /**
     http://leetcode.com/oldoj#question_128

     Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

     For example,
     Given [100, 4, 200, 1, 3, 2],
     The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

     Your algorithm should run in O(n) complexity.
     */
    public int longestConsecutive2(int[] num) {
        int res = 1;
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();

        for (int i = 0; i < num.length; i++) {
            if (map.containsKey(num[i])) {
                continue;
            }
            map.put(num[i], 1);

            int tmp = 1;
            if (map.containsKey(num[i]+1)) {
                tmp = merge2(map, num[i], num[i] + 1);
            }
            if (map.containsKey(num[i]-1)) {
                tmp = merge2(map, num[i]-1, num[i]);
            }
            res = Math.max(res, tmp);
        }
        return res;
    }

    private int merge2(Map<Integer, Integer> map, int small, int large) {
        int low = small - map.get(small) + 1;
        int up = large + map.get(large) - 1;
        int len = up - low + 1;

        map.put(low, len);
        map.put(up, len);

        return len;
    }


    public int longestConsecutive(int[] num) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        int result = 1;

        for (int i : num) {
            // 不处理重复数字
            if (map.containsKey(i)) {
                continue;
            }
            // 单个数字最长连续为1
            map.put(i, 1);

            if (map.containsKey(i + 1)) {
                result = Math.max(result, merge(map, i, i + 1));
            }

            if (map.containsKey(i - 1)) {
                result = Math.max(result, merge(map, i - 1, i));
            }
        }

        return result;
    }

    /**
     * 合并分段的数字，只有段的两端需要合并
     */
    private int merge(Map<Integer, Integer> map, int small, int large) {
        int upper = large + map.get(large) - 1;
        int lower = small - map.get(small) + 1;
        int len = upper - lower + 1;

        // 长度计数更新到两端的数字上
        map.put(upper, len);
        map.put(lower, len);
        return len;
    }

    public static void main(String[] args) {
        LongestConsecutiveSequence s = new LongestConsecutiveSequence();
        int res = s.longestConsecutive(new int[] {0, 0});
        System.out.println(res);
    }
}
