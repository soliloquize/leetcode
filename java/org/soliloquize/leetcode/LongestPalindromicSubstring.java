package org.soliloquize.leetcode;

public class LongestPalindromicSubstring {

    /**
     http://leetcode.com/onlinejudge#question_5

     Given a string S, find the longest palindromic substring in S.
     You may assume that the maximum length of S is 1000, and
     there exists one unique longest palindromic substring.
     */

    public static void main(String[] args) {
        LongestPalindromicSubstring s = new LongestPalindromicSubstring();
        System.out.println(s.longestPalindrome("nancy"));
        System.out.println(s.longestPalindrome2("nancy"));
    }


    public String longestPalindrome2(String input) {
        int max = 0;
        int left = 0;
        int right = 0;
        char[] chars = input.toCharArray();
        for (int k = 0; k < input.length(); k++) {
            int i = k - 1;
            int j = k + 1;
            int length = 1;
            while (i >= 0 && j < input.length() && chars[i--] == chars[j++]) {
                length += 2;
            }
            if (max < length) {
                max = length;
                left = i + 1;
                right = j - 1;
            }

            i = k - 1;
            j = k;
            length = 0;
            while (i >= 0 && j < input.length() && chars[i--] == chars[j++]) {
                length += 2;
            }
            if (max < length) {
                max = length;
                left = i + 1;
                right = j - 1;
            }
        }
        return new String(chars, left, right - left + 1);
    }

    public String longestPalindrome(String input) {
        int left = 0;
        int right = 0;
        int max = 0;
        for (int k = 0; k < input.length(); k++) {
            // 奇数长度回文
            int i = k - 1;
            int j = k + 1;
            while (i >= 0 && j < input.length()) {
                if (input.charAt(i) != input.charAt(j)) {
                    break;
                }
                i -= 1;
                j += 1;
            }
            int current = j - i + 1;
            if (max < current) {
                max = current;
                left = i + 1;
                right = j - 1;
            }

            // 偶数长度回文
            i = k;
            j = k + 1;
            while (i >= 0 && j < input.length()) {
                if (input.charAt(i) != input.charAt(j)) {
                    break;
                }
                i -= 1;
                j += 1;
            }
            current = j - i + 1;
            if (max < current) {
                max = current;
                left = i + 1;
                right = j - 1;
            }

        }
        return input.substring(left, right + 1);
    }
}
