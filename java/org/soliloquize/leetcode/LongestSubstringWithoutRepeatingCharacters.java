package org.soliloquize.leetcode;

import java.util.Arrays;

public class LongestSubstringWithoutRepeatingCharacters {

    /**
     http://leetcode.com/onlinejudge#question_3

     Given a string, find the length of the longest substring without repeating characters.
     For example, the longest substring without repeating letters for "abcabcbb" is "abc",
     which the length is 3. For "bbbbb" the longest substring is "b", with the length of 1.
     */

    public static void main(String[] args) {
        LongestSubstringWithoutRepeatingCharacters s = new LongestSubstringWithoutRepeatingCharacters();
        System.out.println(s.lengthOfLongestSubstring("abcabcbb"));
        System.out.println(s.lengthOfLongestSubstring("bbbbb"));
        System.out.println("->");
        System.out.println(s.lengthOfLongestSubstring2("abcabcbb"));
        System.out.println(s.lengthOfLongestSubstring2("bbbbb"));
    }

    public int lengthOfLongestSubstring2(String s) {
        if (s.length() <= 1) {
            return s.length();
        }
        int i = 0;
        int j = 1;
        int res = 0;
        boolean[] flags = new boolean[26];
        flags[s.charAt(0) - 'a'] = true;
        for (; j < s.length(); j++) {
            char c = s.charAt(j);
            if (flags[c - 'a'] == false) {
                flags[c - 'a'] = true;
                res = Math.max(res, j - i + 1);
            } else {
                res = Math.max(res, j - i);
                while (i < s.length() && s.charAt(i) != c) {
                    flags[i] = false;
                    i += 1;
                }
                i += 1;
            }
        }
        return res;
    }

    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        if (s.length() == 1) {
            return 1;
        }

        boolean [] flag = new boolean[26];
        Arrays.fill(flag, false);

        int i = 0;
        int j = 1;

        int max = 1;

        // 用i，j两个游标记录
        // j往后移动，每访问一个元素，标识是否出现过，一旦遇到重复字符则停止，更新当前长度
        // i往后移动知道遇到重复字符为止
        flag[s.charAt(i) - 'a'] = true;
        for (j = 1; j < s.length() && i < s.length(); j++) {
            if (flag[s.charAt(j) - 'a'] == false) {
                flag[s.charAt(j) - 'a'] = true;
            } else {
                max = max < (j - i) ? j - i : max;
                while (i < s.length()) {
                    if (s.charAt(i) != s.charAt(j)) {
                        flag[s.charAt(i) - 'a'] = false;
                    } else {
                        break;
                    }
                    i += 1;
                }
                i += 1;
            }
        }
        max = max < (j - i) ? j - i : max;

        return max;
    }
}
