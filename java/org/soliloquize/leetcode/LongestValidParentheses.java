package org.soliloquize.leetcode;

import java.util.*;

public class LongestValidParentheses {
    /**
     http://leetcode.com/oldoj#question_32

     Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.

     For "(()", the longest valid parentheses substring is "()", which has length = 2.

     Another example is ")()())", where the longest valid parentheses substring is "()()", which has length = 4.
     */

    public static class Item {
        int index;
        char c;
        public Item(int index, char c) {
            this.index = index;
            this.c = c;
        }
    }

    public static class Range implements Comparable<Range> {
        int start;
        int end;
        public Range(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public int compareTo(Range o) {
            if (start != o.start) {
                return start - o.start;
            }
            return end - o.end;
        }

        public String toString() {
            return "[" + start + ", " + end + "]";
        }
    }

    public int longestValidParentheses(String s) {
        List<Item> list = new ArrayList<Item>();
        List<Range> ranges = new ArrayList<Range>();

        /**
         * 用stack记录每一个合法的区间
         */
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                list.add(new Item(i, c));
            } else {
                if (list.size() > 0) {
                    Item last = list.get(list.size()-1);
                    if (last.c == '(') {
                        ranges.add(new Range(last.index, i));
                        list.remove(list.size()-1);
                    }
                }
            }
        }
        /**
         * 按区间起始位置排序，合并相连接的区间，计算合并后最长区间长度
         */
        Collections.sort(ranges);
        int max = 0;
        for (int i = 0; i < ranges.size(); ) {
            Range r = ranges.get(i);
            int start = r.start;
            int end = r.end;
            int j = i + 1;
            for (; j < ranges.size(); j++) {
                if (ranges.get(j).start > end + 1) {
                    break;
                }
                end = end > ranges.get(j).end ? end : ranges.get(j).end;
            }
            i = j;
            if (max < (end - start + 1)) {
                max = end - start + 1;
            }
        }
        return max;
    }

    public int longestValidParentheses2(String s) {
        Stack<Integer> stack = new Stack<Integer>();
        boolean[] flag = new boolean[s.length()];
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(i);
            } else {
                if (stack.size() == 0) {
                    continue;
                }
                int index = stack.pop();
                flag[index] = true;
                flag[i] = true;
            }
        }
        int res = 0;
        int current = 0;
        for (int i = 0; i < flag.length; i++) {
            if (flag[i]) {
                current += 1;
                res = Math.max(res, current);
            } else {
                current = 0;
            }
        }
        return res;
    }

    public int longestValidParentheses3(String s) {
        boolean[] flags = new boolean[s.length()];
        Deque<Integer> stack = new ArrayDeque<Integer>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(') {
                stack.push(i);
            } else {
                if (stack.isEmpty()) {
                    continue;
                }
                int index = stack.pop();
                flags[index] = true;
                flags[i] = true;
            }
        }
        int res = 0;
        int tmp = 0;
        for (int i = 0; i < flags.length; i++) {
            if (flags[i]) {
                tmp += 1;
                res = Math.max(res, tmp);
            } else {
                tmp = 0;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        LongestValidParentheses s = new LongestValidParentheses();
        System.out.println(s.longestValidParentheses2(")()())"));
        System.out.println(s.longestValidParentheses2("(()"));
        System.out.println(s.longestValidParentheses2("()(()"));
        System.out.println("->");
        System.out.println(s.longestValidParentheses3(")()())"));
        System.out.println(s.longestValidParentheses3("(()"));
        System.out.println(s.longestValidParentheses3("()(()"));
    }
}
