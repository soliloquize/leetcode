package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.Point;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MaxPointsOnALine {
    /**
     http://oj.leetcode.com/problems/max-points-on-a-line/

     Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
     */

    private int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    class Record {
        public int a;
        public int b;
        public int x;
        public Record(int a, int b, int x) {
            this.a = a;
            this.b = b;
            this.x = x;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Record record = (Record) o;

            if (a != record.a) return false;
            if (b != record.b) return false;
            if (x != record.x) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = a;
            result = 31 * result + b;
            result = 31 * result + x;
            return result;
        }

        @Override
        public String toString() {
            return "Record{" +
                    "a=" + a +
                    ", b=" + b +
                    ", x=" + x +
                    '}';
        }
    }

    class RealNumber {
        public int a;
        public int b;
        public RealNumber(int a, int b) {
            // 处理b为0的情况
            if (b == 0) {
                this.a = 0;
                this.b = 0;
                return;
            }
            // 归一化，负数则将符号设在a上
            boolean positive = (a > 0 && b > 0) || (a < 0 && b < 0);
            a = Math.abs(a);
            b = Math.abs(b);
            int tmp = gcd(a, b);
            this.a = a / tmp;
            this.b = b / tmp;
            if (!positive) {
                this.a *= -1;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RealNumber that = (RealNumber) o;

            if (a != that.a) return false;
            if (b != that.b) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = a;
            result = 31 * result + b;
            return result;
        }

        @Override
        public String toString() {
            return "RealNumber{" +
                    "a=" + a +
                    ", b=" + b +
                    '}';
        }
    }

    class Line {
        RealNumber k;
        RealNumber b;

        public Line(RealNumber k, RealNumber b) {
            this.k = k;
            this.b = b;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Line line = (Line) o;

            if (b != null ? !b.equals(line.b) : line.b != null) return false;
            if (k != null ? !k.equals(line.k) : line.k != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = k != null ? k.hashCode() : 0;
            result = 31 * result + (b != null ? b.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Line{" +
                    "k=" + k +
                    ", b=" + b +
                    '}';
        }
    }

    public int maxPoints(Point[] points) {
        if (points.length == 1) {
            return 1;
        }
        Map<Line, Set<Record>> map = new HashMap<Line, Set<Record>>();

        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                Line line = null;

                int k1 = points[j].y - points[i].y;
                int k2 = points[j].x - points[i].x;

                // x轴平行线
                if (k1 == 0) {
                    line = new Line(new RealNumber(0, 0), new RealNumber(points[i].y, 1));
                }
                // y轴平行线
                else if (k2 == 0) {
                    line = new Line(new RealNumber(points[i].x, 1), new RealNumber(Integer.MIN_VALUE, Integer.MIN_VALUE));

                } else {
                    RealNumber k = new RealNumber(k1, k2);
                    int b1 = points[j].x * points[i].y - points[i].x * points[j].y;
                    int b2 = points[j].x - points[i].x;
                    RealNumber b = new RealNumber(b1, b2);
                    line = new Line(k, b);
                }
                if (!map.containsKey(line)) {
                    map.put(line, new HashSet<Record>());
                }
                map.get(line).add(new Record(points[i].x, points[i].y, i));
                map.get(line).add(new Record(points[j].x, points[j].y, j));
            }
        }
        int res = 0;
        for (Map.Entry<Line,  Set<Record>> entry : map.entrySet()) {
            res = Math.max(res, entry.getValue().size());
        }
        return res;
    }

    public static void main(String [] args) {

        MaxPointsOnALine s = new MaxPointsOnALine();
        Point[] ps = new Point[] {
                new Point(0,-12),
                new Point(5,2),
                new Point(2,5),
                new Point(0,-5),
                new Point(1,5),
                new Point(2,-2),
                new Point(5,-4),
                new Point(3,4),
                new Point(-2,4),
                new Point(-1,4),
                new Point(0,-5),
                new Point(0,-8),
                new Point(-2,-1),
                new Point(0,-11),
                new Point(0,-9)
        };
        System.out.println(s.maxPoints(ps));

    }
}
