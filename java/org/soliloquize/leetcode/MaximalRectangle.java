package org.soliloquize.leetcode;

public class MaximalRectangle {
    /**
     http://oj.leetcode.com/problems/maximal-rectangle/

     Given a 2D binary matrix filled with 0's and 1's, find the largest rectangle containing all ones and return its area.
     */

    public int maximalRectangle2(char[][] matrix) {
        int m = matrix.length;
        if (m == 0) {
            return 0;
        }
        int n = matrix[0].length;
        int [][] numbers = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    numbers[i][j] = 1;
                } else {
                    numbers[i][j] = -m*n;
                }
            }
        }
        int [][] sums = new int[m][n];
        for (int j = 0; j < n; j++) {
            int tmp = 0;
            for (int i = 0; i < m; i++) {
                tmp += matrix[i][j];
                sums[i][j] = tmp;
            }
        }

        int result = 0;
        for (int i = 0; i < m; i++) {
            for (int j = i; j < m; j++) {
                int current = 0;
                for (int k = 0; k < n; k++) {
                    int tmp = sums[j][k] - sums[i][k] + matrix[i][k];
                    current += tmp;
                    if (current < 0) {
                        current = 0;
                    } else {
                        if (result < current) {
                            result = current;
                        }
                    }
                }
            }
        }
        return result;
    }

    public int maximalRectangle(char[][] matrix) {
        // 类似最大子矩阵之和
        int m = matrix.length;
        if (m == 0) {
            return 0;
        }
        int n = matrix[0].length;
        int[][] numbers = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == '1') {
                    numbers[i][j] = 1;
                } else {
                    // 将0设为不可能的数
                    numbers[i][j] = -m * n;
                }
            }
        }

        int[][] sum = new int[m][n];
        for (int j = 0; j < n; j++) {
            int tmp = 0;
            for (int i = 0; i < m; i++) {
                tmp += numbers[i][j];
                sum[i][j] = tmp;
            }
        }

        int max = 0;
        for (int i = 0; i < m; i++) {
            for (int j = i; j < m; j++) {
                int current = 0;
                for (int k = 0; k < n; k++) {
                    int tmp = sum[j][k] - sum[i][k] + numbers[i][k];
                    current += tmp;
                    if (current < 0) {
                        current = 0;
                    } else {
                        if (current > max) {
                            max = current;
                        }
                    }
                }
            }
        }

        return max;
    }
}
