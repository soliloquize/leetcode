package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.Constructor;
import org.soliloquize.leetcode.common.TreeNode;

public class MaximumDepthOfBinaryTree {
    /**
     http://leetcode.com/onlinejudge#question_104

     Given a binary tree, find its maximum depth.

     The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
     */

    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        int left = Integer.MIN_VALUE;
        int right = Integer.MIN_VALUE;
        if (root.left != null) {
            left = maxDepth(root.left);
        }
        if (root.right != null) {
            right = maxDepth(root.right);
        }
        return Math.max(left, right) + 1;
    }

    public static void main(String[] args) {
        MaximumDepthOfBinaryTree s = new MaximumDepthOfBinaryTree();
        System.out.println(s.maxDepth(Constructor.buildTree(new int[]{5, 4, 8, 11, Integer.MIN_VALUE, 13, 4, 7, 2, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, 1})));
    }
}
