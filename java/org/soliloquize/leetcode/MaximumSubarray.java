package org.soliloquize.leetcode;

public class MaximumSubarray {

    /**
     http://leetcode.com/onlinejudge#question_53

     Find the contiguous subarray within an array (containing at least one number) which has the largest sum.

     For example, given the array [−2,1,−3,4,−1,2,1,−5,4],
     the contiguous subarray [4,−1,2,1] has the largest sum = 6.
     */
    public int maxSubArray(int[] A) {
        int max = Integer.MIN_VALUE;
        int current = 0;

        for (int i = 0; i < A.length; i++) {
            current += A[i];
            max = max < current ? current : max;
            if (current < 0) {
                current = 0;
            }
        }

        return max;
    }

    public int maxSubArray2(int[] A) {
        int res = Integer.MIN_VALUE;
        int current = 0;
        for (int i = 0; i < A.length; i++) {
            current += A[i];
            res = Math.max(current, res);
            if (current < 0) {
                current = 0;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        MaximumSubarray s = new MaximumSubarray();
        System.out.println(s.maxSubArray(new int[] {-2, 1, -3, 4, -1, 2, 1, -5, 4}));
        System.out.println(s.maxSubArray2(new int[] {-2, 1, -3, 4, -1, 2, 1, -5, 4}));
    }
}
