package org.soliloquize.leetcode;

public class MedianOfTwoSortedArrays {

    /**
     http://leetcode.com/onlinejudge#question_4

     There are two sorted arrays A and B of size m and n respectively.
     Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
     */
    public double findMedianSortedArrays(int A[], int B[]) {
        int total = A.length + B.length;
        if (total % 2 != 0) {
            return find(A, 0, A.length, B, 0, B.length, total / 2 + 1);
        }
        return (find(A, 0, A.length, B, 0, B.length, total / 2) + find(A, 0, A.length, B, 0, B.length, total / 2 + 1)) / 2;
    }

    private double find(int A[], int as, int m, int B[], int bs, int n, int k) {
        // 确保A中长度更少
        if (m > n) {
            return find(B, bs, n, A, as, m, k);
        }
        // 已经访问到最后
        if (m == 0) {
            return B[k-1];
        }
        // k为1时可直接判定
        if (k == 1) {
            return Math.min(A[as], B[bs]);
        }
        // 分成两部分处理
        int a = Math.min(k / 2, m);
        int b = k - a;

        // 如果A中元素小，则截止目前的元素肯定在前k个中，接着找剩余数字中第k - a个元素，反之亦然
        if (A[as + a - 1] < B[bs + b - 1]) {
            return find(A, as + a, m - a, B, bs, n, k - a);
        } else if (A[as + a - 1] > B[bs + b - 1]) {
            return find(A, as, m, B, bs + b, n - b, k - b);
        }
        return A[as + a - 1];
    }

    public double findMedianSortedArrays2(int A[], int B[]) {
        int total = A.length + B.length;
        if (total % 2 != 0) {
            return search(A, 0, A.length, B, 0, B.length, total / 2 + 1);
        }
        return (search(A, 0, A.length, B, 0, B.length, total / 2) + search(A, 0, A.length, B, 0, B.length, total / 2 + 1)) / 2;
    }

    private double search(int[] A, int astart, int m, int [] B, int bstart, int n, int k) {
        if (m > n) {
            return search(B, bstart, n, A, astart, m, k);
        }
        int a = Math.min(k / 2, m);
        int b = k - a;
        // 已经访问到最后
        if (m == 0) {
            return B[k-1];
        }
        // k为1时可直接判定
        if (k == 1) {
            return Math.min(A[astart], B[bstart]);
        }
        if (A[astart + a - 1] < B[bstart + b - 1]) {
            return find(A, astart + a, m - a, B, bstart, n, k - a);
        } else {
            return find(A, astart, m, B, bstart + b, n - b, k - b);
        }
    }

    public static void main(String[] args) {
        MedianOfTwoSortedArrays s = new MedianOfTwoSortedArrays();
        System.out.println(s.findMedianSortedArrays(new int[]{3}, new int[]{1, 2}));
        System.out.println(s.findMedianSortedArrays2(new int[]{3}, new int[]{1, 2}));
    }
}
