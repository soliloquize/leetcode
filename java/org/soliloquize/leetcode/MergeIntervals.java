package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MergeIntervals {

    /**
     * http://leetcode.com/onlinejudge#question_56
     * <p/>
     * Given a collection of intervals, merge all overlapping intervals.
     * <p/>
     * For example,
     * Given [1,3],[2,6],[8,10],[15,18],
     * return [1,6],[8,10],[15,18].
     */
    public class Interval {
        int start;
        int end;

        Interval() {
            start = 0;
            end = 0;
        }

        Interval(int s, int e) {
            start = s;
            end = e;
        }
    }

    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                if (o1.start < o2.start) {
                    return -1;
                } else if (o1.start > o2.start) {
                    return 1;
                } else {
                    return o1.end - o2.end;
                }
            }
        });
        ArrayList<Interval> result = new ArrayList<Interval>();
        for (int i = 0; i < intervals.size(); ) {
            Interval a = intervals.get(i);
            int j = i + 1;
            while (j < intervals.size()) {
                Interval b = intervals.get(j);
                if (a.end >= b.start) {
                    a.end = a.end > b.end ? a.end : b.end;
                    j += 1;
                } else {
                    break;
                }
            }
            i = j;
            result.add(a);
        }
        return result;
    }
}
