package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

import java.util.ArrayList;

public class MergeKSortedLists {

    /**
     http://leetcode.com/onlinejudge#question_23

     Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.
     */
    public ListNode mergeKLists(ArrayList<ListNode> lists) {
        if (lists == null) {
            return null;
        }

        ListNode header = null;
        ListNode current = null;

        while (lists.size() > 0) {
            ListNode min = null;
            int pos = -1;
            for (int i = 0; i < lists.size(); i++) {
                ListNode tmp = lists.get(i);
                if (tmp == null) {
                    lists.remove(i);
                    i -= 1;
                    continue;
                }
                if (min == null) {
                    min = tmp;
                    pos = i;
                } else if (min.val > tmp.val){
                    min = tmp;
                    pos = i;
                }
            }

            if (min == null) {
                break;
            }

            // 更新结果
            if (header == null) {
                header = new ListNode(min.val);
                current = header;
            } else {
                current.next = new ListNode(min.val);
                current = current.next;
            }

            // 移动选中的节点
            if (min.next == null) {
                lists.remove(pos);
            } else {
                lists.set(pos, min.next);
            }
        }
        return header;
    }
}
