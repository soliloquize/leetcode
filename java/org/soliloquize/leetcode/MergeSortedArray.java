package org.soliloquize.leetcode;

public class MergeSortedArray {
    /**
     http://leetcode.com/onlinejudge#question_88

     Given two sorted integer arrays A and B, merge B into A as one sorted array.

     Note:
     You may assume that A has enough space to hold additional elements from B.
     The number of elements initialized in A and B are m and n respectively.
     */

    public static void main(String[] args) {
        MergeSortedArray s = new MergeSortedArray();
        int A[] = new int[] {0};
        int B[] = new int[] {1};
        s.merge2(A, 0, B, 1);
        for (int i = 0; i < A.length; i++) {
            System.out.println(A[i]);
        }
    }

    public void merge3(int A[], int m, int B[], int n) {
        int start = A.length - 1;
        for (int i = m-1, j = A.length - 1; i >= 0; i--, j--) {
            A[j] = A[i];
            start = j;
        }
        int i = start;
        int j = 0;
        int k = 0;
        while (i < A.length && j < n) {
            if (A[i] < B[j]) {
                A[k++] = A[i++];
            } else {
                A[k++] = A[j++];
            }
        }
        while (i < A.length) {
            A[k++] = A[i++];
        }
        while (j < n) {
            A[k++] = B[j++];
        }
    }


    public void merge2(int A[], int m, int B[], int n) {
        int start = A.length;
        for (int i = m - 1, j = A.length-1; i >= 0; i--, j--) {
            A[j] = A[i];
            start = j;
        }
        int i = start;
        int j = 0;
        int p = 0;
        while (i < A.length && j < n) {
            if (A[i] < B[j]) {
                A[p++] = A[i++];
            } else {
                A[p++] = B[j++];
            }
        }
        while (i < A.length) {
            A[p++] = A[i++];
        }
        while (j < n) {
            A[p++] = B[j++];
        }
    }

    public void merge(int A[], int m, int B[], int n) {
        int i = 0;
        int j = 0;
        int res[] = new int[m + n];
        int p = 0;
        while (i < m && j < n) {
            if (A[i] < B[j]) {
                res[p++] = A[i++];
            } else {
                res[p++] = B[j++];
            }
        }
        while (i < m) {
            res[p++] = A[i++];
        }
        while (j < n) {
            res[p++] = B[j++];
        }
        for (int k = 0; k < p; k++) {
            A[k] = res[k];
        }
    }
}
