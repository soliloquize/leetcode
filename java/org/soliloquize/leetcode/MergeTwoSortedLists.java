package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class MergeTwoSortedLists {
    /**
     http://leetcode.com/oldoj#question_21

     Merge two sorted linked lists and return it as a new list.
     The new list should be made by splicing together the nodes of the first two lists.
     */
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        ListNode head = null;
        ListNode p = null;
        ListNode q = null;

        // 选定起始节点
        if (l1.val < l2.val) {
            head = l1;
            p = l1.next;
            q = l2;
        } else {
            head = l2;
            p = l1;
            q = l2.next;
        }
        ListNode last = head;

        // 两个链表都还有元素
        while (p != null && q != null) {
            if (p.val <= q.val) {
                last.next = p;
                last = p;
                p = p.next;
            } else {
                last.next = q;
                last = q;
                q = q.next;
            }
        }

        // 处理尾部
        if (p != null) {
            last.next = p;
        }

        if (q != null) {
            last.next = q;
        }

        return head;
    }
}
