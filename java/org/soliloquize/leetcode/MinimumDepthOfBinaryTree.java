package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class MinimumDepthOfBinaryTree {

    /**
     http://leetcode.com/onlinejudge#question_111

     Given a binary tree, find its minimum depth.

     The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
     */

    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        int left = Integer.MAX_VALUE;
        int right = Integer.MAX_VALUE;
        if (root.left != null) {
            left = minDepth(root.left);
        }
        if (root.right != null) {
            right = minDepth(root.right);
        }
        return Math.min(left, right) + 1;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(4);
        root.left.left = new TreeNode(11);
        root.left.left.left = new TreeNode(7);
        root.left.left.right = new TreeNode(2);
        root.right = new TreeNode(8);
        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(4);
        root.right.right.right = new TreeNode(1);

        MinimumDepthOfBinaryTree s = new MinimumDepthOfBinaryTree();
        System.out.println(s.minDepth(root));
        System.out.println(s.minDepth2(root));
    }

    public int minDepth2(TreeNode root) {
        depth = Integer.MAX_VALUE;
        search(root, 0);
        return depth;
    }

    private void search(TreeNode node, int current) {
        current += 1;
        if (node.left == null || node.right == null) {
            depth = Math.min(current, depth);
        }
        if (node.left != null) {
            search(node.left, current + 1);
        }
        if (node.right != null) {
            search(node.right, current + 1);
        }
    }


    private int depth = Integer.MAX_VALUE;

    public int minDepth_(TreeNode root) {
        depth = Integer.MAX_VALUE;
        if (root == null) {
            return 0;
        }
        travel(root, 1);
        return depth;
    }

    private void travel(TreeNode root, int depth) {
        if (root.left == null && root.right == null) {
            this.depth = (this.depth > depth) ? depth : this.depth;
        }
        else {
            if (root.left != null) {
                travel(root.left, depth + 1);
            }
            if (root.right != null) {
                travel(root.right, depth + 1);
            }
        }
    }
}
