package org.soliloquize.leetcode;

import java.util.Arrays;
import java.util.PriorityQueue;

public class MinimumPathSum {
    /**
     http://leetcode.com/oldoj#question_64

     Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right
     which minimizes the sum of all numbers along its path.

     Note: You can only move either down or right at any point in time.
     */

    public static void main(String[] args) {
        MinimumPathSum s = new MinimumPathSum();
        int[][] grid = new int[][] {
            new int[] {1, 2, 3, 4},
            new int[] {1, 2, 3, 4},
            new int[] {1, 2, 3, 4},
            new int[] {1, 2, 3, 4},
        };
        System.out.println(s.minPathSum(grid));
        System.out.println(s.minPathSum2(grid));
    }

    public int minPathSum2(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;
        int[][] p = new int[m][n];
        p[0][0] = grid[0][0];

        for (int i = 0, j = 1; j < n; j++) {
            p[i][j] = p[i][j-1] + grid[i][j];
        }
        for (int i = 1, j = 0; i < m; i++) {
            p[i][j] = p[i-1][j] + grid[i][j];
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                p[i][j] = Math.min(p[i-1][j], p[i][j-1]) + grid[i][j];
            }
        }
        return p[m-1][n-1];
    }


    class Pos implements Comparable<Pos> {
        int x;
        int y;
        int distance;
        public Pos() {}
        public Pos(int x, int y, int distance) {
            this.x = x;
            this.y = y;
            this.distance = distance;
        }
        @Override
        public int compareTo(Pos o) {
            return distance - o.distance;
        }
    }

    public int minPathSum(int[][] grid) {
        if (grid.length == 0) {
            return 0;
        }
        int[][] distance = new int[grid.length][grid[0].length];
        for (int i = 0; i < distance.length; i++) {
            Arrays.fill(distance[i], Integer.MAX_VALUE);
        }
        PriorityQueue<Pos> heap = new PriorityQueue<Pos>();
        heap.add(new Pos(0, 0, grid[0][0]));
        distance[0][0] = grid[0][0];
        while (heap.size() > 0) {
            Pos pos = heap.poll();
            if (pos.x + 1 < grid.length) {
                int tmp = distance[pos.x][pos.y] + grid[pos.x+1][pos.y];
                if (distance[pos.x+1][pos.y] > tmp) {
                    distance[pos.x+1][pos.y] = tmp;
                    heap.add(new Pos(pos.x+1, pos.y, tmp));
                }
            }
            if (pos.y + 1 < grid[pos.x].length) {
                int tmp = distance[pos.x][pos.y] + grid[pos.x][pos.y+1];
                if (distance[pos.x][pos.y+1] > tmp) {
                    distance[pos.x][pos.y+1] = tmp;
                    heap.add(new Pos(pos.x, pos.y+1, tmp));
                }
            }
        }
        return distance[grid.length-1][grid[0].length-1];
    }
}
