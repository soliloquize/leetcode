package org.soliloquize.leetcode;

import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubstring {
    /**
     http://oj.leetcode.com/problems/minimum-window-substring/

     Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

     For example,
     S = "ADOBECODEBANC"
     T = "ABC"
     Minimum window is "BANC".

     Note:
     If there is no such window in S that covers all characters in T, return the emtpy string "".

     If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
     */

    private Character lastInvalid = null;

    public String minWindow(String S, String T) {
        String result = "";
        Map<Character, Integer> charMap = countChar(T);
        Map<Character, Integer> currentCharMap = new HashMap<Character, Integer>();
        Map<Character, Integer> totalCharMap = countChar(S);

        if (totalCharMap.keySet().size() < charMap.keySet().size()) {
            return result;
        }

        for (Map.Entry<Character, Integer> entry : charMap.entrySet()) {
            if (!totalCharMap.containsKey(entry.getKey())) {
                return result;
            }
        }

        int i = 0;
        int j = 0;

        lastInvalid = null;
        while (j < S.length()) {
            boolean found = false;
            for (; j < S.length(); j++) {
                char c = S.charAt(j);
                if (!charMap.containsKey(c)) {
                    continue;
                }
                if (!currentCharMap.containsKey(c)) {
                    currentCharMap.put(c, 1);
                } else {
                    currentCharMap.put(c, currentCharMap.get(c) + 1);
                }
                if (isValid(currentCharMap, charMap)) {
                    j += 1;
                    found = true;
                    break;
                }
            }

            if (!found) {
                break;
            }

            for (; i < j - T.length() + 1; i++) {
                char c = S.charAt(i);
                if (!charMap.containsKey(c)) {
                    continue;
                }
                int tmp = currentCharMap.get(c);
                if (tmp == 1) {
                    currentCharMap.remove(c);
                } else {
                    currentCharMap.put(c, currentCharMap.get(c) - 1);
                }
                if (isValid(currentCharMap, charMap)) {
                    continue;
                } else {
                    if (result.length() == 0 || result.length() > (j - i)) {
                        result = S.substring(i, j);
                    }
                    i += 1;
                    break;
                }
            }
        }

        return result;
    }

    private boolean isValid(Map<Character, Integer> currentCharMap, Map<Character, Integer> charMap) {
        if (currentCharMap.keySet().size() != charMap.keySet().size()) {
            return false;
        }
        if (lastInvalid != null) {
            if (!currentCharMap.containsKey(lastInvalid) || currentCharMap.get(lastInvalid) < charMap.get(lastInvalid)) {
                return false;
            }
        }
        for (Map.Entry<Character, Integer> entry : charMap.entrySet()) {
            if (!currentCharMap.containsKey(entry.getKey())) {
                lastInvalid = entry.getKey();
                return false;
            }
            if (currentCharMap.get(entry.getKey()) < entry.getValue()) {
                lastInvalid = entry.getKey();
                return false;
            }
        }
        lastInvalid = null;
        return true;
    }

    private Map<Character, Integer> countChar(String T) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = 0; i < T.length(); i++) {
            char c = T.charAt(i);
            if (!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }
        return map;
    }

    public static void main(String[] args) {
        MinimumWindowSubstring s = new MinimumWindowSubstring();
        System.out.println(s.minWindow("aabaabaaab", "bb"));
    }
}
