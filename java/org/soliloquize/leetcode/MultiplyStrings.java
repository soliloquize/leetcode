package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiplyStrings {
    /**
     http://oj.leetcode.com/problems/multiply-strings/
     Given two numbers represented as strings, return multiplication of the numbers as a string.

     Note: The numbers can be arbitrarily large and are non-negative.
     */
    public String multiply(String num1, String num2) {
        if (num1.length() < num2.length()) {
            String tmp = num1;
            num1 = num2;
            num2 = tmp;
        }
        String tmp = null;
        String[] cache = new String[10];
        Arrays.fill(cache, null);
        for (int i = 0; i < num2.length(); i++) {
            List<Integer> digits = convert(num1);
            int index = Integer.valueOf(num2.charAt(i) - '0');
            if (cache[index] == null) {
                cache[index] = singleMultiply(digits, index);
            }
            if (tmp == null) {
                tmp = cache[index] + zeros(num2.length() - i - 1);
            } else {
                tmp = add(tmp, cache[index] + zeros(num2.length() - i - 1));
            }
        }
        return tmp;
    }

    private String add(String num1, String num2) {
        int i = num1.length()-1;
        int j = num2.length()-1;
        int carrier = 0;
        StringBuilder builder = new StringBuilder();
        while (i >= 0 && j >= 0) {
            int val = num1.charAt(i) - '0' + num2.charAt(j) - '0' + carrier;
            carrier = val / 10;
            builder.append((char) (val % 10 + '0'));
            i--;
            j--;
        }
        while (i >= 0) {
            int val = num1.charAt(i) - '0' + carrier;
            carrier = val / 10;
            builder.append((char) (val % 10 + '0'));
            i--;
        }
        while (j >= 0) {
            int val = num2.charAt(j) - '0' + carrier;
            carrier = val / 10;
            builder.append((char) (val % 10 + '0'));
            j--;
        }
        if (carrier > 0) {
            builder.append((char) (carrier + '0'));
        }
        builder.reverse();
        return builder.toString();
    }

    private String zeros(int num) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < num; i++) {
            builder.append("0");
        }
        return builder.toString();
    }

    private List<Integer> convert(String num1) {
        List<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < num1.length(); i++) {
            result.add(num1.charAt(i) - '0');
        }
        return result;
    }

    private String singleMultiply(List<Integer> num1, int num2) {
        if (num2 == 0) {
            return "0";
        }
        int carrier = 0;
        for (int i = num1.size()-1; i >= 0; i--) {
            int val = num1.get(i) * num2 + carrier;
            carrier = val / 10;
            num1.set(i, val % 10);
        }
        StringBuilder builder = new StringBuilder();
        if (carrier > 0) {
            builder.append(String.valueOf(carrier));
        }
        for (int num : num1) {
            builder.append(String.valueOf(num));
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        MultiplyStrings s = new MultiplyStrings();
        List<Integer> list = s.convert("123");
        System.out.println(s.singleMultiply(list, 5));
        System.out.println(s.multiply("123", "456"));
        System.out.println(s.multiply("9133", "0"));
    }
}
