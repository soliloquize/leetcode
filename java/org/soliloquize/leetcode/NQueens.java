package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class NQueens {
    /**

     http://leetcode.com/oldoj#question_51

     The n-queens puzzle is the problem of placing n queens on an n�n chessboard such that no two queens attack each other.

     Given an integer n, return all distinct solutions to the n-queens puzzle.

     Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space respectively.

     For example,
     There exist two distinct solutions to the 4-queens puzzle:

     [
     [".Q..",  // Solution 1
     "...Q",
     "Q...",
     "..Q."],

     ["..Q.",  // Solution 2
     "Q...",
     "...Q",
     ".Q.."]
     ]
     */
    public static void main(String[] args) {
        NQueens s = new NQueens();
        ArrayList<String[]> res = s.solveNQueens(4);
        for (String[] strs : res) {
            System.out.println(Arrays.deepToString(strs));
        }
        System.out.println("-->");
        res = s.solveNQueens2(4);
        for (String[] strs : res) {
            System.out.println(Arrays.deepToString(strs));
        }
    }

    public ArrayList<String[]> solveNQueens2(int n) {
        char[][] matrix = new char[n][n];
        for (int i = 0; i < matrix.length; i++) {
            Arrays.fill(matrix[i], '.');
        }
        ArrayList<String[]> result = new ArrayList<String[]>();
        solve(matrix, 0, n, result);
        return result;
    }

    private void solve(char[][] matrix, int r, int n, ArrayList<String[]> res) {
        if (r == n) {
            String[] current = new String[matrix.length];
            for (int i = 0; i < matrix.length; i++) {
                current[i] = new String(matrix[i]);
            }
            res.add(current);
            return;
        }
        for (int i = r, j = 0; j < n; j++) {
            boolean valid = true;
            for (int x = 0, y = j; x < n; x++) {
                if (matrix[x][y] == 'Q') {
                    valid = false;
                    break;
                }
            }

            if (!valid) {
                continue;
            }

            for (int x = i, y = j; x >= 0 && y >= 0; x--, y--) {
                if (matrix[x][y] == 'Q') {
                    valid = false;
                    break;
                }
            }

            if (!valid) {
                continue;
            }

            for (int x = i, y = j; x >= 0 && y < n; x--, y++) {
                if (matrix[x][y] == 'Q') {
                    valid = false;
                    break;
                }
            }

            if (!valid) {
                continue;
            }

            matrix[i][j] = 'Q';
            solve(matrix, r + 1, n, res);
            matrix[i][j] = '.';
        }
    }

    public ArrayList<String[]> solveNQueens(int n) {
        char[][] matrix = new char[n][n];
        for (int i = 0; i < matrix.length; i++) {
            Arrays.fill(matrix[i], '.');
        }
        ArrayList<String[]> result = new ArrayList<String[]>();
        search(n, 0, matrix, result);
        return result;
    }

    private void search(int n, int r, char[][] current, ArrayList<String[]> result) {
        if (r == n) {
            String [] tmp = new String[current.length];
            for (int i = 0; i < current.length; i++) {
                StringBuilder builder = new StringBuilder();
                for (int j = 0; j < current[i].length; j++) {
                    builder.append(current[i][j]);
                }
                tmp[i] = builder.toString();
            }
            result.add(tmp);
            return;
        }
        for (int k = 0; k < n; k++) {
            if (current[r][k] == '.') {
                boolean flag = false;
                for (int i = 0; i < r; i++) {
                    if (current[i][k] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                for (int i = r - 1, j = k - 1; i >= 0 && j >= 0; i--, j--) {
                    if (current[i][j] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                for (int i = r - 1, j = k + 1; i >= 0 && j < n; i--, j++) {
                    if (current[i][j] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                current[r][k] = 'Q';
                search(n, r + 1, current, result);
                current[r][k] = '.';
            }
        }
    }
}
