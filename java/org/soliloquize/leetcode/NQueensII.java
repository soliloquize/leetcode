package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class NQueensII {
    /**
     http://leetcode.com/oldoj#question_52

     Follow up for N-Queens problem.

     Now, instead outputting board configurations, return the total number of distinct solutions.
     */
    public int totalNQueens(int n) {
        char[][] matrix = new char[n][n];
        for (int i = 0; i < matrix.length; i++) {
            Arrays.fill(matrix[i], '.');
        }
        ArrayList<String[]> result = new ArrayList<String[]>();
        search(n, 0, matrix, result);
        return result.size();
    }

    private void search(int n, int r, char[][] current, ArrayList<String[]> result) {
        if (r == n) {
            String [] tmp = new String[current.length];
            for (int i = 0; i < current.length; i++) {
                StringBuilder builder = new StringBuilder();
                for (int j = 0; j < current[i].length; j++) {
                    builder.append(current[i][j]);
                }
                tmp[i] = builder.toString();
            }
            result.add(tmp);
            return;
        }
        for (int k = 0; k < n; k++) {
            if (current[r][k] == '.') {
                boolean flag = false;
                for (int i = 0; i < r; i++) {
                    if (current[i][k] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                for (int i = r - 1, j = k - 1; i >= 0 && j >= 0; i--, j--) {
                    if (current[i][j] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                for (int i = r - 1, j = k + 1; i >= 0 && j < n; i--, j++) {
                    if (current[i][j] == 'Q') {
                        flag = true;
                        break;
                    }
                }
                if (flag) {
                    continue;
                }
                current[r][k] = 'Q';
                search(n, r + 1, current, result);
                current[r][k] = '.';
            }
        }
    }
}
