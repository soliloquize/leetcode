package org.soliloquize.leetcode;

public class NextPermutation {

    /**
     http://leetcode.com/onlinejudge#question_31

     Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.

     If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).

     The replacement must be in-place, do not allocate extra memory.

     Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.
     1,2,3 → 1,3,2
     3,2,1 → 1,2,3
     1,1,5 → 1,5,1
     */

    public void nextPermutation(int[] num) {
        for (int i = num.length - 1; i >= 1; i--) {
            if (num[i] <= num[i-1]) {
                continue;
            }
            for (int k = num.length - 1; k >= i; k--) {
                if (num[k] > num[i-1]) {
                    int tmp = num[i-1];
                    num[i-1] = num[k];
                    num[k] = tmp;

                    for (int x = i, y = num.length-1; x < y; x++, y--) {
                        tmp = num[x];
                        num[x] = num[y];
                        num[y] = tmp;
                    }

                    return;
                }
            }
        }

        for (int x = 0, y = num.length-1; x < y; x++, y--) {
            int tmp = num[x];
            num[x] = num[y];
            num[y] = tmp;
        }
    }
}
