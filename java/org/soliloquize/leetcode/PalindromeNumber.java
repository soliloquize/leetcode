package org.soliloquize.leetcode;

public class PalindromeNumber {

    /**
     http://leetcode.com/onlinejudge#question_9

     Determine whether an integer is a palindrome. Do this without extra space.
     */
    public boolean isPalindrome2(int x) {
        if (x < 0) {
            return false;
        }
        int length = getLength(x);

        int p = (int) Math.pow(10, length-1);
        while (x > 0) {
            int m = x / p % 10;
            int n = x % 10;
            if (m != n) {
                return false;
            }
            x = x % m;
            x /= 10;
            p /= 10;
        }
        return true;
    }

    public static void main(String[] args) {
        PalindromeNumber s = new PalindromeNumber();
        System.out.println(s.isPalindrome2(12345));
        System.out.println(s.isPalindrome2(12321));
    }


    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        int length = getLength(x);

        int i = 1;
        int j = length;

        while (i < j) {
            int m = x / pow(i) % 10;
            int n = x / pow(j) % 10;
            if (m != n) {
                return false;
            }
            i += 1;
            j -= 1;
        }

        return true;
    }

    private int pow(int length) {
        int result = 1;
        for (int i = 1; i < length; i++) {
            result *= 10;
        }
        return result;
    }

    private int getLength(int x) {
        int length = 0;
        while (x > 0) {
            length += 1;
            x /= 10;
        }
        return length;
    }
}
