package org.soliloquize.leetcode;

import java.util.ArrayList;

public class PalindromePartitioning {
    /**
     http://leetcode.com/oldoj#question_131

     Given a string s, partition s such that every substring of the partition is a palindrome.

     Return all possible palindrome partitioning of s.

     For example, given s = "aab",
     Return

     [
     ["aa","b"],
     ["a","a","b"]
     ]
     */
    public ArrayList<ArrayList<String>> partition2(String s) {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        partition2(s.toCharArray(), 0, new ArrayList<String>(), result);
        return result;
    }

    private void partition2(char[] input, int start, ArrayList<String> current, ArrayList<ArrayList<String>> result) {
        if (start == input.length) {
            result.add(new ArrayList<String>(current));
            return;
        }
        for (int i = start; i < input.length; i++) {
            if (isPalindrome(input, start, i)) {
                StringBuilder builder = new StringBuilder();
                builder.append(input, start, i - start + 1);
                current.add(builder.toString());
                partition2(input, i + 1, current, result);
                current.remove(current.size()-1);
            }
        }
    }

    public static void main(String [] args) {
        PalindromePartitioning s = new PalindromePartitioning();
        ArrayList<ArrayList<String>> res = s.partition2("aab");
        System.out.println(res);
    }

    public ArrayList<ArrayList<String>> partition(String s) {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        calculate(s, 0, new ArrayList<String>(), result);
        return result;
    }

    private void calculate(String s, int p, ArrayList<String> current, ArrayList<ArrayList<String>> result) {
        if (p == s.length()) {
            result.add(new ArrayList<String>(current));
            return;
        }
        char[] input = s.toCharArray();
        int start = p;
        for (int i = 0; p + i < s.length(); i++) {
            int end = p + i;
            if (isPalindrome(input, start, end)) {
                current.add(s.substring(start, end+1));
                calculate(s, end + 1, current, result);
                current.remove(current.size()-1);
            }
        }
    }

    private boolean isPalindrome(char[] input, int start, int end) {
        if (start == end) {
            return true;
        }
        for (int i = start, j = end; i < j; i++, j--) {
            if (input[i]!= input[j]) {
                return false;
            }
        }
        return true;
    }
}