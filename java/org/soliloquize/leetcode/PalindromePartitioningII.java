package org.soliloquize.leetcode;

import java.util.Arrays;

public class PalindromePartitioningII {
    /**
     http://leetcode.com/oldoj#question_132

     Given a string s, partition s such that every substring of the partition is a palindrome.

     Return the minimum cuts needed for a palindrome partitioning of s.

     For example, given s = "aab",
     Return 1 since the palindrome partitioning ["aa","b"] could be produced using 1 cut.
     */

    public int minCut2(String s) {
        if (s.length() == 0) {
            return 0;
        }
        char[] chars = s.toCharArray();
        int[] p = new int[s.length()];
        for (int i = 1; i < s.length(); i++) {
            p[i] = p[i-1] + 1;
            for (int j = i - 1; j >= 0; j--) {
                if (isPalindrome(chars, j, i)) {
                    if (j > 0) {
                        p[i] = Math.min(p[i], p[j-1] + 1);
                    } else {
                        p[i] = Math.min(p[i], p[j]);
                    }
                }
            }
        }
        return p[s.length()-1];
    }

    public static void main(String[] args) {
        PalindromePartitioningII s= new PalindromePartitioningII();
        System.out.println(s.minCut2("aab"));
    }


    public int minCut(String s) {
        // p[i] = min(1 + p[j]), for j = 0...i-1 if s[i..j] is palindrome
        if (s.length() <= 1) {
            return 0;
        }
        int [] p = new int[s.length()];
        Arrays.fill(p, Integer.MAX_VALUE);
        p[0] = 0;
        char[] chars = s.toCharArray();
        for (int i = 1; i < chars.length; i++) {
            p[i] = p[i-1] + 1;
            for (int j = 0; j < i; j++) {
                if (isPalindrome(chars, j, i)) {
                    if (j < 1) {
                        p[i] = Math.min(p[j], p[i]);
                    } else {
                        p[i] = Math.min(p[j-1] + 1, p[i]);
                    }
                }
            }
        }
        return p[s.length()-1];
    }

    private boolean isPalindrome(char[] chars, int start, int end) {
        if (start == end) {
            return true;
        }
        for (int i = start, j = end; i < j; i++, j--) {
            if (chars[i] != chars[j]) {
                return false;
            }
        }
        return true;
    }
}
