package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class PartitionList {
    /**
     http://leetcode.com/oldoj#question_86

     Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

     You should preserve the original relative order of the nodes in each of the two partitions.

     For example,
     Given 1->4->3->2->5->2 and x = 3,
     return 1->2->2->4->3->5.
     */

    public static void main(String[] args) {
        PartitionList s = new PartitionList();
        int[] input = new int[] {1, 4, 3, 2, 5, 2};
        ListNode root = new ListNode(1);
        ListNode p = root;
        for (int i = 1; i < input.length; i++) {
            p.next = new ListNode(input[i]);
            p = p.next;
        }
        ListNode q = s.partition2(root, 3);
        while (q != null) {
            System.out.println(q.val);
            q = q.next;
        }
    }


    public ListNode partition2(ListNode head, int x) {
        ListNode small = new ListNode(0);
        ListNode large = new ListNode(0);

        ListNode sp = small;
        ListNode lp = large;

        ListNode p = head;
        while (p != null) {
            if (p.val < x) {
                sp.next = p;
                sp = sp.next;
            } else {
                lp.next = p;
                lp = lp.next;
            }
            p = p.next;
        }

        sp.next = large.next;
        lp.next = null;

        return small.next;
    }

    public ListNode partition(ListNode head, int x) {
        ListNode root = new ListNode(-1);
        ListNode pivot = new ListNode(-1);
        ListNode p = root;
        ListNode q = pivot;

        ListNode k = head;
        while (k != null) {
            if (k.val < x) {
                p.next = k;
                p = p.next;
            } else {
                q.next = k;
                q = q.next;
            }
            k = k.next;
        }
        p.next = pivot.next;
        q.next = null;
        return root.next;
    }
}
