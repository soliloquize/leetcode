package org.soliloquize.leetcode;

import java.util.ArrayList;

public class PascalTriangle {
    /**
     http://leetcode.com/onlinejudge#question_118

     Given numRows, generate the first numRows of Pascal's triangle.

     For example, given numRows = 5,
     Return

     [
     [1],
     [1,1],
     [1,2,1],
     [1,3,3,1],
     [1,4,6,4,1]
     ]
     */
    public ArrayList<ArrayList<Integer>> generate(int numRows) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < numRows; i++) {
            ArrayList<Integer> row = new ArrayList<Integer>();
            for (int j = 0; j <= i; j++) {
                row.add(cmn(i, j));
            }
            result.add(row);
        }

        return result;
    }

    private int cmn(int m, int n) {
        if (m == 0 || m == n) {
            return 1;
        }
        int res = 1;
        int i = n + 1;
        int j = 2;
        while (i <= m) {
            res *= i++;
            while (j <= (m - n) && res % j == 0) {
                res /= j++;
            }
        }
        return res;
    }

    public static void main(String[] args) throws Exception {
        PascalTriangle solution = new PascalTriangle();
        System.out.println(solution.cmn(5, 1));
        System.out.println(solution.generate(5));
    }
}
