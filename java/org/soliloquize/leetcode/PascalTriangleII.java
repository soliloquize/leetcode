package org.soliloquize.leetcode;

import java.util.ArrayList;

public class PascalTriangleII {

    /**
     http://leetcode.com/onlinejudge#question_119

     Given an index k, return the kth row of the Pascal's triangle.

     For example, given k = 3,
     Return [1,3,3,1].

     Note:
     Could you optimize your algorithm to use only O(k) extra space?
     */

    public ArrayList<Integer> getRow2(int rowIndex) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i <= rowIndex; i++) {
            result.add(cmn2(rowIndex, i));
        }
        return result;
    }

    public int cmn2(int m, int n) {
        long res = 1;

        int i = m;
        int j = 1;

        while (i >= n + 1) {
            res *= i;
            i -= 1;
            while ((j <= m - n) && (res % j == 0)) {
                res /= j;
                j += 1;
            }
        }
        return (int)(res);
    }

    public static void main(String[] args) {
        PascalTriangleII s = new PascalTriangleII();
        System.out.println(s.getRow2(2));
        System.out.println(s.getRow2(3));
        System.out.println(s.getRow2(4));
    }



    public ArrayList<Integer> getRow(int rowIndex) {
        ArrayList<Integer> row = new ArrayList<Integer>();
        for (int j = 0; j <= rowIndex; j++) {
            row.add(cmn(rowIndex, j));
        }
        return row;
    }

    private int cmn(int m, int n) {
        if (m == 0 || m == n) {
            return 1;
        }
        long res = 1;
        long i = m;
        long j = 2;
        while (i >= n + 1) {
            res *= i--;
            while (j <= (m - n) && res % j == 0) {
                res /= j++;
            }
        }
        return (int) res;
    }
}
