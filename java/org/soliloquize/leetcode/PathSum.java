package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class PathSum {

    /**
     http://leetcode.com/onlinejudge#question_112

     Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up all the values along the path equals the given sum.

     For example:
     Given the below binary tree and sum = 22,
     5
     / \
     4   8
     /   / \
     11  13  4
     /  \      \
     7    2      1
     return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
     */

    public static void main(String[] args) {
        PathSum s = new PathSum();
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(4);
        root.left.left = new TreeNode(11);
        root.left.left.left = new TreeNode(7);
        root.left.left.right = new TreeNode(2);
        root.right = new TreeNode(8);
        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(4);
        root.right.right.right = new TreeNode(1);
        System.out.println(s.hasPathSum(root, 22));
        System.out.println(s.hasPathSum2(root, 22));
    }

    public boolean hasPathSum2(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        hasSum = false;
        search(root, 0, sum);
        return hasSum;
    }

    private void search(TreeNode root, int current, int target) {
        current += root.val;
        if (root.left == null && root.right == null) {
            if (current == target) {
                hasSum = true;
            }
            return;
        }
        if (root.left != null) {
            search(root.left, current, target);
        }
        if (root.right != null) {
            search(root.right, current, target);
        }
    }

    private boolean hasSum = false;

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        hasSum = false;
        travel(root, 0, sum);
        return hasSum;
    }

    private void travel(TreeNode root, int current, int sum) {
        current = current + root.val;
        if (root.left == null && root.right == null) {
            hasSum |= current == sum;
        } else {
            if (root.left != null) {
                travel(root.left, current, sum);
                if (hasSum) {
                    return;
                }
            }
            if (root.right != null) {
                travel(root.right, current, sum);
                if (hasSum) {
                    return;
                }
            }
        }
    }
}
