package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayList;

public class PathSumII {
    /**
     http://leetcode.com/onlinejudge#question_113

     Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.

     For example:
     Given the below binary tree and sum = 22,
     5
     / \
     4   8
     /   / \
     11  13  4
     /  \    / \
     7    2  5   1
     return

     [
     [5,4,11,2],
     [5,8,4,5]
     ]
     */

    public static void main(String[] args) {
        PathSumII s = new PathSumII();
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(4);
        root.left.left = new TreeNode(11);
        root.left.left.left = new TreeNode(7);
        root.left.left.right = new TreeNode(2);
        root.right = new TreeNode(8);
        root.right.left = new TreeNode(13);
        root.right.right = new TreeNode(4);
        root.right.right.right = new TreeNode(1);
        System.out.println(s.pathSum(root, 22));
        System.out.println(s.pathSum2(root, 22));
    }

    public ArrayList<ArrayList<Integer>> pathSum2(TreeNode root, int sum) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        if (root == null) {
            return result;
        }
        search(root, 0, sum, new ArrayList<Integer>(), result);
        return result;
    }

    private void search(TreeNode root, int current, int target, ArrayList<Integer> steps, ArrayList<ArrayList<Integer>> result) {
        current += root.val;
        steps.add(root.val);
        if (root.left == null && root.right == null) {
            if (current == target) {
                result.add(new ArrayList<Integer>(steps));
            }
            steps.remove(steps.size()-1);
            return;
        }
        if (root.left != null) {
            search(root.left, current, target, steps, result);
        }
        if (root.right != null) {
            search(root.right, current, target, steps, result);
        }
        steps.remove(steps.size()-1);
    }

    private ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

    public ArrayList<ArrayList<Integer>> pathSum(TreeNode root, int sum) {
        if (root == null) {
            return new ArrayList<ArrayList<Integer>>();
        }
        result.clear();
        travel(root, 0, sum, new ArrayList<Integer>());
        return result;
    }

    private void travel(TreeNode root, int current, int sum, ArrayList<Integer> steps) {
        current = current + root.val;
        steps.add(root.val);
        if (root.left == null && root.right == null) {
            if (current == sum) {
                result.add(new ArrayList<Integer>(steps));
            }
        } else {
            if (root.left != null) {
                travel(root.left, current, sum, steps);
            }
            if (root.right != null) {
                travel(root.right, current, sum, steps);
            }
        }
        steps.remove(steps.size()-1);
    }
}
