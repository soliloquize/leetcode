package org.soliloquize.leetcode;

public class PermutationSequence {
    /**
     http://leetcode.com/oldoj#question_60

     The set [1,2,3,…,n] contains a total of n! unique permutations.

     By listing and labeling all of the permutations in order,
     We get the following sequence (ie, for n = 3):

     •"123"
     •"132"
     •"213"
     •"231"
     •"312"
     •"321"

     Given n and k, return the kth permutation sequence.

     Note: Given n will be between 1 and 9 inclusive.
     */
    public String getPermutation(int n, int k) {
        int [] num = new int[n];
        for (int i = 0; i < num.length; i++) {
            num[i] = i + 1;
        }
        if (k == 1) {
            return buildResult(num);
        }

        int count = pow(n);
        if (k == count) {
            for (int i = 0, j = num.length - 1; i < j; i++, j--) {
                int tmp = num[i];
                num[i] = num[j];
                num[j] = num[i];
            }
            return buildResult(num);
        }

        for (int i = 2; i <= k; i++) {
            nextPermutation(num);
        }
        return buildResult(num);
    }

    private int pow(int k) {
        int res = 1;
        for (int i = 1; i <=k; i++) {
            res *= k;
        }
        return res;
    }

    private String buildResult(int[] num) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < num.length; i++) {
            builder.append((char) (((int)'0') + num[i]));
        }
        return builder.toString();
    }

    private void nextPermutation(int[] num) {
        for (int i = num.length - 1; i >= 1; i--) {
            if (num[i] <= num[i-1]) {
                continue;
            }
            for (int k = num.length - 1; k >= i; k--) {
                if (num[k] > num[i-1]) {
                    int tmp = num[i-1];
                    num[i-1] = num[k];
                    num[k] = tmp;

                    for (int x = i, y = num.length-1; x < y; x++, y--) {
                        tmp = num[x];
                        num[x] = num[y];
                        num[y] = tmp;
                    }

                    return;
                }
            }
        }

        for (int x = 0, y = num.length-1; x < y; x++, y--) {
            int tmp = num[x];
            num[x] = num[y];
            num[y] = tmp;
        }
    }
}
