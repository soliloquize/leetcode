package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class Permutations {
    /**
     http://leetcode.com/onlinejudge#question_46

     Given a collection of numbers, return all possible permutations.

     For example,
     [1,2,3] have the following permutations:
     [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].
     */
    public ArrayList<ArrayList<Integer>> permute(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(num);
        result.add(copy(num));
        while (true) {
            nextPermutation(num);
            ArrayList<Integer> current = copy(num);
            if (current.equals(result.get(0))) {
                break;
            }
            result.add(current);
        }
        return result;
    }

    private ArrayList<Integer> copy(int [] num) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for (int i = 0; i < num.length; i++) {
            result.add(num[i]);
        }
        return result;
    }

    private void nextPermutation(int[] num) {
        for (int i = num.length - 1; i >= 1; i--) {
            if (num[i] <= num[i-1]) {
                continue;
            }
            for (int k = num.length - 1; k >= i; k--) {
                if (num[k] > num[i-1]) {
                    int tmp = num[i-1];
                    num[i-1] = num[k];
                    num[k] = tmp;

                    for (int x = i, y = num.length-1; x < y; x++, y--) {
                        tmp = num[x];
                        num[x] = num[y];
                        num[y] = tmp;
                    }

                    return;
                }
            }
        }

        for (int x = 0, y = num.length-1; x < y; x++, y--) {
            int tmp = num[x];
            num[x] = num[y];
            num[y] = tmp;
        }
    }
}
