package org.soliloquize.leetcode;

public class PlusOne {
    /**
     http://leetcode.com/oldoj#question_66

     Given a number represented as an array of digits, plus one to the number.
     */

    public static void main(String[] args) {
        PlusOne s = new PlusOne();
        int[] res = s.plusOne2(new int [] {9});
        for (int i : res) {
            System.out.println(i);
        }
    }

    public int[] plusOne2(int[] digits) {
        int carry = 1;

        for (int i = digits.length-1; i >=0; i--) {
            int tmp = digits[i] + carry;
            digits[i] = tmp % 10;
            carry = tmp / 10;
        }
        if (carry > 0) {
            int[] result = new int[digits.length+1];
            result[0] = carry;
            System.arraycopy(digits, 0, result, 1, digits.length);
            return result;
        }
        return digits;
    }


    public int[] plusOne(int[] digits) {
        int carry = 1;

        for (int i = digits.length-1; i >= 0; i--) {
            int tmp = digits[i] + carry;
            digits[i] = tmp % 10;
            carry = tmp / 10;
        }
        if (carry > 0) {
            int [] result = new int[digits.length + 1];
            result[0] = carry;
            for (int i = 0; i < digits.length; i++) {
                result[i+1] = digits[i];
            }
            return result;
        }
        return digits;
    }
}
