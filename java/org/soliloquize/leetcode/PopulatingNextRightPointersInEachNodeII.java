package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeLinkNode;

public class PopulatingNextRightPointersInEachNodeII {
    /**
     http://oj.leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/

     Follow up for problem "Populating Next Right Pointers in Each Node".

     What if the given tree could be any binary tree? Would your previous solution still work?

     Note:

     You may only use constant extra space.
     For example,
     Given the following binary tree,
     1
     /  \
     2    3
     / \    \
     4   5    7
     After calling your function, the tree should look like:
     1 -> NULL
     /  \
     2 -> 3 -> NULL
     / \    \
     4-> 5 -> 7 -> NULL
     */
    public void connect(TreeLinkNode root) {
        if (root == null) {
            return;
        }
        // 剪枝，避免重复计算
        if (root.left != null && root.left.next != null) {
            return;
        }
        if (root.right != null && root.right.next != null) {
            return;
        }
        // 同时存在左右节点
        if (root.left != null && root.right != null) {
            root.left.next = root.right;
        }
        // 找到同一层次节点第一个包含子节点的节点
        if (root.next != null) {
            TreeLinkNode p = root;
            while (p.next != null) {
                p = p.next;
                if (p.left != null || p.right != null) {
                    break;
                }
            }

            // 递归计算p节点
            if (p.left != null && p.left.next == null) {
                connect(p);
            } else if (p.right != null && p.right.next == null){
                connect(p);
            }

            // 建立同一层次间的联系
            if (root.right != null) {
                if (p.left != null) {
                    root.right.next = p.left;
                } else {
                    root.right.next = p.right;
                }
            } else if (root.left != null) {
                if (p.left != null) {
                    root.left.next = p.left;
                } else {
                    root.left.next = p.right;
                }
            }
        }
        connect(root.left);
        connect(root.right);
    }
}
