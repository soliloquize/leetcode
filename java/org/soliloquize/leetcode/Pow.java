package org.soliloquize.leetcode;

import java.util.Arrays;

public class Pow {
    /**
     http://leetcode.com/oldoj#question_50

     Implement pow(x, n).
     */
    public double pow2(double x, int n) {
        if (n >= 0) {
            return powPositive(x, n);
        }
        return powNegative(x, n);
    }

    private double powNegative(double x, int n) {
        return 1 / powPositive(x, -n);
    }

    private double powPositive(double x, int n) {
        double result = 1;

        double[] commons = new double[32];
        commons[0] = x;
        for (int i = 1; i < commons.length; i++) {
            commons[i] = commons[i-1] * commons[i-1];
        }

        for (int i = 0; i < commons.length; i++) {
            boolean flag = ((n & 1) == 1);
            if (flag) {
                result *= commons[i];
            }
            n >>= 1;
        }
        return result;
    }


    public double pow(double x, int n) {
        if (n >= 0) {
            return pow_positive(x, n);
        }
        return pow_negtive(x, n);
    }

    private double pow_negtive(double x, int n) {
        return 1 / pow_positive(x, -n);
    }

    private double pow_positive(double x, int n) {
        if (n == 0) {
            return 1;
        }
        int size = 32;
        boolean[] flags = new boolean[size];
        Arrays.fill(flags, false);

        double[] commons = new double[size];
        Arrays.fill(commons, 0);
        commons[0] = x;
        for (int i = 0; i < size; i++) {
            int tmp = n >> i;
            if ((tmp & 1) == 1) {
                flags[i] = true;
            }
            if (tmp == 0) {
                break;
            }
            if (i > 0) {
                commons[i] = commons[i-1] * commons[i-1];
            }
        }
        double result = 1;
        for (int i = 0; i < size; i++) {
            if (flags[i]) {
                result *= commons[i];
            }
        }
        return result;
    }

    public static void main(String[] args) throws Exception {
        Pow s = new Pow();
        System.out.println(s.pow(34.00515, 3));
        System.out.println(s.pow(5, 10));
        System.out.println(s.pow2(34.00515, 3));
        System.out.println(s.pow2(5, 10));
    }
}
