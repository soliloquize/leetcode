package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class RecoverBinarySearchTree {
    /**
     http://oj.leetcode.com/problems/recover-binary-search-tree/

     Two elements of a binary search tree (BST) are swapped by mistake.

     Recover the tree without changing its structure.

     Note:
     A solution using O(n) space is pretty straight forward. Could you devise a constant space solution?
     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.


     OJ's Binary Tree Serialization:
     The serialization of a binary tree follows a level order traversal, where '#' signifies a path terminator where no node exists below.

     Here's an example:
     1
     / \
     2   3
     /
     4
     \
     5
     The above binary tree is serialized as "{1,2,3,#,#,4,#,#,5}".
     */
    public void recoverTree(TreeNode root) {
        if (root == null) {
            return;
        }
        List<TreeNode> nodes = new ArrayList<TreeNode>();
        travel(root, nodes);

        TreeNode wrong = null;
        int i = 0;
        for (i = 0; i < nodes.size() - 1; i++) {
            TreeNode first = nodes.get(i);
            TreeNode second = nodes.get(i+1);
            // 出现逆序则节点是错的
            if (first.val > second.val) {
                wrong = first;
                break;
            }
        }

        // 找到比当前元素小的最小元素
        TreeNode wrong2 = null;
        for (int j = i + 1; j < nodes.size(); j++) {
            TreeNode tmp = nodes.get(j);
            if (tmp.val > wrong.val) {
                break;
            } else {
                if (wrong2 == null || (wrong2.val > tmp.val)) {
                    wrong2 = tmp;
                }
            }
        }

        // 交换两个节点的值
        int tmp = wrong.val;
        wrong.val = wrong2.val;
        wrong2.val = tmp;
    }

    private void travel(TreeNode root, List<TreeNode> nodes) {
        if (root.left != null) {
            travel(root.left, nodes);
        }
        nodes.add(root);
        if (root.right != null) {
            travel(root.right, nodes);
        }
    }

    public void recoverTree2(TreeNode root) {
        List<TreeNode> nodes = new ArrayList<TreeNode>();
        travel(root, nodes);

        for (int i = 0; i < nodes.size()-1; i++) {
            int a = nodes.get(i).val;
            int b = nodes.get(i+1).val;
            if (a < b) {
                continue;
            }
            int index = i + 1;
            int min = b;
            for (int j = i + 2; j < nodes.size(); j++) {
                int t = nodes.get(j).val;
                if (t > a) {
                    return;
                }
                if (min > t) {
                    min = t;
                    index = j;
                }
            }
            nodes.get(i).val = min;
            nodes.get(index).val = a;
            return;
        }
    }

    public static void main(String[] args) {
        RecoverBinarySearchTree s = new RecoverBinarySearchTree();
        TreeNode root = new TreeNode(0);
        root.left = new TreeNode(1);
        s.recoverTree(root);
        System.out.println(root.val);

        TreeNode root1 = new TreeNode(0);
        root1.left = new TreeNode(1);
        s.recoverTree2(root1);
        System.out.println(root1.val);
    }
}
