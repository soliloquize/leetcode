package org.soliloquize.leetcode;

public class RegularExpressionMatching {
    /**
     http://oj.leetcode.com/problems/regular-expression-matching/

     Implement regular expression matching with support for '.' and '*'.

     '.' Matches any single character.
     '*' Matches zero or more of the preceding element.

     The matching should cover the entire input string (not partial).

     The function prototype should be:
     bool isMatch(const char *s, const char *p)

     Some examples:
     isMatch("aa","a") → false
     isMatch("aa","aa") → true
     isMatch("aaa","aa") → false
     isMatch("aa", "a*") → true
     isMatch("aa", ".*") → true
     isMatch("ab", ".*") → true
     isMatch("aab", "c*a*b") → true
     */

    public boolean isMatch(String s, String p) {
        return isMatch(s, 0, p, 0, new int[s.length()+1][p.length()+1]);
    }

    public boolean isMatch(String s, int sindex, String p, int pindex, int[][] matches) {
        if (sindex == s.length() && pindex == p.length()) {
            return true;
        }
        if (sindex != s.length() && pindex == p.length()) {
            return false;
        }

        if (matches[sindex][pindex] == 1) {
            return true;
        }
        if (matches[sindex][pindex] == 2) {
            return false;
        }

        boolean result = false;
        if (pindex + 1 < p.length() && p.charAt(pindex + 1) == '*') {
            if (sindex < s.length() && (s.charAt(sindex) == p.charAt(pindex) || p.charAt(pindex) == '.')) {
                result = isMatch(s, sindex + 1, p, pindex, matches) || isMatch(s, sindex + 1, p, pindex + 2, matches) || isMatch(s, sindex, p, pindex + 2, matches);
            } else {
                result = isMatch(s, sindex, p, pindex + 2, matches);
            }
        } else {
            if (sindex < s.length() && (s.charAt(sindex) == p.charAt(pindex) || p.charAt(pindex) == '.')) {
                result = isMatch(s, sindex + 1, p, pindex + 1, matches);
            } else {
                result = false;
            }
        }
        matches[sindex][pindex] = result ? 1 : 2;
        return result;
    }

    public static void main(String[] args) throws Exception {
        RegularExpressionMatching s = new RegularExpressionMatching();
        System.out.println(s.isMatch("aa", ".*"));
    }
}
