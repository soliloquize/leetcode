package org.soliloquize.leetcode;

public class RemoveDuplicatesFromSortedArray {

    /**
     http://leetcode.com/onlinejudge#question_26

     Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.

     Do not allocate extra space for another array, you must do this in place with constant memory.

     For example,
     Given input array A = [1,1,2],

     Your function should return length = 2, and A is now [1,2].
     */

    public static void main(String[] args) {
        RemoveDuplicatesFromSortedArray s = new RemoveDuplicatesFromSortedArray();
        System.out.println(s.removeDuplicates(new int[] {1, 1, 2}));
    }

    public int removeDuplicates(int[] A) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int p = 0;
        int q = 1;
        while (q < A.length) {
            if (A[q] != A[p]) {
                A[++p] = A[q];
            }
            q += 1;
        }
        return p + 1;
    }
}
