package org.soliloquize.leetcode;

public class RemoveDuplicatesFromSortedArrayII {
    /**
     http://leetcode.com/oldoj#question_80

     Follow up for "Remove Duplicates":
     What if duplicates are allowed at most twice?

     For example,
     Given sorted array A = [1,1,1,2,2,3],

     Your function should return length = 5, and A is now [1,1,2,2,3].
     */

    public static void main(String[] args) {
        RemoveDuplicatesFromSortedArrayII s = new RemoveDuplicatesFromSortedArrayII();
        System.out.println(s.removeDuplicates2(new int[] {1, 1, 1, 2, 2, 3}));
        System.out.println(s.removeDuplicates3(new int[] {1, 1, 1, 2, 2, 3}));
    }

    public int removeDuplicates(int[] A) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int p = 0;
        int q = 1;
        while (q < A.length) {
            if (A[q] != A[p]) {
                A[++p] = A[q];
            } else {
                if (p == 0 || (p - 1 >= 0 && A[p - 1] != A[q])) {
                    A[++p] = A[q];
                }
            }
            q += 1;
        }
        return p + 1;
    }










    public int removeDuplicates3(int[] A) {
        if (A.length <= 1) {
            return A.length;
        }
        int p = 0;
        int q = 1;
        int last = A[p];
        int count = 0;
        while (q < A.length) {
            if (A[q] != last) {
                A[++p] = A[q];
                count = 0;
            } else {
                if (count == 0) {
                    A[++p] = A[q];
                    count += 1;
                }
            }
            q += 1;
        }
        return p + 1;
    }

    public int removeDuplicates2(int[] A) {
        if (A.length == 0 || A.length == 1) {
            return A.length;
        }

        int p = 0;
        int q = 1;
        while (q < A.length) {
            if (A[p] != A[q]) {
                p += 1;
                A[p] = A[q];
            } else {
                if (p == 0 || A[p-1] != A[q]) {
                    p += 1;
                    A[p] = A[q];
                }
            }
            q += 1;
        }
        return p + 1;
    }

    public int removeDuplicates_(int[] A) {
        if (A.length <= 1) {
            return A.length;
        }
        int i = 0;
        int j = i + 1;
        int status = 0;
        for (; i < A.length; ) {
            status = 0;
            for (; j < A.length; j++) {
                if (status == 0) {
                    if (A[i] == A[j]) {
                        status = 1;
                        if (i + 1 < A.length && A[i + 1] == Integer.MAX_VALUE) {
                            A[i + 1] = A[j];
                            i += 1;
                            A[j] = Integer.MAX_VALUE;
                        } else {
                            i = j;
                        }
                    } else {
                        break;
                    }
                } else {
                    if (A[i] == A[j]) {
                        A[j] = Integer.MAX_VALUE;
                    } else {
                        break;
                    }
                }
            }
            if (i + 1 < A.length && j < A.length) {
                int tmp = A[j];
                A[j] = Integer.MAX_VALUE;
                A[i+1] = tmp;
                j += 1;
            }
            i += 1;
        }
        int res = 0;
        for (; res < A.length; res++) {
            if (A[res] == Integer.MAX_VALUE) {
                break;
            }
        }
        return res;
    }
}
