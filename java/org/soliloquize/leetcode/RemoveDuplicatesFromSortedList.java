package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class RemoveDuplicatesFromSortedList {
    /**
     http://leetcode.com/oldoj#question_83

     Given a sorted linked list, delete all duplicates such that each element appear only once.

     For example,
     Given 1->1->2, return 1->2.
     Given 1->1->2->3->3, return 1->2->3.
     */

    public static void main(String[] args) {
        RemoveDuplicatesFromSortedList s = new RemoveDuplicatesFromSortedList();
        int[] inputs = new int[] {1, 1, 2, 3, 3, 4};
        ListNode node = new ListNode(1);
        ListNode p = node;
        for (int i = 1; i < inputs.length; i++) {
            p.next = new ListNode(inputs[i]);
            p = p.next;
        }
        p = s.deleteDuplicates(node);
        while (p != null) {
            System.out.println(p.val);
            p = p.next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode p = head;
        ListNode q = head.next;
        while (q != null) {
            if (q.val != p.val) {
                p.next = q;
                p = p.next;
            }
            q = q.next;
        }
        p.next = null;
        return head;
    }


    public ListNode deleteDuplicates2(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode p = head;
        ListNode q = head.next;
        p.next = null;
        while (q != null) {
            if (q.val != p.val) {
                p.next = q;
                p = p.next;
            }
            q = q.next;
            p.next = null;
        }
        return head;
    }

}
