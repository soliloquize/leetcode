package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class RemoveDuplicatesFromSortedListII {
    /**
     http://leetcode.com/oldoj#question_82

     Given a sorted linked list, delete all nodes that have duplicate numbers, leaving only distinct numbers from the original list.

     For example,
     Given 1->2->3->3->4->4->5, return 1->2->5.
     Given 1->1->1->2->3, return 2->3.
     */
    public static void main(String[] args) {
        RemoveDuplicatesFromSortedListII s = new RemoveDuplicatesFromSortedListII();
        int[] inputs = new int[] {1, 2, 3, 3, 4, 4, 5};
        ListNode node = new ListNode(1);
        ListNode p = node;
        for (int i = 1; i < inputs.length; i++) {
            p.next = new ListNode(inputs[i]);
            p = p.next;
        }
        p = s.deleteDuplicates(node);
        while (p != null) {
            System.out.println(p.val);
            p = p.next;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode root = new ListNode(-1);
        ListNode previous = root;

        ListNode p = head;
        while (p != null) {
            ListNode q = p.next;
            boolean duplicated = false;
            while (q != null) {
                if (q.val == p.val) {
                    duplicated = true;
                }
                if (q.val != p.val) {
                    break;
                }
                q = q.next;
            }
            if (!duplicated) {
                previous.next = p;
                previous = previous.next;
            }
            p = q;
        }
        previous.next = null;
        return root.next;
    }


    public ListNode deleteDuplicates2(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode r = new ListNode(-1);
        ListNode k = r;
        ListNode p = head;
        ListNode q = head.next;

        boolean flag = false;
        while (q != null) {
            if (q.val == p.val) {
                flag = true;
            } else {
                if (!flag) {
                    k.next = p;
                    k = k.next;
                }
                p = q;
                flag = false;
            }
            q = q.next;
            k.next = null;
        }
        if (!flag) {
            k.next = p;
            k = k.next;
            k.next = null;
        }
        return r.next;
    }

}
