package org.soliloquize.leetcode;

public class RemoveElement {

    /**
     http://leetcode.com/onlinejudge#question_27

     Given an array and a value, remove all instances of that value in place and return the new length.

     The order of elements can be changed. It doesn't matter what you leave beyond the new length.
     */

    public static void main(String[] args) {
        RemoveElement s = new RemoveElement();
        System.out.println(s.removeElement(new int[] {1, 3, 4, 5, 7, 4, 3, 2, 4}, 4));
        System.out.println(s.removeElement_(new int[] {2}, 3));
    }

    public int removeElement(int[] A, int elem) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int p = 0;
        int q = 0;
        while (q < A.length) {
            if (A[q] != elem) {
                A[p++] = A[q];
            }
            q += 1;
        }
        return p;
    }

    public int removeElement_(int[] A, int elem) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int p = 0;
        int q = A.length - 1;
        while (p < q) {
            while (p < q && A[p] != elem) {
                p += 1;
            }
            while (q > p && A[q] == elem) {
                q -= 1;
            }
            if (q <= p) {
                break;
            }
            A[p] = A[q];
            q -= 1;
        }
        if (p == q && p < A.length && A[p] == elem) {
            return p;
        }
        return p + 1;
    }
}
