package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class RemoveNthNodeFromEndOfList {

    /**
     http://leetcode.com/onlinejudge#question_19

     Given a linked list, remove the nth node from the end of list and return its head.

     For example,

     Given linked list: 1->2->3->4->5, and n = 2.

     After removing the second node from the end, the linked list becomes 1->2->3->5.
     Note:
     Given n will always be valid.
     Try to do this in one pass.
     */

    public ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null) {
            return null;
        }

        int length = 0;
        ListNode node = head;
        while (node != null) {
            length += 1;
            node = node.next;
        }

        if (length == n) {
            return head.next;
        }

        int step = length - n;
        node = head;
        for (int i = 0; i < step - 1; i++) {
            node = node.next;
        }

        if (node.next == null) {
            return head;
        } else {
            node.next = node.next.next;
        }
        return head;
    }
}
