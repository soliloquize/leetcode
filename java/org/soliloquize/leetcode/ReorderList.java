package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class ReorderList {
    /**
     http://oj.leetcode.com/problems/reorder-list/

     Given a singly linked list L: L0→L1→…→Ln-1→Ln,
     reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…

     You must do this in-place without altering the nodes' values.

     For example,
     Given {1,2,3,4}, reorder it to {1,4,2,3}.
     */

    public void reorderList(ListNode head) {
        // 先拆分成两个链表，然后将其中一个链表反转，最后合并两个链表
        if (head == null || head.next == null) {
            return;
        }

        int length = 0;
        ListNode p = head;
        while (p != null) {
            p = p.next;
            length += 1;
        }

        length /= 2;
        p = head;
        while (length > 0) {
            length -= 1;
            p = p.next;
        }

        ListNode q = p.next;
        p.next = null;
        ListNode t = q;

        q = reverse(q);
        p = head;
        if (t != null) {
            t.next = null;
        }

        ListNode x = null;
        ListNode y = null;
        while (p != null && q != null) {
            x = p.next;
            y = q.next;

            p.next = q;
            q.next = x;

            p = x;
            q = y;
        }
    }

    private ListNode reverse(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode p = head;
        ListNode z = null;
        ListNode q = null;
        ListNode t = null;
        ListNode h = null;
        while (p != null) {
            q = p.next;
            if (q == null) {
                p.next = z;
                z = p;
                h = p;
                break;
            }
            t = q.next;
            q.next = p;
            p.next = z;
            // 记录上一次的尾节点
            z = q;
            // 记录最后的头指针
            h = q;
            p = t;
        }
        return h;
    }

    public static void main(String[] args) {
        ReorderList s = new ReorderList();
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);

        s.reorderList(head);

        System.out.println(head.val);
        System.out.println(head.next.val);
        System.out.println(head.next.next.val);
        System.out.println(head.next.next.next.val);
    }
}
