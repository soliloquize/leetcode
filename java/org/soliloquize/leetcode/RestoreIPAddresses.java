package org.soliloquize.leetcode;

import java.util.ArrayList;

public class RestoreIPAddresses {
    /**
     http://oj.leetcode.com/problems/restore-ip-addresses/

     Given a string containing only digits, restore it by returning all possible valid IP address combinations.

     For example:
     Given "25525511135",

     return ["255.255.11.135", "255.255.111.35"]. (Order does not matter)
     */
    public ArrayList<String> restoreIpAddresses(String s) {
        ArrayList<String> result = new ArrayList<String>();
        func(s, 0, 3, new ArrayList<String>(), result);
        return result;
    }

    private void func(String s, int index, int dot, ArrayList<String> current, ArrayList<String> ips) {
        if ((s.length() - index > (dot + 1) * 3) || (s.length() - index < dot + 1)) {
            return;
        }
        if (dot <= 0) {
            if (s.length() - index <= 3) {
                String tmp = s.substring(index);
                if (!isValid(tmp)) {
                    return;
                }
                current.add(tmp);
                StringBuilder builder = new StringBuilder();
                builder.append(current.get(0));
                for (int i = 1; i < current.size(); i++) {
                    builder.append(".").append(current.get(i));
                }

                ips.add(builder.toString());
                current.remove(current.size()-1);
            }
            return;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = index; i < s.length(); i++) {
            builder.append(s.charAt(i));
            if (builder.length() > 3) {
                break;
            }
            String tmp = builder.toString();
            if (!isValid(tmp)) {
                continue;
            }
            current.add(builder.toString());
            func(s, i + 1, dot - 1, current, ips);
            current.remove(current.size()-1);
        }
    }

    private boolean isValid(String tmp) {
        int val = Integer.valueOf(tmp);
        if (val > 255) {
            return false;
        }
        if (val == 0 && tmp.length() != 1) {
            return false;
        }
        if (val != 0 && tmp.charAt(0) == '0') {
            return false;
        }
        return true;
    }

    public ArrayList<String> restoreIpAddresses2(String s) {
        ArrayList<String> result = new ArrayList<String>();
        search(s.toCharArray(), 0, 3, new StringBuilder(), result);
        return result;
    }

    private void search(char[] input, int index, int n, StringBuilder builder, ArrayList<String> result) {
        int length = builder.length();
        if (n == 0) {
            if (input.length - index > 3) {
                return;
            }
            for (int i = index; i < input.length; i++) {
                builder.append(input[i]);
            }
            String ip = builder.toString();
            if (isValid2(ip)) {
                result.add(ip);
            }
            builder.delete(length, builder.length());
            return;
        }
        for (int i = index; i < input.length && i < index + 3; i++) {
            builder.append(input[i]);
            builder.append(".");
            search(input, i + 1, n - 1, builder, result);
            builder.deleteCharAt(builder.length()-1);
        }
        builder.delete(length, builder.length());
    }

    private boolean isValid2(String ip) {
        if (ip.startsWith(".") || ip.endsWith(".")) {
            return false;
        }
        String[] splits = ip.split("\\.");
        if (splits.length != 4) {
            return false;
        }
        for (String split : splits) {
            if (split.length() > 1 && split.startsWith("0")) {
                return false;
            }
            int val = Integer.parseInt(split);
            if (val > 255) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        RestoreIPAddresses s = new RestoreIPAddresses();
        System.out.println(s.restoreIpAddresses("25525511135"));
        System.out.println(s.restoreIpAddresses("0000"));
        System.out.println(s.restoreIpAddresses("1111"));
        System.out.println(s.restoreIpAddresses("010010"));

        System.out.println("-->");
        System.out.println(s.restoreIpAddresses2("25525511135"));
        System.out.println(s.restoreIpAddresses2("0000"));
        System.out.println(s.restoreIpAddresses2("1111"));
        System.out.println(s.restoreIpAddresses2("010010"));
    }
}
