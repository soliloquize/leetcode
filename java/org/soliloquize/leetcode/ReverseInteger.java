package org.soliloquize.leetcode;

public class ReverseInteger {

    /**
     http://leetcode.com/onlinejudge#question_7

     Reverse digits of an integer.

     Example1: x = 123, return 321
     Example2: x = -123, return -321
     */
    public int reverse(int x) {
        int flag = x < 0 ? -1 : 1;
        long y = Math.abs((long) x);

        long result = 0;
        while (y > 0) {
            result = result * 10 + y % 10;
            y /= 10;
        }
        return (int) (flag * result);
    }
}
