package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class ReverseLinkedListII {
    /**
     http://leetcode.com/oldoj#question_92

     Reverse a linked list from position m to n. Do it in-place and in one-pass.

     For example:
     Given 1->2->3->4->5->NULL, m = 2 and n = 4,

     return 1->4->3->2->5->NULL.

     Note:
     Given m, n satisfy the following condition:
     1 ? m ? n ? length of list.
     */


    public static class Pair<F, S> {
        public F first;
        public S second;
        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
    }

    public Pair<ListNode, ListNode> reverse(ListNode head) {
        ListNode p = head;
        ListNode q = p.next;
        head.next = null;
        while (q != null) {
            ListNode t = q.next;
            q.next = p;
            p = q;
            q = t;
        }
        return new Pair<ListNode, ListNode>(p, head);
    }

    public ListNode reverseBetween2(ListNode head, int m, int n) {
        if (head == null || head.next == null || m == n) {
            return head;
        }
        // 创建一个新的节点，避免原链表头反转带来的一些特殊处理
        ListNode newHead = new ListNode(Integer.MIN_VALUE);
        newHead.next = head;
        ListNode p = newHead;
        int index = 1;
        m += 1;
        n += 1;
        while (p != null && index < m - 1) {
            p = p.next;
            if (p != null) {
                index += 1;
            }
        }
        // 待反转节点
        ListNode z = p.next;
        ListNode q = p.next;
        while (q != null && index < n - 1) {
            q = q.next;
            if (q != null) {
                index += 1;
            }
        }
        // 暂存后半节点
        ListNode t = q.next;
        q.next = null;
        Pair<ListNode, ListNode> pair = reverse(z);
        p.next = pair.first;
        pair.second.next = t;
        return newHead.next;
    }

    public static void main(String[] args) {
        ReverseLinkedListII s = new ReverseLinkedListII();
        ListNode list = new ListNode(1);
        list.next = new ListNode(2);
        list.next.next = new ListNode(3);
//        list.next.next.next = new ListNode(4);
//        list.next.next.next.next = new ListNode(5);

        ListNode p = s.reverseBetween2(list, 1, 2);
        System.out.println(p.val);
        System.out.println(p.next.val);
        System.out.println(p.next.next.val);
//        System.out.println(p.next.next.next.val);
//        System.out.println(p.next.next.next.next.val);


    }


    public ListNode reverseBetween(ListNode head, int m, int n) {
        if (head == null || m == n) {
            return head;
        }

        int i = 1;
        ListNode p = head;
        // 进入反转之前的最后一个节点
        ListNode last = null;
        for (; i < m; i++) {
            last = p;
            p = p.next;
        }

        // 反转过程中上一次的节点
        ListNode z = null;
        ListNode q = null;
        ListNode t = null;
        // 反转的第一个元素
        ListNode f = null;
        boolean isBreak = false;
        for (; p != null; ) {
            q = p.next;

            // 是否超过了范围限制
            if (i > n) {
                isBreak = true;
                break;
            }

            // 进行反转，交换当前两个节点位置
            p.next = z;

            if (i == n) {
                break;
            }

            // 已经是最后一个元素，无需反转直接返回
            if (q == null) {
                break;
            }

            if (f == null) {
                f = p;
            }

            z = q;
            t = q.next;
            q.next = p;

            // 往后移动
            p = t;

            // 每做一次交换相当于往后移动两步
            i += 2;
        }

        if (!isBreak && !(p == null)) {
            if (last != null) {
                last.next = p;
            } else {
                head = p;
            }

            if (f != null) {
                f.next = q;
            }
        } else {
            if (last != null) {
                last.next = z;
            } else {
                head = z;
            }

            if (f != null) {
                f.next = p;
            }
        }

        return head;
    }
}
