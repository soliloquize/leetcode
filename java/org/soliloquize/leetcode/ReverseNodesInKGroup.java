package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class ReverseNodesInKGroup {
    /**
     http://leetcode.com/oldoj#question_25

     Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.

     If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

     You may not alter the values in the nodes, only nodes itself may be changed.

     Only constant memory is allowed.

     For example,
     Given this linked list: 1->2->3->4->5

     For k = 2, you should return: 2->1->4->3->5

     For k = 3, you should return: 3->2->1->4->5
     */

    public static class Pair<F, S> {
        public F first;
        public S second;
        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
    }

    public ListNode reverseKGroup2(ListNode head, int k) {
        if (head == null || k == 1) {
            return head;
        }

        Pair<ListNode, ListNode> pair = null;
        ListNode p = head;
        ListNode q = head;
        int step = 1;
        while (q != null) {
            // 记录长度
            q = q.next;
            if (q == null) {
                break;
            }
            step += 1;

            // 开始反转
            if (step == k) {
                // 记录下一轮反转头指针
                ListNode t = q.next;
                q.next = null;

                Pair<ListNode, ListNode> current = reverse(p);
                if (pair == null) {
                    pair = current;
                } else {
                    // 将一段段链表连接起来
                    pair.second.next = current.first;
                    pair.second = current.second;
                }

                // 重置
                pair.second.next = t;
                p = t;
                q = t;
                step = 1;
            }
        }
        if (pair == null) {
            return head;
        }
        return pair.first;
    }

    public Pair<ListNode, ListNode> reverse(ListNode head) {
        ListNode p = head;
        ListNode q = p.next;
        head.next = null;
        while (q != null) {
            ListNode t = q.next;
            q.next = p;
            p = q;
            q = t;
        }
        return new Pair<ListNode, ListNode>(p, head);
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);

        ReverseNodesInKGroup s = new ReverseNodesInKGroup();
        ListNode p = s.reverseKGroup2(head, 5);
        ListNode q = p;
        while (q != null) {
            System.out.println(q.val);
            q = q.next;
        }

    }

    public ListNode reverseKGroup(ListNode head, int k) {
        if (k <= 1) {
            return head;
        }
        ListNode p = head;
        ListNode root = head;
        ListNode lastP = null;
        while (p != null) {
            ListNode q = p;
            for (int i = 0; i < k - 1 && q != null; i++) {
                q = q.next;
            }
            if (q == null) {
                if (lastP != null) {
                    lastP.next = p;
                }
                break;
            }
            // 记录下一段开始
            ListNode next = q.next;
            // 分隔每一段
            q.next = null;
            ListNode x = p;
            ListNode z = null;
            while (x != null) {
                ListNode y = x.next;
                x.next = z;
                if (y == null) {
                    z = x;
                    break;
                } else {
                    ListNode t = y.next;
                    y.next = x;
                    x = t;
                    z = y;
                }
            }

            // 记录上一段反转后的最后一个节点
            if (lastP != null) {
                lastP.next = z;
            }
            lastP = p;

            // 更新新的根节点
            if (root == head) {
                root = z;
            }

            p = next;
        }
        return root;
    }
}
