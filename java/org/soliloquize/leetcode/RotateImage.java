package org.soliloquize.leetcode;

import java.util.Arrays;

public class RotateImage {
    /**
     http://leetcode.com/oldoj#question_48

     You are given an n x n 2D matrix representing an image.

     Rotate the image by 90 degrees (clockwise).

     Follow up:
     Could you do this in-place?
     */

    class Pair {
        public int x;
        public int y;
        public Pair(int x, int y) {
            set(x, y);
        }
        public void set(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }

    public void rotate(int[][] matrix) {
        if (matrix.length <= 1) {
            return;
        }
        int n = matrix.length;
        Pair[] positions = new Pair[] {new Pair(0, 0), new Pair(0, 0), new Pair(0, 0), new Pair(0, 0)} ;
        for (int k = 0; k < n - 1; k++) {
            // 初始化四个端点的坐标
            positions[0].set(0 + k, 0 + k);
            positions[1].set(0 + k, n - 1 - k);
            positions[2].set(n - 1 - k, n - 1 - k);
            positions[3].set(n - 1 - k, 0 + k);

            for (int i = 0; i < n - 1 - 2 * k; i++) {
                int tmp = matrix[positions[0].x][positions[0].y];
                matrix[positions[0].x][positions[0].y] = matrix[positions[3].x][positions[3].y];
                matrix[positions[3].x][positions[3].y] = matrix[positions[2].x][positions[2].y];
                matrix[positions[2].x][positions[2].y] = matrix[positions[1].x][positions[1].y];
                matrix[positions[1].x][positions[1].y] = tmp;

                positions[0].y += 1;
                positions[1].x += 1;
                positions[2].y -= 1;
                positions[3].x -= 1;
            }
        }
    }

    public void rotate2(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return;
        }
        rotate2(matrix, 0, 0, matrix.length);
    }

    private void rotate2(int[][] matrix, int top, int left, int n) {
        if (n <= 1) {
            return;
        }
        Pair[] positions = new Pair[] {
                new Pair(top, left),
                new Pair(top, left + n - 1),
                new Pair(top + n - 1, left + n - 1),
                new Pair(top + n - 1, left)} ;
        for (int i = 0; i < n - 1; i++) {
            int tmp = matrix[positions[0].x][positions[0].y];
            matrix[positions[0].x][positions[0].y] = matrix[positions[3].x][positions[3].y];
            matrix[positions[3].x][positions[3].y] = matrix[positions[2].x][positions[2].y];
            matrix[positions[2].x][positions[2].y] = matrix[positions[1].x][positions[1].y];
            matrix[positions[1].x][positions[1].y] = tmp;
            positions[0].y += 1;
            positions[1].x += 1;
            positions[2].y -= 1;
            positions[3].x -= 1;
        }
        rotate2(matrix, top + 1, left + 1, n - 2);
    }

    public static void main(String[] args) throws Exception {
        RotateImage s = new RotateImage();
        int[][] matrix = new int[][] {
                new int[] {1, 2, 3, 4},
                new int[] {5,6,7,8},
                new int[] {9,10,11,12},
                new int[] {13,14,15,16},
        };
        s.rotate(matrix);
        System.out.println(Arrays.deepToString(matrix));

        matrix = new int[][] {
                new int[] {1, 2, 3, 4},
                new int[] {5,6,7,8},
                new int[] {9,10,11,12},
                new int[] {13,14,15,16},
        };
        s.rotate2(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }
}
