package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class RotateList {
    /**
     http://leetcode.com/oldoj#question_61

     Given a list, rotate the list to the right by k places, where k is non-negative.

     For example:
     Given 1->2->3->4->5->NULL and k = 2,
     return 4->5->1->2->3->NULL.
     */
    public ListNode rotateRight(ListNode head, int n) {
        if (head == null) {
            return null;
        }
        int length = 0;
        ListNode p = head;
        while (p != null) {
            p = p.next;
            length += 1;
        }

        if (length == n) {
            return head;
        }

        n = n % length;

        int start = length - n - 1;
        p = head;
        while (start > 0) {
            p = p.next;
            start -= 1;
        }

        ListNode newHead = p.next;
        p.next = null;
        ListNode q = newHead;
        if (q == null) {
            return head;
        }
        while (q.next != null) {
            q = q.next;
        }
        q.next = head;
        return newHead;
    }
}
