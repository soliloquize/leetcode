package org.soliloquize.leetcode;

import java.util.Arrays;

public class ScrambleString {
    /**
     http://oj.leetcode.com/problems/scramble-string/

     Given a string s1, we may represent it as a binary tree by partitioning it to two non-empty substrings recursively.

     Below is one possible representation of s1 = "great":

     great
     /    \
     gr    eat
     / \    /  \
     g   r  e   at
     / \
     a   t
     To scramble the string, we may choose any non-leaf node and swap its two children.

     For example, if we choose the node "gr" and swap its two children, it produces a scrambled string "rgeat".

     rgeat
     /    \
     rg    eat
     / \    /  \
     r   g  e   at
     / \
     a   t
     We say that "rgeat" is a scrambled string of "great".

     Similarly, if we continue to swap the children of nodes "eat" and "at", it produces a scrambled string "rgtae".

     rgtae
     /    \
     rg    tae
     / \    /  \
     r   g  ta  e
     / \
     t   a
     We say that "rgtae" is a scrambled string of "great".

     Given two strings s1 and s2 of the same length, determine if s2 is a scrambled string of s1.
     */
    public boolean isScramble(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        return func(s1, s2, 0, s1.length());
    }

    private boolean func(String s1, String s2, int start, int end) {
        boolean flag = internal(s1, s2, start, end);
        if (flag) {
            return true;
        }

        String s3 = reverse(s1, start, end);
        flag = internal(s3, s2, start, end);

        return flag;
    }

    private boolean internal(String s1, String s2, int start, int end) {
        if (isEqual(s1, s2, start, end)) {
            return true;
        }

        if (isEqual(reverse(s1, start, end), s2, start, end)) {
            return true;
        }

        int[] count = new int[26];
        Arrays.fill(count, 0);

        for (int i = start; i < end - 1; i++) {
            count[s1.charAt(i) - 'a'] += 1;
            count[s2.charAt(i) - 'a'] -= 1;

            if (isFit(count)) {
                boolean flag = func(s1, s2, start, i + 1);
                if (!flag) {
                    return false;
                }
                flag = func(s1, s2, i + 1, end);
                if (!flag) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    private boolean isFit(int[] count) {
        for (int i = 0; i < count.length; i++) {
            if (count[i] != 0) {
                return false;
            }
        }
        return true;
    }

    private boolean isEqual(String s1, String s2, int start, int end) {
        for (int i = 0; i < end - start; i++) {
            boolean flag = isRotateEqual(s1, s2, start, end, i);
            if (flag) {
                return true;
            }
        }
        return false;
    }

    private boolean isRotateEqual(String s1, String s2, int start, int end, int rotate) {
        for (int i = start, j = start + rotate; j < end; i++, j++) {
            if (s1.charAt(i) != s2.charAt(j)) {
                return false;
            }
        }
        for (int j = start, i = end - rotate; j < start + rotate && i < end; i++, j++) {
            if (s1.charAt(i) != s2.charAt(j)) {
                return false;
            }
        }
        return true;
    }

    private String reverse(String str, int start, int end) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < start; i++) {
            builder.append(str.charAt(i));
        }
        for (int i = end-1; i >= start; i--) {
            builder.append(str.charAt(i));
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        ScrambleString s = new ScrambleString();
//        boolean flag = s.isScramble("aaccd", "acaad");
//        boolean flag = s.isScramble("abc", "acb");
//        boolean flag = s.isScramble("ab", "aa");
//        boolean flag = s.isScramble("abc", "cba");
        boolean flag = s.isScramble("abab", "bbaa");
        System.out.println(flag);
    }
}
