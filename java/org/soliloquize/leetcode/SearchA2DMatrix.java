package org.soliloquize.leetcode;

public class SearchA2DMatrix {
    /**
     http://leetcode.com/oldoj#question_74

     Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:


     •Integers in each row are sorted from left to right.
     •The first integer of each row is greater than the last integer of the previous row.

     For example,

     Consider the following matrix:

     [
     [1,   3,  5,  7],
     [10, 11, 16, 20],
     [23, 30, 34, 50]
     ]
     Given target = 3, return true
     */

    public static void main(String[] args) {
        SearchA2DMatrix s = new SearchA2DMatrix();
        int[][] matrix = new int[][] {
                new int[] {1, 3, 5, 7},
                new int[] {10, 11, 16, 20},
                new int[] {23, 30, 34, 50},
        };
        System.out.println(s.searchMatrix(matrix, 3));
        System.out.println(s.searchMatrix2(matrix, 3));
    }

    public boolean searchMatrix2(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        int top = 0;
        int bottom = m-1;

        int row = -1;
        while (top <= bottom) {
            int middle = (top + bottom) / 2;
            if (target < matrix[middle][0]) {
                bottom = middle - 1;
            } else if (target > matrix[middle][n-1]) {
                top = middle + 1;
            } else {
                row = middle;
                break;
            }
        }
        if (row == -1) {
            return false;
        }

        int left = 0;
        int right = n - 1;

        while (left <= right) {
            int middle = (left + right) / 2;
            int val = matrix[row][middle];
            if (val == target) {
                return true;
            } else if (val > target) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return false;
    }


    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0) {
            return false;
        }

        int m = matrix.length;
        int n = matrix[0].length;

        int rs = 0;
        int re = m - 1;

        int x = -1;
        int y = -1;

        // find the row
        while (rs <= re) {
            int middle = (rs + re) / 2;
            if (matrix[middle][0] > target) {
                re = middle - 1;
            } else if (matrix[middle][n-1] < target) {
                rs = middle + 1;
            } else {
                x = middle;
                break;
            }
        }

        if (x == -1) {
            return false;
        }

        int left = 0;
        int right = n - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (matrix[x][middle] > target) {
                right = middle - 1;
            } else if (matrix[x][middle] < target) {
                left = middle + 1;
            } else {
                return true;
            }
        }
        return false;
    }
}
