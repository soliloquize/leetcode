package org.soliloquize.leetcode;

public class SearchForARange {
    /**
     http://leetcode.com/oldoj#question_34

     Given a sorted array of integers, find the starting and ending position of a given target value.

     Your algorithm's runtime complexity must be in the order of O(log n).

     If the target is not found in the array, return [-1, -1].

     For example,
     Given [5, 7, 7, 8, 8, 10] and target value 8,
     return [3, 4].
     */

    public static void main(String[] args) {
        SearchForARange s = new SearchForARange();
        int[] res = s.searchRange(new int[] {5, 7, 7, 8, 8, 10}, 8);
        System.out.println(String.format("%d, %d", res[0], res[1]));


        res = s.searchRange2(new int[] {5, 7, 7, 8, 8, 10}, 8);
        System.out.println(String.format("%d, %d", res[0], res[1]));
    }

    public int[] searchRange2(int[] A, int target) {
        return new int[] {findLeft2(A, target), findRight2(A, target)};
    }

    private int findRight2(int[] A, int target) {
        int left = 0;
        int right = A.length;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] > target) {
                right = middle - 1;
            } else if (A[middle] < target) {
                left = middle + 1;
            } else {
                if (middle + 1 < A.length && A[middle] == A[middle + 1]) {
                    left = middle + 1;
                } else {
                    return middle;
                }
            }
        }
        return -1;
    }

    private int findLeft2(int[] A, int target) {
        int left = 0;
        int right = A.length;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] > target) {
                right = middle - 1;
            } else if (A[middle] < target){
                left = middle + 1;
            } else {
                if (middle > 0 && A[middle - 1] == A[middle]) {
                    right = middle - 1;
                } else {
                    return middle;
                }
            }
        }
        return -1;
    }


    public int[] searchRange(int[] A, int target) {
        return new int[] {findLeft(A, target), findRight(A, target)};
    }

    private int findLeft(int[] A, int target) {
        int left = 0;
        int right = A.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] > target) {
                right = middle - 1;
            } else if (A[middle] < target) {
                left = middle + 1;
            } else {
                if (middle > 0 && A[middle - 1] == target) {
                    right = middle - 1;
                } else {
                    return middle;
                }
            }
        }
        return -1;
    }

    private int findRight(int[] A, int target) {
        int left = 0;
        int right = A.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] > target) {
                right = middle - 1;
            } else if (A[middle] < target) {
                left = middle + 1;
            } else {
                if (middle < A.length - 1 && A[middle + 1] == target) {
                    left = middle + 1;
                } else {
                    return middle;
                }
            }
        }
        return -1;
    }
}
