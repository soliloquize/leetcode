package org.soliloquize.leetcode;

public class SearchInRotatedSortedArray {

    /**
     http://leetcode.com/onlinejudge#question_33

     Suppose a sorted array is rotated at some pivot unknown to you beforehand.

     (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

     You are given a target value to search. If found in the array return its index, otherwise return -1.

     You may assume no duplicate exists in the array.
     */

    public static void main(String[] args) {
        SearchInRotatedSortedArray s = new SearchInRotatedSortedArray();
        System.out.println(s.search(new int[] {4, 5, 6, 7, 0, 1, 2}, 5));
        System.out.println(s.search(new int[] {1, 2, 3, 4, 5, 6, 0}, 5));
        System.out.println("->");
        System.out.println(s.search2(new int[] {4, 5, 6, 7, 0, 1, 2}, 5));
        System.out.println(s.search2(new int[] {1, 2, 3, 4, 5, 6, 0}, 5));
    }

    public int search2(int[] A, int target) {
        int pivot = findPivot2(A);
        int index = binarySearch2(A, 0, pivot-1, target);
        if (index >= 0) {
            return index;
        }
        return binarySearch2(A, pivot, A.length - 1, target);
    }

    private int binarySearch2(int [] A, int start, int end, int target) {
        while (start <= end) {
            int middle = (start + end) / 2;
            if (A[middle] == target) {
                return middle;
            } else if (A[middle] > target) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
        }
        return -1;
    }

    private int findPivot2(int [] A) {
        int left = 0;
        int right = A.length - 1;
        while (left < right) {
            int middle = (left + right) / 2;
            if (middle == left) {
                return right;
            }
            if (A[left] < A[middle]) {
                left = middle;
            } else if (A[left] > A[middle]) {
                right = middle;
            }
        }
        return 0;
    }


    public int search(int[] A, int target) {
        if (A == null) {
            return -1;
        }
        if (A.length == 1) {
            return A[0] == target ? 0 : -1;
        }
        int pivot = findPivot(A);
        int pos = binarySearch(A, 0, pivot-1, target);
        if (pos != -1) {
            return pos;
        }
        return binarySearch(A, pivot, A.length - 1, target);
    }

    private int findPivot(int [] A) {
        int start = 0;
        int end = A.length - 1;
        while (start < end) {
            int middle = (start + end) / 2;
            if (start == middle) {
                return end;
            }
            if (A[start] < A[middle]) {
                start = middle;
            } else {
                end = middle;
            }
        }
        return -1;
    }

    private int binarySearch(int [] A, int start, int end, int target) {
        while (start <= end) {
            int middle = (start + end) / 2;
            if (A[middle] == target) {
                return middle;
            } else {
                if (A[middle] > target) {
                    end = middle - 1;
                } else {
                    start = middle + 1;
                }
            }
        }
        return -1;
    }
}
