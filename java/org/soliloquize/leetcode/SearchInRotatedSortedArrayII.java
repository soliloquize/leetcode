package org.soliloquize.leetcode;

public class SearchInRotatedSortedArrayII {
    /**
     http://oj.leetcode.com/problems/search-in-rotated-sorted-array-ii/

     Follow up for "Search in Rotated Sorted Array":
     What if duplicates are allowed?

     Would this affect the run-time complexity? How and why?

     Write a function to determine if a given target is in the array.
     */

    public static void main(String[] args) {
        SearchInRotatedSortedArrayII s = new SearchInRotatedSortedArrayII();
        System.out.println(s.search(new int[] {3, 3, 1, 1, 2, 2, 0}, 2));
        System.out.println(s.search2(new int[] {3, 3, 3, 3, 0, 1, 1, 1, 1, 1, 2}, 1));
    }

    public boolean search2(int[] A, int target) {
        int pivot  =findPivot(A);
        return binarySearch(A, 0, pivot-1, target) || binarySearch(A, pivot, A.length-1, target);
    }

    private boolean binarySearch(int[] A, int left, int right, int target) {
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] == target) {
                return true;
            } else if (A[middle] > target) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return false;
    }

    private int findPivot(int[] A) {
        int left = 0;
        int right = A.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (middle == left) {
                return right;
            }
            if (A[middle] >= A[left]) {
                left = middle;
            } else {
                right = middle;
            }
        }
        return -1;
    }


    public boolean search(int[] A, int target) {
        for (int i = 0; i < A.length; i++) {
            if (target == A[i]) {
                return true;
            }
        }
        return false;
    }
}
