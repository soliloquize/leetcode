package org.soliloquize.leetcode;

public class SearchInsertPosition {
    /**
     http://leetcode.com/oldoj#question_35

     Given a sorted array and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

     You may assume no duplicates in the array.

     Here are few examples.
     [1,3,5,6], 5 → 2
     [1,3,5,6], 2 → 1
     [1,3,5,6], 7 → 4
     [1,3,5,6], 0 → 0
     */

    public static void main(String[] args) {
        SearchInsertPosition s =  new SearchInsertPosition();
        System.out.println(s.searchInsert(new int[] {1, 3, 5, 6}, 5));
        System.out.println(s.searchInsert(new int[] {1, 3, 5, 6}, 2));
        System.out.println(s.searchInsert(new int[] {1, 3, 5, 6}, 7));
        System.out.println(s.searchInsert(new int[] {1, 3, 5, 6}, 0));

        System.out.println(s.searchInsert2(new int[] {1, 3, 5, 6}, 5));
        System.out.println(s.searchInsert2(new int[] {1, 3, 5, 6}, 2));
        System.out.println(s.searchInsert2(new int[] {1, 3, 5, 6}, 7));
        System.out.println(s.searchInsert2(new int[] {1, 3, 5, 6}, 0));
    }

    public int searchInsert2(int[] A, int target) {
        int left = 0;
        int right = A.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] == target) {
                return middle;
            } else if (A[middle] > target) {
                right = middle - 1;
            } else {
                left = middle + 1;
            }
        }
        return left;
    }


    public int searchInsert(int[] A, int target) {
        if (A.length == 0) {
            return 0;
        }
        int left = 0;
        int right = A.length - 1;

        while (left <= right) {
            int middle = (left + right) / 2;
            if (A[middle] > target) {
                right = middle - 1;
            } else if (A[middle] < target) {
                left = middle + 1;
            } else {
                return middle;
            }
        }
        return left;
    }
}
