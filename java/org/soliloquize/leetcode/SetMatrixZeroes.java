package org.soliloquize.leetcode;

public class SetMatrixZeroes {
    /**
     http://oj.leetcode.com/problems/set-matrix-zeroes

     Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.

     click to show follow up.

     Follow up:
     Did you use extra space?
     A straight forward solution using O(mn) space is probably a bad idea.
     A simple improvement uses O(m + n) space, but still not the best solution.
     Could you devise a constant space solution?
     */

    public void setZeros2(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0) {
            return;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        boolean firstRow = false;
        for (int j = 0; j < n; j++) {
            firstRow |= matrix[0][j] == 0;
        }
        boolean firstColumn = false;
        for (int i = 0; i < m; i++) {
            firstColumn |= matrix[i][0] == 0;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }

        for (int j = 1; j < n; j++) {
            if (matrix[0][j] == 0) {
                for (int k = 1; k < m; k++) {
                    matrix[k][j] = 0;
                }
            }
        }

        for (int i = 1; i < m; i++) {
            if (matrix[i][0] == 0) {
               for (int k = 1; k < n; k++) {
                   matrix[i][k] = 0;
               }
            }
        }

        if (firstRow) {
            for (int j = 0; j < n; j++) {
                matrix[0][j] = 0;
            }
        }

        if (firstColumn) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }
    }

    public void setZeroes(int[][] matrix) {
        if (matrix.length == 0) {
            return;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        // 用第一行第一列记录对应的行与列是否包含0
        boolean firstRow = false;
        for (int j = 0; j < n; j++) {
            firstRow |= matrix[0][j] == 0;
        }
        boolean firstColumn = false;
        for (int i = 0; i < matrix.length; i++) {
            firstColumn |= matrix[i][0] == 0;
        }

        // 逐行查看是否存在0
        for (int i = 1; i < m; i++) {
            boolean flag = false;
            for (int j = 1; j < n; j++) {
                flag |= matrix[i][j] == 0;
            }
            if (flag) {
                matrix[i][0] = 0;
            }
        }

        // 逐列查看是否存在0
        for (int j = 1; j < n; j++) {
            boolean flag = false;
            for (int i = 1; i < m; i++) {
                flag |= matrix[i][j] == 0;
            }
            if (flag) {
                matrix[0][j] = 0;
            }
        }
        for (int i = 0, j = 1; j < n; j++) {
            if (matrix[i][j] == 0) {
                for (int k = 0; k < m; k++) {
                    matrix[k][j] = 0;
                }
            }
        }

        for (int i = 1, j = 0; i < m; i++) {
            if (matrix[i][j] == 0) {
                for (int k = 0; k < n; k++) {
                    matrix[i][k] = 0;
                }
            }
        }

        if (firstRow) {
            for (int j = 0; j < n; j++) {
                matrix[0][j] = 0;
            }
        }

        if (firstColumn) {
            for (int i = 0; i < m; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}
