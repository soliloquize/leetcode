package org.soliloquize.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class SimplifyPath {
    /**
     http://leetcode.com/oldoj#question_71

     Given an absolute path for a file (Unix-style), simplify it.

     For example,
     path = "/home/", => "/home"
     path = "/a/./b/../../c/", => "/c"

     Corner Cases:

     •Did you consider the case where path = "/../"?
     In this case, you should return "/".
     •Another corner case is the path might contain multiple slashes '/' together, such as "/home//foo/".
     In this case, you should ignore redundant slashes and return "/home/foo".
     */

    public static void main(String[] args) {
        SimplifyPath s = new SimplifyPath();
        System.out.println(s.simplifyPath("/home/"));
        System.out.println(s.simplifyPath("/a/./b/../../c/"));

        System.out.println("->");
        System.out.println(s.simplifyPath2("/home/"));
        System.out.println(s.simplifyPath2("/a/./b/../../c/"));
    }

    public String simplifyPath2(String path) {
        Deque<String> stack = new ArrayDeque<String>();
        String[] splits = path.split("/");
        for (String split : splits) {
            if (split.trim().length() == 0) {
                continue;
            }
            if (split.equals(".")) {
                continue;
            }
            if (split.endsWith("..")) {
                if (stack.size() > 0) {
                    stack.pop();
                }
                continue;
            }
            stack.push(split);
        }

        StringBuilder builder = new StringBuilder();
        while (stack.size() > 0) {
            builder.append("/").append(stack.poll());
        }
        if (builder.length() == 0) {
            builder.append("/");
        }
        return builder.toString();
    }

    public String simplifyPath(String path) {
        String[] splits = path.split("/");

        List<String> list = new ArrayList<String>();
        for (String str : splits) {
            str = str.trim();
            if (str.length() == 0) {
                continue;
            }
            if (str.equals(".")) {
                continue;
            }
            if (str.endsWith("..")) {
                if (list.size() > 0) {
                    list.remove(list.size()-1);
                }
                continue;
            }
            list.add(str);
        }
        StringBuilder builder = new StringBuilder();
        for (String str : list) {
            builder.append("/").append(str);
        }
        if (builder.length() == 0) {
            builder.append("/");
        }
        return builder.toString();
    }
}
