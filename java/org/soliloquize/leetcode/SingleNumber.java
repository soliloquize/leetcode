package org.soliloquize.leetcode;

public class SingleNumber {
    /**
     http://oj.leetcode.com/problems/single-number/

     Given an array of integers, every element appears twice except for one. Find that single one.

     Note:
     Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
     */
    public int singleNumber(int[] A) {
        if (A == null || A.length == 0) {
            return 0;
        }
        int res = A[0];
        for (int i = 1; i < A.length; i++) {
            res ^= A[i];
        }
        return res;
    }

    public static void main(String[] args) {
        SingleNumber s = new SingleNumber();
        System.out.println(s.singleNumber(new int[] {1, 1, 2, 2, 3}));
    }
}
