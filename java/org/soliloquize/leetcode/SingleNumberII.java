package org.soliloquize.leetcode;

import java.util.Arrays;

public class SingleNumberII {

    /**
     http://oj.leetcode.com/problems/single-number-ii/

     Given an array of integers, every element appears three times except for one. Find that single one.

     Note:
     Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
     */
    public int singleNumber(int[] A) {
        int [] count = new int[32];
        Arrays.fill(count, 0);
        int result = 0;

        for (int i = 0; i < 32; ++i) {
            count[i] = 0;
            for (int j = 0; j < A.length; ++j) {
                if (((A[j] >> i) & 1) == 1) {
                    count[i] = (count[i] + 1) % 3;
                }
            }
            result |= (count[i] << i);
        }
        return result;
    }

    public int singleNumber2(int[] A) {
        int length = 32;
        int[] flags = new int[length];

        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < length; j++) {
                flags[j] += (A[i] >> j) & 1;
                flags[j] %= 3;
            }
        }

        int res = 0;
        for (int i = length-1; i >= 0; i--) {
            res = (res << 1) + flags[i];
        }
        return res;
    }

    public static void main(String[] args) {
        SingleNumberII s = new SingleNumberII();
        System.out.println(s.singleNumber2(new int[] {1, 1, 1, 2, 2, 2, 3, 3, 3, 4}));
    }
}
