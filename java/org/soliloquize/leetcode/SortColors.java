package org.soliloquize.leetcode;

import java.util.Arrays;

public class SortColors {
    /**
     http://leetcode.com/oldoj#question_75

     Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, with the colors in the order red, white and blue.

     Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

     Note:
     You are not suppose to use the library's sort function for this problem.
     */

    public void sortColors3(int[] A) {
        int p = 0;
        int q = A.length-1;

        for (int i = 0; i < A.length; ) {
            if (A[i] == 0) {
                swap(A, i, p);
                p += 1;
            } else if (A[i] == 2) {
                swap(A, i, q);
                q -= 1;
            } else {
                i += 1;
            }
        }
    }

    public void sortColors2(int[] A) {
        int p = 0;
        int q = A.length - 1;
        for (int i = 0; i < A.length; ) {
            if (A[i] == 0 && i > p) {
                swap(A, i, p);
                p += 1;
            } else if (A[i] == 2 && i < q) {
                swap(A, i, q);
                q -= 1;
            } else {
                i += 1;
            }
        }
    }

    public static void main(String[] args) {
        SortColors s = new SortColors();
        int[] A = new int[] {0, 1, 2, 0, 1, 2, 2, 1, 0, 2, 1, 0};
        s.sortColors2(A);
        for (int i = 0; i < A.length; i++) {
            System.out.println(A[i]);
        }
    }

    private void swap(int[] A, int i, int j) {
        int tmp = A[i];
        A[i] = A[j];
        A[j] = tmp;
    }


    public void sortColors(int[] A) {
        int zero = 0;
        int one = 0;
        int two = 0;
        for (int i = 0; i < A.length; i++) {
            int n = A[i];
            switch (n) {
                case 0:
                    zero += 1;
                    break;
                case 1:
                    one += 1;
                    break;
                case 2:
                    two += 1;
            }
        }
        for (int i = 0; i < A.length; ) {
            while (zero > 0) {
                A[i] = 0;
                zero -= 1;
                i += 1;
            }
            while (one > 0) {
                A[i] = 1;
                one -= 1;
                i += 1;
            }
            while (two > 0) {
                A[i] = 2;
                two -= 1;
                i += 1;
            }
        }
    }
}
