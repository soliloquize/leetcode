package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

import java.util.Map;
import java.util.TreeMap;

public class SortList {
    /**
     http://oj.leetcode.com/problems/sort-list/

     Sort a linked list in O(n log n) time using constant space complexity.
     */

    long smallest = Integer.MAX_VALUE;
    long largest = Integer.MIN_VALUE;
    public ListNode sortList(ListNode head) {
        return sort(head, null, 0);
    }

    private ListNode sort(ListNode head, ListNode end, int depth) {
        if (head == null || head == end || head.next == null) {
            return head;
        }
        smallest = Integer.MAX_VALUE;
        largest = Integer.MIN_VALUE;
        findRange(head, end);

        if (Math.abs(largest - smallest) < 100) {
            countSort(head, end);
            return head;
        }
        // 选取第一个元素作为pivot， 将比pivot元素小的移动到链表前半部
        int pivot = head.val;
        ListNode p = head.next;
        ListNode q = head;
        ListNode z = head;
        while (p != null && q != null && q != end) {
            if (q.val < pivot) {
                swap(p, q);

                z = p;
                p = p.next;
            }
            q = q.next;
        }

        swap(head, z);
        sort(head, z, depth + 1);
        sort(p, end, depth + 1);
        return head;
    }

    private void swap(ListNode p, ListNode q) {
        int tmp = p.val;
        p.val = q.val;
        q.val = tmp;
    }

    private void findRange(ListNode p, ListNode end) {
        ListNode q = p;
        while (q != end) {
            smallest = Math.min(q.val, smallest);
            largest = Math.max(q.val, largest);
            q = q.next;
        }
    }

    private void countSort(ListNode p, ListNode end) {
        Map<Integer, Integer> tree = new TreeMap<Integer, Integer>();
        ListNode q = p;
        while (q != end) {
            if (tree.containsKey(q.val)) {
                tree.put(q.val, tree.get(q.val) + 1);
            } else {
                tree.put(q.val, 1);
            }
            q = q.next;
        }
        q = p;
        for (Map.Entry<Integer, Integer> entry : tree.entrySet()) {
            int k = entry.getKey();
            int c = entry.getValue();
            while (c > 0) {
                q.val = k;
                q = q.next;
                c -= 1;
            }
        }
    }
}
