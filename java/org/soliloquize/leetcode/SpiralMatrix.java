package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Collections;

public class SpiralMatrix {

    /**
     http://leetcode.com/onlinejudge#question_54

     Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

     For example,
     Given the following matrix:

     [
     [ 1, 2, 3 ],
     [ 4, 5, 6 ],
     [ 7, 8, 9 ]
     ]
     You should return [1,2,3,6,9,8,7,4,5].
     */

    public ArrayList<Integer> spiralOrder2(int[][] matrix) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return result;
        }
        spiralOrderRecursive2(matrix, 0, 0, matrix.length, matrix[0].length, result);
        return result;
    }

    private void spiralOrderRecursive2(int[][] matrix, int top, int left, int bottom, int right, ArrayList<Integer> spiral) {
        if (top >= bottom || left >= right) {
            return;
        }
        for (int i = top, j = left; j < right - 1; j++) {
            spiral.add(matrix[i][j]);
        }
        for (int i = top, j = right - 1; i < bottom - 1; i++) {
            spiral.add(matrix[i][j]);
        }
        for (int i = bottom - 1, j = right - 1; j > left; j--) {
            spiral.add(matrix[i][j]);
        }
        for (int i = bottom - 1, j = left; i > top; i--) {
            spiral.add(matrix[i][j]);
        }
        spiralOrderRecursive2(matrix, top + 1, left + 1, bottom - 1, right - 1, spiral);
    }

    public static void main(String[] args) {
        SpiralMatrix s = new SpiralMatrix();
        System.out.println(s.spiralOrder(new int[][] {new int[] {1, 2, 3, 4}, new int[] {5, 6, 7, 8}, new int[] {9, 10, 11, 12}, new int[] {13, 14, 15, 16}}));
        System.out.println(s.spiralOrder2(new int[][] {new int[] {1, 2, 3, 4}, new int[] {5, 6, 7, 8}, new int[] {9, 10, 11, 12}, new int[] {13, 14, 15, 16}}));
    }


    public ArrayList<Integer> spiralOrder(int[][] matrix) {
        ArrayList<Integer> spiral = new ArrayList<Integer>();
        if (matrix != null && matrix.length > 0) {
            spiralOrderRecursive(matrix, 0, 0, matrix.length, matrix[0].length, spiral);
        }
        return spiral;
    }

    private void spiralOrderRecursive(int[][] matrix, int top, int left, int bottom, int right, ArrayList<Integer> spiral) {
        if (left >= right || top >= bottom) {
            return;
        }
        for (int i = top, j = left; j < right; j++) {
            spiral.add(matrix[i][j]);
        }
        for (int j = right - 1, i = top + 1; i < bottom; i++) {
            spiral.add(matrix[i][j]);
        }
        for (int i = bottom - 1, j = right - 2; j >= left && i > top; j--) {
            spiral.add(matrix[i][j]);
        }
        for (int i = bottom - 2, j = left; i > top && j < right - 1; i--) {
            spiral.add(matrix[i][j]);
        }
        spiralOrderRecursive(matrix, top + 1, left + 1, bottom - 1, right - 1, spiral);
    }
}
