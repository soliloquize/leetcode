package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class SpiralMatrixII {
    /**
     http://leetcode.com/onlinejudge#question_59

     Given an integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

     For example,
     Given n = 3,

     You should return the following matrix:
     [
     [ 1, 2, 3 ],
     [ 8, 9, 4 ],
     [ 7, 6, 5 ]
     ]
     */
    public int[][] generateMatrix(int n) {
        int [][] matrix = new int[n][n];
        int spiral = 1;
        spiralOrderRecursive(matrix, 0, 0, n, n, spiral);
        return matrix;
    }

    private void spiralOrderRecursive(int[][] matrix, int top, int left, int bottom, int right, int spiral) {
        if (left >= right || top >= bottom) {
            return;
        }
        for (int i = top, j = left; j < right; j++) {
            matrix[i][j] = spiral++;
        }
        for (int j = right - 1, i = top + 1; i < bottom; i++) {
            matrix[i][j] = spiral++;
        }
        for (int i = bottom - 1, j = right - 2; j >= left && i > top; j--) {
            matrix[i][j] = spiral++;
        }
        for (int i = bottom - 2, j = left; i > top && j < right - 1; i--) {
            matrix[i][j] = spiral++;
        }
        spiralOrderRecursive(matrix, top + 1, left + 1, bottom - 1, right - 1, spiral);
    }

    public static void main(String[] args) {
        SpiralMatrixII s = new SpiralMatrixII();
        int[][] matrix = s.generateMatrix(3);
        System.out.println(Arrays.deepToString(matrix));
    }
}
