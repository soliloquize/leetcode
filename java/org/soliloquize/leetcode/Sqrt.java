package org.soliloquize.leetcode;

public class Sqrt {
    /**
     http://leetcode.com/oldoj#question_69

     Implement int sqrt(int x).

     Compute and return the square root of x.
     */

    public int sqrt2(int x) {
        if (x == 0) {
            return 0;
        }
        if (x == 1) {
            return 1;
        }
        int low = 0;
        int up = x / 2;

        while (low <= up) {
            int middle = (low + up) / 2;
            int tmp = middle * middle;
            if (tmp / middle != middle) {
                up = middle - 1;
                continue;
            }
            if (tmp == x) {
                return middle;
            } else if (tmp < x) {
                low = middle + 1;
            } else {
                up = middle - 1;
            }
        }
        return low - 1;
    }

    public static void main(String[] args) {
        Sqrt s = new Sqrt();
        System.out.println(s.sqrt(100));
        System.out.println(s.sqrt(24));
        System.out.println(s.sqrt2(0));
        System.out.println(s.sqrt2(1));
        System.out.println(s.sqrt2(100));
        System.out.println(s.sqrt2(25));
        System.out.println(s.sqrt2(24));
        System.out.println(s.sqrt2(16));
    }

    public int sqrt(int x) {
        if (x == 0) {
            return 0;
        }
        if (x == 1) {
            return 1;
        }
        int left = 0;
        int right = x;
        while (left <= right) {
            int middle = (left + right) / 2;
            int tmp = middle * middle;
            // 越界
            if (middle != tmp / middle) {
                right = middle - 1;
            } else if (tmp > x) {
                right = middle - 1;
            } else if (tmp < x) {
                left = middle + 1;
            } else {
                return middle;
            }
        }
        return left - 1;
    }
}
