package org.soliloquize.leetcode;

public class StrStr {
    /**
     * http://oj.leetcode.com/problems/implement-strstr/
     * Implement strStr().
     * <p/>
     * Returns a pointer to the first occurrence of needle in haystack, or null if needle is not part of haystack.
     */
    public String strStr(String haystack, String needle) {
        if (haystack.equals(needle) || needle.length() == 0) {
            return haystack;
        }
        if (needle.length() == 0 || needle.length() > haystack.length()) {
            return null;
        }
        return kmp(haystack, needle);
    }

    private String kmp(String haystack, String needle) {
        int[] p = preprocess(needle);
        for (int i = 0, j = 0; i < haystack.length() && j < needle.length(); i += 1) {
            if (haystack.charAt(i) == needle.charAt(j)) {
                j += 1;
                if (j == needle.length()) {
                    return haystack.substring(i - j + 1);
                }
            } else {
                while (j > 0) {
                    j = p[j-1] + 1;
                    if (haystack.charAt(i) == needle.charAt(j)) {
                        j += 1;
                        if (j == needle.length()) {
                            return haystack.substring(i - j + 1);
                        }
                        break;
                    }
                }
            }
        }
        return null;
    }

    private int[] preprocess(String needle) {
        int[] p = new int[needle.length()];
        p[0] = -1;
        for (int i = 1; i < p.length; i++) {
            int tmp = p[i - 1];
            while (true) {
                if (tmp == -1) {
                    if (needle.charAt(0) == needle.charAt(i)) {
                        p[i] = 0;
                    } else {
                        p[i] = -1;
                    }
                    break;
                }
                if (needle.charAt(tmp + 1) == needle.charAt(i)) {
                    p[i] = tmp + 1;
                    break;
                } else {
                    tmp = p[tmp];
                }

            }
        }

        return p;
    }

    public String strStr2(String haystack, String needle) {
        return "";
    }

    private String kmp2(String haystack, String needle) {
        return "";
    }

    private int[] preprocess2(String needle) {
        return new int[0];
    }

    public static void main(String[] args) {
        StrStr s = new StrStr();
        System.out.println(s.strStr("", ""));
    }
}
