package org.soliloquize.leetcode;

public class StringToInteger {

    /**
     http://leetcode.com/onlinejudge#question_8

     Implement atoi to convert a string to an integer.

     Hint: Carefully consider all possible input cases. If you want a challenge,
     please do not see below and ask yourself what are the possible input cases.

     Notes: It is intended for this problem to be specified vaguely (ie, no given input specs).
     You are responsible to gather all the input requirements up front.
     */
    public int atoi(String str) {
        if (str == null) {
            return 0;
        }
        str = str.trim();
        if (str.length() == 0) {
            return 0;
        }
        if (str.startsWith("+")) {
            str = str.substring(1);
        }
        int index = str.length();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (i == 0 && (c == '+' || c == '-')) {
                continue;
            }
            if (c >= '0' && c <= '9') {
                continue;
            }
            index = i;
            break;
        }
        str = str.substring(0, index);
        try {
            long res = Long.valueOf(str);
            if (res > Integer.MAX_VALUE) {
                return Integer.MAX_VALUE;
            } else if (res < Integer.MIN_VALUE) {
                return Integer.MIN_VALUE;
            } else {
                return (int) res;
            }
        } catch (Exception e) {
            return 0;
        }
    }
}
