package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class Subsets {
    /**
     http://leetcode.com/oldoj#question_78

     Given a set of distinct integers, S, return all possible subsets.

     Note:

     Elements in a subset must be in non-descending order.
     The solution set must not contain duplicate subsets.
     For example,
     If S = [1,2,3], a solution is:

     [
     [3],
     [1],
     [2],
     [1,2,3],
     [1,3],
     [2,3],
     [1,2],
     []
     ]
     */

    public static void main(String[] args) {
        Subsets s = new Subsets();
        System.out.println(s.subsets(new int[] {1, 2, 3}));
        System.out.println(s.subsets2(new int[] {1, 2, 3}));
    }

    public ArrayList<ArrayList<Integer>> subsets2(int[] S) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < (int) Math.pow(2, S.length); i++) {
            ArrayList<Integer> current = new ArrayList<Integer>();
            for (int j = 0; j < S.length; j++) {
                if (((i >> j) & 1) == 1) {
                    current.add(S[j]);
                }
            }
            result.add(current);
        }
        return result;
    }


    public ArrayList<ArrayList<Integer>> subsets(int[] S) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(S);
        int max = pow(S.length);
        for (int i = 0; i < max; i++) {
            ArrayList<Integer> current = new ArrayList<Integer>();
            int tmp = i;
            for (int j = 0; j < S.length; j++) {
                if (1 == (tmp & 1)) {
                    current.add(S[j]);
                }
                tmp >>= 1;
            }
            result.add(current);
        }
        return result;
    }

    private int pow(int length) {
        int result = 1;
        for (int i = 0; i < length; i++) {
            result *= 2;
        }
        return result;
    }
}
