package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class SubsetsII {
    /**
     http://leetcode.com/oldoj#question_90

     Given a collection of integers that might contain duplicates, S, return all possible subsets.

     Note:


     •Elements in a subset must be in non-descending order.
     •The solution set must not contain duplicate subsets.

     For example,
     If S = [1,2,2], a solution is:

     [
     [2],
     [1],
     [1,2,2],
     [2,2],
     [1,2],
     []
     ]
     */

    public static void main(String[] args) {
        SubsetsII s = new SubsetsII();
        System.out.println(s.subsetsWithDup(new int[]{1, 2, 2}));
        System.out.println(s.subsetsWithDup2(new int[]{1, 2, 2}));
    }

    public ArrayList<ArrayList<Integer>> subsetsWithDup2(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(num);
        search(num, 0, new ArrayList<Integer>(), result);
        result.add(new ArrayList<Integer>());
        return result;
    }

    private void search(int[] num, int index, ArrayList<Integer> current, ArrayList<ArrayList<Integer>> result) {
        for (int i = index; i < num.length; i++) {
            current.add(num[i]);
            result.add(new ArrayList<Integer>(current));
            search(num, i + 1, current, result);
            current.remove(current.size()-1);
            while (i + 1 < num.length && num[i] == num[i+1]) {
                i += 1;
            }
        }
    }


    public ArrayList<ArrayList<Integer>> subsetsWithDup(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        Arrays.sort(num);
        int max = pow(num.length);
        for (int i = 0; i < max; i++) {
            ArrayList<Integer> current = new ArrayList<Integer>();
            int tmp = i;
            for (int j = 0; j < num.length; j++) {
                if (1 == (tmp & 1)) {
                    current.add(num[j]);
                }
                tmp >>= 1;
            }
            boolean flag = false;
            for (int j = result.size()-1; j >=0; j--) {
                if (result.get(j).equals(current)) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                result.add(current);
            }
        }
        return result;
    }

    private int pow(int length) {
        int result = 1;
        for (int i = 0; i < length; i++) {
            result *= 2;
        }
        return result;
    }
}
