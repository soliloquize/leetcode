package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubstringWithConcatenationOfAllWords {
    /**
     http://oj.leetcode.com/problems/substring-with-concatenation-of-all-words/

     You are given a string, S, and a list of words, L, that are all of the same length. Find all starting indices of substring(s) in S that is a concatenation of each word in L exactly once and without any intervening characters.

     For example, given:
     S: "barfoothefoobarman"
     L: ["foo", "bar"]

     You should return the indices: [0,9].
     (order does not matter).
     */
    public ArrayList<Integer> findSubstring(String S, String[] L) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        if (L.length == 0) {
            return result;
        }

        Map<String, Integer> wordMap = countWords(L);
        Map<Character, Integer> charMap = countChars(L);
        int wordLength = L[0].length();
        int length = wordLength * L.length;
        int i = 0;
        int j = length - 1;
        if (i >= S.length() || j >= S.length()) {
            return result;
        }

        Map<Character, Integer> currentCharMap = new HashMap<Character, Integer>();
        for (int k = i; k <= j; k++) {
            char c = S.charAt(k);
            if (!currentCharMap.containsKey(c)) {
                currentCharMap.put(c, 1);
            } else {
                currentCharMap.put(c, currentCharMap.get(c) + 1);
            }
        }

        while (j < S.length()) {
            // 首先找到所有字符匹配的子串
            while (!currentCharMap.equals(charMap) && (j < S.length() - 1)) {
                char start = S.charAt(i);
                i += 1;
                j += 1;
                char next = S.charAt(j);
                int nextVal = currentCharMap.get(start) - 1;
                if (nextVal == 0) {
                    currentCharMap.remove(start);
                } else {
                    currentCharMap.put(start, nextVal);
                }
                if (!currentCharMap.containsKey(next)) {
                    currentCharMap.put(next, 1);
                } else {
                    currentCharMap.put(next, currentCharMap.get(next) + 1);
                }
            }

            if (!currentCharMap.equals(charMap)) {
                break;
            }

            Map<String, Integer> currentWordMap = new HashMap<String, Integer>(wordMap);
            // 判断是否所有单词符合
            boolean flag = false;
            StringBuilder builder = new StringBuilder();
            for (int k = i; k <= j; k++) {
                builder.append(S.charAt(k));
                if (builder.length() == wordLength) {
                    String tmp = builder.toString();
                    if (!currentWordMap.containsKey(tmp) || (currentWordMap.get(tmp) <= 0)) {
                        flag = true;
                        break;
                    }
                    currentWordMap.put(tmp, currentWordMap.get(tmp) - 1);

                    builder.delete(0, builder.length());
                }
            }
            if (!flag) {
                result.add(i);
            }

            i += 1;
            j += 1;
        }

        return result;
    }

    private Map<Character, Integer> countChars(String[] L) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (String str : L) {
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                if (!map.containsKey(c)) {
                    map.put(c, 1);
                } else {
                    map.put(c, map.get(c) + 1);
                }
            }
        }
        return map;
    }

    private Map<String, Integer> countWords(String[] L) {
        Map<String, Integer> map = new HashMap<String, Integer>();
        for (String str : L) {
            if (!map.containsKey(str)) {
                map.put(str, 1);
            } else {
                map.put(str, map.get(str) + 1);
            }
        }
        return map;
    }

    public static void main(String[] args) {
        SubstringWithConcatenationOfAllWords s = new SubstringWithConcatenationOfAllWords();
//        ArrayList<Integer> res = s.findSubstring("lingmindraboofooowingdingbarrwingmonkeypoundcake", new String[] {"fooo","barr","wing","ding","wing"});
//        ArrayList<Integer> res = s.findSubstring("barfoothefoobarman", new String[] {"foo", "bar"});
        ArrayList<Integer> res = s.findSubstring("lingmindraboofooowingdingbarrwingmonkeypoundcake", new String[] {"fooo","barr","wing","ding","wing"});
        System.out.println(res);
    }
}
