package org.soliloquize.leetcode;

import java.util.*;

public class SudokuSolver {
    /**
     http://oj.leetcode.com/problems/sudoku-solver/

     Write a program to solve a Sudoku puzzle by filling the empty cells.

     Empty cells are indicated by the character '.'.

     You may assume that there will be only one unique solution.


     A sudoku puzzle...
     */


    Map<Pos, Set<Character>> map = new HashMap<Pos, Set<Character>>();

    class Pos implements Comparable<Pos> {
        int x;
        int y;

        public Pos(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Pos o) {
            return getLength(this) - getLength(o);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pos pos = (Pos) o;

            if (x != pos.x) return false;
            if (y != pos.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        public String toString() {
            return String.format("x: %d, y: %d", x, y);
        }
    }

    private int getLength(Pos pos) {
        return map.get(pos).size();
    }

    private List<Pos> initMap(char[][] board) {
        map.clear();
        List<Pos> posList = new LinkedList<Pos>();
        char[] chars = "123456789".toCharArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                Pos pos = new Pos(i, j);
                Set<Character> set = new HashSet<Character>();
                if (board[i][j] == '.') {
                    for (char c : chars) {
                        set.add(c);
                    }
                    map.put(pos, set);
                    posList.add(pos);
                } else {
                    set.add(board[i][j]);
                    map.put(pos, set);
                }
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.') {
                    continue;
                }

                for (int k = 0; k < board.length; k++) {
                    if (k == i) {
                        continue;
                    }
                    Pos p = new Pos(k, j);
                    map.get(p).remove(board[i][j]);
                }

                // 避免同一列出现相同元素
                for (int k = 0; k < board[i].length; k++) {
                    if (k == j) {
                        continue;
                    }
                    Pos p = new Pos(i, k);
                    map.get(p).remove(board[i][j]);
                }

                // 避免局部矩形区域内出现相同元素
                int m = i / 3 * 3;
                int n = j / 3 * 3;
                for (int p = m; p < m + 3; p++) {
                    for (int q = n; q < n + 3; q++) {
                        if (p == i && q == j) {
                            continue;
                        }
                        Pos x = new Pos(p, q);
                        map.get(x).remove(board[i][j]);
                    }
                }
            }
        }
        return posList;
    }

    private boolean found = false;

    public void solveSudoku(char[][] board) {
        //  每一个位置记录可能的数字，然后递归搜寻，初始化遍历整个棋盘，生成初始化的可能性，然后从可能性小的位置开始深搜，用堆记录位置节点
        found = false;

        List<Pos> posList = initMap(board);
        Collections.sort(posList);
        recursive(board, posList, 0);
    }

    private void recursive(char[][] board, List<Pos> posList, int depth) {
        if (posList.size() == 0) {
            found = true;
            return;
        }
        Pos pos = posList.get(0);
        posList.remove(0);
        Set<Character> set = map.get(pos);

        for (char c : set) {
            boolean flag = true;
            // 避免同一行出现相同元素
            for (int k = 0; k < board.length; k++) {
                if (k == pos.x) {
                    continue;
                }
                if (board[k][pos.y] == c) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                continue;
            }

            // 避免同一列出现相同元素
            for (int k = 0; k < board[pos.x].length; k++) {
                if (k == pos.y) {
                    continue;
                }
                if (board[pos.x][k] == c) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                continue;
            }

            // 避免局部矩形区域内出现相同元素
            int m = pos.x / 3 * 3;
            int n = pos.y / 3 * 3;
            for (int p = m; p < m + 3; p++) {
                for (int q = n; q < n + 3; q++) {
                    if (p == pos.x && q == pos.y) {
                        continue;
                    }

                    if (board[p][q] == c) {
                        flag = false;
                        break;
                    }
                }
            }
            if (!flag) {
                continue;
            }

            char old = board[pos.x][pos.y];
            board[pos.x][pos.y] = c;
            recursive(board, posList, depth + 1);
            if (found) {
                return;
            }
            board[pos.x][pos.y] = old;
        }
        posList.add(0, pos);
    }

    public static void main(String[] args) {
        SudokuSolver s = new SudokuSolver();
        char[][] board = new char[9][9];

        board[0] = "53..7....".toCharArray();
        board[1] = "6..195...".toCharArray();
        board[2] = ".98....6.".toCharArray();
        board[3] = "8...6...3".toCharArray();
        board[4] = "4..8.3..1".toCharArray();
        board[5] = "7...2...6".toCharArray();
        board[6] = ".6....28.".toCharArray();
        board[7] = "...419..5".toCharArray();
        board[8] = "....8..79".toCharArray();

        s.solveSudoku(board);
        for (char[] c1 : board) {
            System.out.println(new String(c1));
        }
    }
}
