package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class SumRootToLeafNumbers {
    /**
     http://leetcode.com/onlinejudge#question_129

     Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.

     An example is the root-to-leaf path 1->2->3 which represents the number 123.

     Find the total sum of all root-to-leaf numbers.

     For example,

     1
     / \
     2   3
     The root-to-leaf path 1->2 represents the number 12.
     The root-to-leaf path 1->3 represents the number 13.

     Return the sum = 12 + 13 = 25.
     */

    public static void main(String[] args) {
        SumRootToLeafNumbers s = new SumRootToLeafNumbers();
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        System.out.println(s.sumNumbers(root));
        System.out.println(s.sumNumbers2(root));
    }

    public int sumNumbers2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        sum = 0;
        search(root, 0);
        return sum;
    }

    private void search(TreeNode node, int current) {
        int next = 10 * current + node.val;
        if (node.left == null && node.right == null) {
            sum += next;
            return;
        }
        if (node.left != null) {
            search(node.left, next);
        }
        if (node.right != null) {
            search(node.right, next);
        }
    }

    private int sum = 0;

    public int sumNumbers(TreeNode root) {
        sum = 0;
        if (root == null) {
            return sum;
        }
        travel(root, 0);
        return sum;
    }

    private void travel(TreeNode root, int current) {
        current = current * 10 + root.val;
        if (root.left == null && root.right == null) {
            sum += current;
        } else {
            if (root.left != null) {
                travel(root.left, current);
            }
            if (root.right != null) {
                travel(root.right, current);
            }
        }
    }
}
