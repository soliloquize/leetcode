package org.soliloquize.leetcode;

import java.util.ArrayDeque;
import java.util.Deque;

public class SurroundedRegions {

    /**
     http://leetcode.com/onlinejudge#question_130

     Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.

     A region is captured by flipping all 'O's into 'X's in that surrounded region .

     For example,

     X X X X
     X O O X
     X X O X
     X O X X

     After running your function, the board should be:

     X X X X
     X X X X
     X X X X
     X O X X
     */

    public static void main(String[] args) {
        char[][] board = new char[][] {
            "XXXX".toCharArray(),
            "XOOX".toCharArray(),
            "XXOX".toCharArray(),
            "XOXX".toCharArray(),
        };

        SurroundedRegions s = new SurroundedRegions();
        s.solve2(board);
        for (int i = 0; i < board.length; i++) {
            System.out.println(new String(board[i]));
        }
    }

    public void solve2(char[][] board) {
        if (board.length == 0 || board[0].length == 0) {
            return;
        }
        int m = board.length;
        int n = board[0].length;
        for (int i = 0; i < m; i++) {
            process(board, i, 0, m, n);
            process(board, i, n - 1, m, n);
        }
        for (int j = 0; j < n; j++) {
            process(board, 0, j, m, n);
            process(board, n - 1, j, m, n);
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                } else if (board[i][j] == 'Y') {
                    board[i][j] = 'O';
                }
            }
        }
    }

    private void process(char[][] board, int i, int j, int m, int n) {
        if (board[i][j] == 'O') {
            search(board, i, j, m, n);
        }
    }

    private void search(char[][] board, int x, int y, int m, int n) {
        if ((x < 0 || x >= m) || (y < 0 || y >= n) || board[x][y] != 'O') {
            return;
        }
        board[x][y] = 'Y';
        search(board, x-1, y, m, n);
        search(board, x+1, y, m, n);
        search(board, x, y-1, m, n);
        search(board, x, y+1, m, n);
    }


    public void solve(char[][] board) {
        if (board.length == 0) {
            return;
        }
        int m = board.length;
        int n = board[0].length;
        if (m <= 1 || n <= 1) {
            return;
        }
        markNonSurroundedRegions(board, m, n);
        markSurroundedRegions(board, m, n);

    }

    private void markSurroundedRegions(char[][] board, int m, int n) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (board[i][j] == 'Y') {
                    board[i][j] = 'O';
                } else if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }
    }

    private void markNonSurroundedRegions(char[][] board, int m, int n) {
        Deque<Pair<Integer, Integer>> deque = findStartPositions(board, m, n);
        while (deque.size() > 0) {
            Pair<Integer, Integer> pos = deque.pollFirst();
            board[pos.fst][pos.snd] = 'Y';
            if (pos.fst + 1 < m && board[pos.fst + 1][pos.snd] == 'O') {
                deque.add(new Pair<Integer, Integer>(pos.fst + 1, pos.snd));
            }
            if (pos.fst - 1 >= 0 && board[pos.fst - 1][pos.snd] == 'O') {
                deque.add(new Pair<Integer, Integer>(pos.fst - 1, pos.snd));
            }
            if (pos.snd + 1 < n && board[pos.fst][pos.snd + 1] == 'O') {
                deque.add(new Pair<Integer, Integer>(pos.fst, pos.snd + 1));
            }
            if (pos.snd - 1 >= 0 && board[pos.fst][pos.snd - 1] == 'O') {
                deque.add(new Pair<Integer, Integer>(pos.fst, pos.snd -1));
            }
        }
    }

    private Deque<Pair<Integer, Integer>> findStartPositions(char[][] board, int m, int n) {
        Deque<Pair<Integer, Integer>> deque = new ArrayDeque<Pair<Integer,Integer>>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (isStartPosition(m, n, i, j, board[i][j])) {
                    deque.add(new Pair<Integer, Integer>(i, j));
                }
            }
        }
        return deque;
    }

    private boolean isStartPosition(int m, int n, int x, int y, char c) {
        if (c != 'O') {
            return false;
        }
        if (x == m - 1 || x == 0) {
            return true;
        }
        if (y == n - 1 || y == 0) {
            return true;
        }
        return false;
    }

    public class Pair <A, B>  {
        public final A fst;
        public final B snd;
        public Pair(A a, B b) {
            fst = a;
            snd = b;
        }
    }
}
