package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.ListNode;

public class SwapNodesInPairs {
    /**
     http://leetcode.com/onlinejudge#question_24

     Given a linked list, swap every two adjacent nodes and return its head.

     For example,
     Given 1->2->3->4, you should return the list as 2->1->4->3.

     Your algorithm should use only constant space. You may not modify the values in the list, only nodes itself can be changed.
     */

    public ListNode swapPairs(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode p = head;
        ListNode q = p.next;
        ListNode z = null;
        ListNode r = null;
        while (p != null && q != null) {
            ListNode tmp = q.next;
            if (z != null) {
                z.next = q;
            }
            if (r == null) {
                r = q;
            }
            q.next = p;
            z = p;
            p.next = tmp;
            p = p.next;
            if (p == null || p.next == null) {
                break;
            }
            q = p.next;
        }
        r = (r == null) ? head : r;
        return r;
    }
}
