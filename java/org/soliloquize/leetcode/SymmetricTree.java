package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

public class SymmetricTree {
    /**
     http://leetcode.com/onlinejudge#question_101

     Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

     For example, this binary tree is symmetric:

     1
     / \
     2   2
     / \ / \
     3  4 4  3
     But the following is not:

     1
     / \
     2   2
     \   \
     3    3
     Note:
     Bonus points if you could solve it both recursively and iteratively.

     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
     */

    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return isSymmetric(root.left, root.right);
    }

    public boolean isSymmetric(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        if (p.val != q.val) {
            return false;
        }
        return isSymmetric(p.left, q.right) && isSymmetric(p.right, q.left);
    }


    public static void main(String[] args) {
        TreeNode root1 = new TreeNode(1);
        root1.left = new TreeNode(2);
        root1.left.left = new TreeNode(3);
        root1.left.right = new TreeNode(4);
        root1.right = new TreeNode(2);
        root1.right.left = new TreeNode(4);
        root1.right.right = new TreeNode(3);

        TreeNode root2 = new TreeNode(1);
        root2.left = new TreeNode(2);
        root2.left.right = new TreeNode(3);
        root2.right = new TreeNode(2);
        root2.right.right = new TreeNode(3);

        SymmetricTree s = new SymmetricTree();
        System.out.println(s.isSymmetric(root1));
        System.out.println(s.isSymmetric(root2));
        System.out.println("->");
        System.out.println(s.isSymmetric2(root1));
        System.out.println(s.isSymmetric2(root2));
    }

    public boolean isSymmetric2(TreeNode root) {
        if (root == null) {
            return false;
        }
        return isSymmetric2(root.left, root.right);
    }

    private boolean isSymmetric2(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        if (p.val != q.val) {
            return false;
        }
        return isSymmetric2(p.left, q.right) && isSymmetric2(p.right, q.left);
    }


    public static class Record {
        int depth;
        TreeNode node;
        public Record(TreeNode node, int depth) {
            this.depth = depth;
            this.node = node;
        }
    }

    public boolean isSymmetric_(TreeNode root) {
        if (root == null) {
            return true;
        }
        List<Record> records = new ArrayList<Record>();
        travel(root, 0, records);

        for (int i = 0, j = records.size() - 1; i < j; i++, j--) {
            Record a = records.get(i);
            Record b = records.get(j);
            if (!(a.depth == b.depth && a.node.val == b.node.val)) {
                return false;
            }
        }

        return true;
    }

    private void travel(TreeNode root, int depth, List<Record> records) {
        if (root.left != null) {
            travel(root.left, depth + 1, records);
        }
        records.add(new Record(root, depth));
        if (root.right != null) {
            travel(root.right, depth + 1, records);
        }
    }
}
