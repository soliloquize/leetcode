package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.List;

public class TextJustification {
    /**
     http://leetcode.com/oldoj#question_68

     Given an array of words and a length L, format the text such that each line has exactly L characters and is fully (left and right) justified.

     You should pack your words in a greedy approach; that is, pack as many words as you can in each line. Pad extra spaces ' ' when necessary so that each line has exactly L characters.

     Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line do not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.

     For the last line of text, it should be left justified and no extra space is inserted between words.

     For example,
     words: ["This", "is", "an", "example", "of", "text", "justification."]
     L: 16.

     Return the formatted lines as:


     [
     "This    is    an",
     "example  of text",
     "justification.  "
     ]

     Note: Each word is guaranteed not to exceed L in length.

     Corner Cases:

     •A line other than the last line might contain only one word. What should you do in this case?
     In this case, that line should be left-justified.
     */
    public ArrayList<String> fullJustify(String[] words, int L) {
        ArrayList<String> result = new ArrayList<String>();

        List<String> current = new ArrayList<String>();
        for (int i = 0; i < words.length; i++) {
            current.add(words[i]);
            // 填满一行
            if (!isLineValid(current, L)) {
                current.remove(current.size()-1);
                result.add(buildLine(current, L));
                current.clear();
                current.add(words[i]);
            }
        }
        if (current.size() > 0) {
            result.add(buildLine(current, L));
        }

        if (result.size() > 0) {
            result.set(result.size()-1, adjustLastLine(result.get(result.size() - 1), L));
        }
        return result;
    }

    private String adjustLastLine(String line, int L) {
        String[] tmp = line.split(" ");
        if (tmp.length == 0) {
            return line;
        }
        StringBuilder builder = new StringBuilder(tmp[0]);
        for (int i = 1; i < tmp.length; i++) {
            if (tmp[i].length() > 0) {
                builder.append(' ').append(tmp[i]);
            }
        }
        int length = builder.length();
        for (int i = 0; i < L - length; i++) {
            builder.append(' ');
        }
        return builder.toString();
    }

    private String buildLine(List<String> current, int L) {
        StringBuilder builder = new StringBuilder();
        builder.append(current.get(0));

        if (current.size() == 1) {
            return appendBlanks(builder, L - current.get(0).length()).toString();
        }

        int total = 0;
        for (String str : current) {
            total += str.length();
        }
        int blanks = L - total;
        int average = blanks / (current.size() - 1);
        int remain = blanks % (current.size() - 1);

        for (int i = 1; i < current.size(); i++) {
            if (remain > 0) {
                builder = appendBlanks(builder, average + 1);
                remain -= 1;
            } else {
                builder = appendBlanks(builder, average);
            }

            builder.append(current.get(i));
        }
        return builder.toString();
    }

    private StringBuilder appendBlanks(StringBuilder builder, int num) {
        for (int i = 0; i < num; i++) {
            builder.append(' ');
        }
        return builder;
    }

    private boolean isLineValid(List<String> current, int L) {
        int total = current.size() - 1;
        for (String str : current) {
            total += str.length();
        }
        return total <= L;
    }

    public static void main(String[] args) {
        TextJustification s = new TextJustification();
        ArrayList<String> res = null;
        res = s.fullJustify(new String[] {"Don't","go","around","saying","the","world","owes","you","a","living;","the","world","owes","you","nothing;","it","was","here","first."}, 30);
        System.out.println(res);
    }
}
