package org.soliloquize.leetcode;

public class TrappingRainWater {
    /**
     http://oj.leetcode.com/problems/trapping-rain-water/
     Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.

     For example,
     Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
     */
    public int trap2(int[] A) {
        if (A.length == 0) {
            return 0;
        }
        int result = 0;
        int[] high = new int[A.length];
        int[] index = new int[A.length];
        int max = Integer.MIN_VALUE;
        int pos = -1;
        for (int i = A.length - 1; i >= 0; i--) {
            if (max < A[i]) {
                max = A[i];
                pos = i;
            }
            high[i] = max;
            index[i] = pos;
        }
        for (int i = 0; i < A.length - 1; ) {
            if (A[i] > high[i+1]) {
                pos = index[i+1];
            } else {
                pos = i + 1;
                for (int j = i + 1; j < A.length; j++) {
                    if (A[j] >= A[i]) {
                        pos = j;
                        break;
                    }
                }
            }
            int height = Math.min(A[i], A[pos]);
            for (int k = i + 1; k < pos; k++) {
                result += height - A[k];
            }

            i = pos;
        }
        return result;
    }


    public int trap(int[] A) {
        int result = 0;
        for (int i = 0; i < A.length; ) {
            int max = 0;
            int position = i;
            for (int j = i+1; j < A.length; j++) {
                if (A[j] < A[i]) {
                    if (max < A[j]) {
                        max = A[j];
                        position = j;
                    }
                } else if (A[j] >= A[i]) {
                    position = j;
                    break;
                }
            }
            int larger = Math.min(A[i], A[position]);
            for (int k = i + 1; k <= position; k++) {
                result += Math.max(0, larger - A[k]);
            }
            if (position == i) {
                i += 1;
            } else {
                i = position;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        TrappingRainWater s = new TrappingRainWater();
        System.out.println(s.trap(new int[] {4, 2, 3}));
        System.out.println(s.trap(new int[] {0,1,0,2,1,0,1,3,2,1,2,1}));
        System.out.println(s.trap2(new int[] {4, 2, 3}));
        System.out.println(s.trap2(new int[] {0,1,0,2,1,0,1,3,2,1,2,1}));
    }
}
