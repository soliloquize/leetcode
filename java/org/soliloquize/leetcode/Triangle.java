package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class Triangle {

    /**
     http://leetcode.com/onlinejudge#question_120

     Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

     For example, given the following triangle

     [
     [2],
     [3,4],
     [6,5,7],
     [4,1,8,3]
     ]
     The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

     Note:
     Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
     */

    public static void main(String[] args) {
        Triangle s = new Triangle();
        ArrayList<ArrayList<Integer>> triangle = new ArrayList<ArrayList<Integer>>();
        triangle.add(new ArrayList<Integer>(Arrays.asList(2)));
        triangle.add(new ArrayList<Integer>(Arrays.asList(3, 4)));
        triangle.add(new ArrayList<Integer>(Arrays.asList(6, 5, 7)));
        triangle.add(new ArrayList<Integer>(Arrays.asList(4, 1, 8, 3)));
        System.out.println(s.minimumTotal(triangle));
        System.out.println(s.minimumTotal2(triangle));
    }

    public int minimumTotal2(ArrayList<ArrayList<Integer>> triangle) {
        int[][] p = new int[triangle.size()][triangle.size()];
        p[0][0] = triangle.get(0).get(0);
        for (int i = 1; i < triangle.size(); i++) {
            for (int j = 0; j <= i; j++) {
                p[i][j] =  triangle.get(i).get(j);
                if (j == 0) {
                    p[i][j] += p[i-1][j];
                } else if (j == i) {
                    p[i][j] += p[i-1][j-1];
                } else {
                    p[i][j] += Math.min(p[i-1][j-1], p[i-1][j]);
                }
            }
        }
        int res = Integer.MAX_VALUE;
        for (int j = 0; j < triangle.size(); j++) {
            res = Math.min(res, p[triangle.size()-1][j]);
        }
        return res;
    }

    private int min;

    public int minimumTotal(ArrayList<ArrayList<Integer>> triangle) {
        if (triangle == null || triangle.size() == 0) {
            return 0;
        }
        min = Integer.MAX_VALUE;
        int[][] matrix = new int[triangle.size()][triangle.size()];
        for (int i = 0; i < matrix.length; i++) {
            Arrays.fill(matrix[i], Integer.MAX_VALUE);
        }
        travel(triangle, 0, 0, 0, matrix);
        return min;
    }

    private void travel(ArrayList<ArrayList<Integer>> triangle, int x, int y, int sum, int[][] matrix) {
        if (x >= triangle.size() - 1) {
            sum += triangle.get(x).get(y);
            min = min > sum ? sum : min;
            return;
        }
        sum += triangle.get(x).get(y);
        if (matrix[x][y] <= sum) {
            return;
        }
        matrix[x][y] = sum;
        // 优先选择更小的元素进行递归，便于剪枝
        if (triangle.get(x+1).get(y) > triangle.get(x + 1).get(y + 1)) {
            travel(triangle, x + 1, y + 1, sum, matrix);
            travel(triangle, x + 1, y, sum, matrix);
        } else {
            travel(triangle, x + 1, y, sum, matrix);
            travel(triangle, x + 1, y + 1, sum, matrix);
        }
    }
}
