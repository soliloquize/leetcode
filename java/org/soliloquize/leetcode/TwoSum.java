package org.soliloquize.leetcode;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {

    /**
     http://leetcode.com/onlinejudge#question_1

     Given an array of integers, find two numbers such that they add up to a specific target number.

     The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.

     You may assume that each input would have exactly one solution.

     Input: numbers={2, 7, 11, 15}, target=9
     Output: index1=1, index2=2
     */

    public int[] twoSum(int[] numbers, int target) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < numbers.length; i++) {
            map.put(numbers[i], i);
        }
        for (int p = 0; p < numbers.length; p++) {
            int tmp = target - numbers[p];
            if (map.containsKey(tmp)) {
                int q = map.get(tmp);
                if (p != q) {
                    return new int[] {p + 1, q + 1};
                }
            }
        }
        return new int[] {-1, -1};
    }

    public static void main(String[] args) {
        TwoSum s = new TwoSum();
        int[] res = s.twoSum(new int[] {3, 2, 4}, 6);
        System.out.println(res[0]);
        System.out.println(res[1]);
    }
}
