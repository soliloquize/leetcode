package org.soliloquize.leetcode;

public class UniqueBinarySearchTrees {
    /**
     http://leetcode.com/oldoj#question_96

     Given n, how many structurally unique BST's (binary search trees) that store values 1...n?

     For example,
     Given n = 3, there are a total of 5 unique BST's.

     1         3     3      2      1
     \       /     /      / \      \
     3     2     1      1   3      2
     /     /       \                 \
     2     1         2                 3
     */

    public static void main(String[] args) {
        UniqueBinarySearchTrees s = new UniqueBinarySearchTrees();
        System.out.println(s.numTrees(3));
        System.out.println(s.numTrees2(3));
    }

    public int numTrees2(int n) {
        if (n == 0) {
            return 0;
        }

        int[] p = new int[n+1];
        p[0] = 1;
        p[1] = 1;

        for (int i = 2; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                p[i] += p[j] * p[i-j-1];
            }
        }
        return p[n];
    }

    public int numTrees(int n) {
        /**
         * 根元素分别选取1..n，将各种情况下数目相加
         * p[n] = p[n-1] + p[1]*p[n-2] + p[2]*p[n-3] + ... p[n-2]*p[1] + p[n-1]
         */
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        int [] p = new int[n+1];
        p[0] = 1;
        p[1] = 1;
        for (int i = 2; i <= n; i++) {
            p[i] = 0;
            for (int j = 0; j < i; j++) {
                p[i] += p[j]*p[i - 1 -j];
            }
        }
        return p[n];
    }
}
