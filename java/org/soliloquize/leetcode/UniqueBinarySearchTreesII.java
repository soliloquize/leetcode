package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

import java.util.ArrayList;

public class UniqueBinarySearchTreesII {
    /**
     http://leetcode.com/oldoj#question_95

     Given n, generate all structurally unique BST's (binary search trees) that store values 1...n.

     For example,
     Given n = 3, your program should return all 5 unique BST's shown below.

     1         3     3      2      1
     \       /     /      / \      \
     3     2     1      1   3      2
     /     /       \                 \
     2     1         2                 3
     */
    public ArrayList<TreeNode> generateTrees(int n) {
        int[] num = new int[n+1];
        for (int i = 0; i < num.length; i++) {
            num[i] = i;
        }
        ArrayList<TreeNode> result = generate(num, 1, num.length);
        return result;
    }

    private ArrayList<TreeNode> generate(int[] num, int start, int end) {
        ArrayList<TreeNode> result = new ArrayList<TreeNode>();
        if (end == start) {
            result.add(null);
            return result;
        }
        if (end - start <= 1) {
            result.add(new TreeNode(num[start]));
            return result;
        }
        for (int i = start; i < end; i++) {
            if (i == start) {
                ArrayList<TreeNode> tmp = generate(num, i + 1, end);
                for (int j = 0; j < tmp.size(); j++) {
                    TreeNode root = new TreeNode(num[i]);
                    root.right = tmp.get(j);
                    result.add(root);
                }
            } else if (i == end - 1) {
                ArrayList<TreeNode> tmp = generate(num, start, i);
                for (int j = 0; j < tmp.size(); j++) {
                    TreeNode root = new TreeNode(num[i]);
                    root.left = tmp.get(j);
                    result.add(root);
                }
            } else {
                ArrayList<TreeNode> tmp1 =  generate(num, start, i);
                ArrayList<TreeNode> tmp2 = generate(num, i + 1, end);
                for (int x = 0; x < tmp1.size(); x++) {
                    for (int y = 0; y < tmp2.size(); y++) {
                        TreeNode root = new TreeNode(num[i]);
                        root.left = tmp1.get(x);
                        root.right = tmp2.get(y);
                        result.add(root);
                    }
                }
            }
        }
        return result;
    }

    public ArrayList<TreeNode> generateTrees2(int n) {
        int[] num = new int[n];
        for (int i = 0; i < n; i++) {
            num[i] = i + 1;
        }
        return search(num, 0, num.length);
    }

    private ArrayList<TreeNode> search(int[] num, int start, int end) {
        ArrayList<TreeNode> result = new ArrayList<TreeNode>();
        if (end - start <= 1) {
            result.add(new TreeNode(num[start]));
            return result;
        }
        for (int i = start; i < end; i++) {
            if (i == start) {
                ArrayList<TreeNode> tmp = search(num, i + 1, end);
                for (TreeNode right : tmp) {
                    TreeNode node = new TreeNode(num[i]);
                    node.right = right;
                    result.add(node);
                }
            } else if (i == end - 1) {
                ArrayList<TreeNode> tmp = search(num, start, end - 1);
                for (TreeNode left : tmp) {
                    TreeNode node = new TreeNode(num[i]);
                    node.left = left;
                    result.add(node);
                }
            } else {
                ArrayList<TreeNode> tmp1 = search(num, start, i);
                ArrayList<TreeNode> tmp2 = search(num, i + 1, end);
                for (TreeNode left : tmp1) {
                    for (TreeNode right : tmp2) {
                        TreeNode node = new TreeNode(num[i]);
                        node.left = left;
                        node.right = right;
                        result.add(node);
                    }
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        UniqueBinarySearchTreesII s = new UniqueBinarySearchTreesII();
        BinaryTreeLevelOrderTraversal ls = new BinaryTreeLevelOrderTraversal();
        for (TreeNode node : s.generateTrees(3)) {
            System.out.println(ls.levelOrder(node));
        }
        System.out.println("->");
        for (TreeNode node : s.generateTrees2(3)) {
            System.out.println(ls.levelOrder(node));
        }
    }
}
