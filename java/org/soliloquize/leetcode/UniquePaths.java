package org.soliloquize.leetcode;

import java.util.Arrays;

public class UniquePaths {
    /**
     http://leetcode.com/oldoj#question_62

     A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).

     The robot can only move either down or right at any point in time. The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).

     How many possible unique paths are there?

     Note: m and n will be at most 100.
     */
    public int uniquePaths(int m, int n) {
        // f[m][n] = f[m-1][n] + f[m][n-1]
        int [][] f = new int[m][n];
        for (int i = 0; i < m; i++) {
            Arrays.fill(f[i], 0);
        }
        f[0][0] = 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    continue;
                } else if (i == 0) {
                    f[i][j] = f[i][j-1];
                } else if (j == 0) {
                    f[i][j] = f[i-1][j];
                } else {
                    f[i][j] = f[i][j-1] + f[i-1][j];
                }
            }
        }
        return f[m-1][n-1];
    }

    public int uniquePaths2(int m, int n) {
        int[][] p = new int[m][n];
        p[0][0] = 1;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                if (i == 0) {
                    p[i][j] = p[i][j-1];
                    continue;
                }
                if (j == 0) {
                    p[i][j] = p[i-1][j];
                    continue;
                }
                p[i][j] = p[i-1][j] + p[i][j-1];
            }
        }
        return p[m-1][n-1];
    }
}
