package org.soliloquize.leetcode;

import java.util.Arrays;

public class UniquePathsII {
    /**
     http://leetcode.com/oldoj#question_62

     Follow up for "Unique Paths":

     Now consider if some obstacles are added to the grids. How many unique paths would there be?

     An obstacle and empty space is marked as 1 and 0 respectively in the grid.

     For example,


     There is one obstacle in the middle of a 3x3 grid as illustrated below.

     [
     [0,0,0],
     [0,1,0],
     [0,0,0]
     ]
     The total number of unique paths is 2.

     Note: m and n will be at most 100.
     */
    public int uniquePathsWithObstacles2(int[][] obstacleGrid) {
        if (obstacleGrid.length == 0 || obstacleGrid[0].length == 0 || obstacleGrid[0][0] == 1) {
            return 0;
        }
        int [][] p = new int[obstacleGrid.length][obstacleGrid[0].length];
        p[0][0] = 1;
        for (int i = 0; i < p.length; i++) {
            for (int j = 0; j < p[i].length; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                if (obstacleGrid[i][j] == 1) {
                    p[i][j] = 0;
                    continue;
                }
                if (i == 0) {
                    p[i][j] = p[i][j-1];
                    continue;
                }
                if (j == 0) {
                    p[i][j] = p[i-1][j];
                    continue;
                }
                p[i][j] = p[i-1][j] + p[i][j-1];
            }
        }
        return p[p.length-1][p[0].length-1];
    }


    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        // f[x][y] = f[x-1][y] + f[x][y-1], if o[x][y] == 0
        if (obstacleGrid.length == 0) {
            return 0;
        }

        if (obstacleGrid[0][0] == 1) {
            return 0;
        }

        int[][] f = new int[obstacleGrid.length][obstacleGrid[0].length];
        for (int i = 0; i < f.length; i++) {
            Arrays.fill(f[i], 0);
        }
        f[0][0] = 1;
        for (int i = 0; i < f.length; i++) {
            for (int j = 0; j < f[i].length; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }
                if (obstacleGrid[i][j] == 1) {
                    continue;
                }
                if (i == 0) {
                    f[i][j] = f[i][j-1];
                } else if (j == 0) {
                    f[i][j] = f[i-1][j];
                } else {
                    f[i][j] = f[i][j-1] + f[i-1][j];
                }
            }
        }

        return f[obstacleGrid.length-1][obstacleGrid[0].length-1];
    }
}
