package org.soliloquize.leetcode;

public class ValidNumber {
    /**
     http://leetcode.com/oldoj#question_65

     Validate if a given string is numeric.

     Some examples:
     "0" => true
     " 0.1 " => true
     "abc" => false
     "1 a" => false
     "2e10" => true


     Note: It is intended for the problem statement to be ambiguous. You should gather all requirements up front before implementing one.
     */
    public boolean isNumber(String s) {
        s = s.toLowerCase().trim();
        s = s.replace("e+", "e");
        s = s.replace("e-", "e");
        if (s.length() > 0) {
            if (s.charAt(0) == '+' || s.charAt(0) == '-') {
                s = s.substring(1);
            }
        }
        if (s.length() == 0) {
            return false;
        }
        return isInteger(s) || isFloat(s);
    }

    private boolean isFloat(String s) {
        int dotNum = 0;
        int eNum = 0;
        int num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                if (s.charAt(i) == '.' && dotNum == 0 && eNum == 0) {
                    dotNum += 1;
                    continue;
                } else if (s.charAt(i) == 'e' && eNum == 0 && num > 0){
                    eNum += 1;
                    continue;
                } else {
                    return false;
                }
            } else {
                num += 1;
            }
        }
        return num > 0 && !s.endsWith("e");
    }

    private boolean isInteger(String s) {
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isDigit(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
