package org.soliloquize.leetcode;

public class ValidPalindrome {
    /**
     http://leetcode.com/onlinejudge#question_125

     Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

     For example,
     "A man, a plan, a canal: Panama" is a palindrome.
     "race a car" is not a palindrome.

     Note:
     Have you consider that the string might be empty? This is a good question to ask during an interview.

     For the purpose of this problem, we define empty string as valid palindrome.
     */

    public boolean isPalindrome(String s) {
        for (int i = 0, j = s.length() - 1; i < j; ) {
            while (i < s.length() && !Character.isLetterOrDigit(s.charAt(i))) {
                i += 1;
            }
            while (j >= 0 && !Character.isLetterOrDigit(s.charAt(j))) {
                j -= 1;
            }
            if (i < j && Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j))) {
                return false;
            }
            i += 1;
            j -= 1;
        }
        return true;
    }

    public static void main(String [] args) throws Exception {
        ValidPalindrome solution = new ValidPalindrome();
        System.out.println(solution.isPalindrome(""));
    }
}
