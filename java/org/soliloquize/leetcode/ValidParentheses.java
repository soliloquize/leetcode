package org.soliloquize.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class ValidParentheses {

    /**
     http://leetcode.com/onlinejudge#question_20

     Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

     The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
     */

    public static void main(String[] args) {
        ValidParentheses s = new ValidParentheses();
        System.out.println(s.isValid("()"));
        System.out.println(s.isValid("()[]{}"));
        System.out.println(s.isValid("(]"));
        System.out.println(s.isValid("([)]"));
        System.out.println("->");

        System.out.println(s.isValid2("()"));
        System.out.println(s.isValid2("()[]{}"));
        System.out.println(s.isValid2("(]"));
        System.out.println(s.isValid2("([)]"));
    }

    public boolean isValid2(String s) {
        Deque<Character> stack = new ArrayDeque<Character>();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if (stack.isEmpty()) {
                    return false;
                }
                char top = stack.pop();
                if (c == ')' && top != '(') {
                    return false;
                }
                if (c == ']' && top != '[') {
                    return false;
                }
                if (c == '}' && top != '{') {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public boolean isValid(String s) {
        List<Character> stack = new ArrayList<Character>();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '(' || c == '[' || c == '{') {
                stack.add(c);
            } else {
                if (stack.size() == 0) {
                    return false;
                }
                char last = stack.get(stack.size() - 1);
                if (c == ')' && last != '(') {
                    return false;
                } else if (c == ']' && last != '[') {
                    return false;
                } else if (c == '}' && last != '{') {
                    return false;
                }
                stack.remove(stack.size() - 1);
            }
        }

        return stack.size() == 0;
    }
}
