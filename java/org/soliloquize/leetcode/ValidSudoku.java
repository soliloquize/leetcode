package org.soliloquize.leetcode;

import java.util.*;

public class ValidSudoku {
    /**
     http://oj.leetcode.com/problems/valid-sudoku/

     Determine if a Sudoku is valid, according to: Sudoku Puzzles - The Rules.

     The Sudoku board could be partially filled, where empty cells are filled with the character '.'.

     A partially filled sudoku which is valid.
     */

    public boolean isValidSudoku(char[][] board) {
        return quickValidate(board);
    }

    Map<Pos, Set<Character>> map = new HashMap<Pos, Set<Character>>();

    class Pos implements Comparable<Pos> {
        int x;
        int y;

        public Pos(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int compareTo(Pos o) {
            return getLength(this) - getLength(o);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Pos pos = (Pos) o;

            if (x != pos.x) return false;
            if (y != pos.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        public String toString() {
            return String.format("x: %d, y: %d", x, y);
        }
    }

    private int getLength(Pos pos) {
        return map.get(pos).size();
    }

    private boolean quickValidate(char[][] board) {
        map.clear();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                char c = board[i][j];
                if (c == '.') {
                    continue;
                }

                for (int k = 0; k < board.length; k++) {
                    if (k == i) {
                        continue;
                    }
                    if (c == board[k][j]) {
                        return false;
                    }
                }

                for (int k = 0; k < board[i].length; k++) {
                    if (k == j) {
                        continue;
                    }
                    if (c == board[i][k]) {
                        return false;
                    }
                }

                int m = i / 3 * 3;
                int n = j / 3 * 3;
                for (int p = m; p < m + 3; p++) {
                    for (int q = n; q < n + 3; q++) {
                        if (p == i && q == j) {
                            continue;
                        }
                        if (c == board[p][q]) {
                            return false;
                        }
                    }
                }
            }
        }

        List<Pos> posList = new LinkedList<Pos>();
        char[] chars = "123456789".toCharArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                Pos pos = new Pos(i, j);
                Set<Character> set = new HashSet<Character>();
                if (board[i][j] == '.') {
                    for (char c : chars) {
                        set.add(c);
                    }
                    map.put(pos, set);
                    posList.add(pos);
                } else {
                    set.add(board[i][j]);
                    map.put(pos, set);
                }
            }
        }

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == '.') {
                    continue;
                }

                for (int k = 0; k < board.length; k++) {
                    if (k == i) {
                        continue;
                    }
                    Pos p = new Pos(k, j);
                    map.get(p).remove(board[i][j]);
                }

                // 避免同一列出现相同元素
                for (int k = 0; k < board[i].length; k++) {
                    if (k == j) {
                        continue;
                    }
                    Pos p = new Pos(i, k);
                    map.get(p).remove(board[i][j]);
                }

                // 避免局部矩形区域内出现相同元素
                int m = i / 3 * 3;
                int n = j / 3 * 3;
                for (int p = m; p < m + 3; p++) {
                    for (int q = n; q < n + 3; q++) {
                        if (p == i && q == j) {
                            continue;
                        }
                        Pos x = new Pos(p, q);
                        map.get(x).remove(board[i][j]);
                    }
                }
            }
        }

        for (Map.Entry<Pos, Set<Character>> entry : map.entrySet()) {
            if (entry.getValue().size() == 0) {
                return false;
            }
        }

        return true;
    }


    public static void main(String[] args) {
        ValidSudoku s = new ValidSudoku();
        char[][] board = new char[9][9];

        board[0] = ".........".toCharArray();
        board[1] = "..2......".toCharArray();
        board[2] = ".....271.".toCharArray();
        board[3] = ".........".toCharArray();
        board[4] = ".2.......".toCharArray();
        board[5] = ".5.......".toCharArray();
        board[6] = "....9...8".toCharArray();
        board[7] = ".....16..".toCharArray();
        board[8] = "....6....".toCharArray();

        System.out.println(s.isValidSudoku(board));
    }
}
