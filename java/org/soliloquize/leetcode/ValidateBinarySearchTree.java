package org.soliloquize.leetcode;

import org.soliloquize.leetcode.common.TreeNode;

public class ValidateBinarySearchTree {
    /**
     http://leetcode.com/onlinejudge#question_98

     Given a binary tree, determine if it is a valid binary search tree (BST).

     Assume a BST is defined as follows:

     The left subtree of a node contains only nodes with keys less than the node's key.
     The right subtree of a node contains only nodes with keys greater than the node's key.
     Both the left and right subtrees must also be binary search trees.
     confused what "{1,#,2,3}" means? > read more on how binary tree is serialized on OJ.
     */

    public static void main(String[] args) {
        TreeNode root1 = new ConvertSortedArrayToBinarySearchTree().sortedArrayToBST(new int[] {1, 2, 3, 4, 5, 6, 7});
        TreeNode root2 = new ConvertSortedArrayToBinarySearchTree().sortedArrayToBST(new int[] {7, 6, 5, 4, 3, 2, 1});
        ValidateBinarySearchTree s = new ValidateBinarySearchTree();
        System.out.println(s.isValidBST(root1));
        System.out.println(s.isValidBST(root2));
        System.out.println("->");
        System.out.println(s.isValidBST2(root1));
        System.out.println(s.isValidBST2(root2));
    }

    public boolean isValidBST2(TreeNode root) {
        if (root == null) {
            return false;
        }
        isValid = true;
        search(root);
        return isValid;
    }

    private Pair search(TreeNode node) {
        int min = node.val;
        int max = node.val;
        if (node.left == null && node.right == null) {
            return new Pair(min, max);
        }
        if (node.left != null) {
            Pair left = search(node.left);
            if (left.max >= node.val) {
                isValid = false;
            }
            min = left.min;
        }
        if (node.right != null) {
            Pair right = search(node.right);
            if (right.min <= node.val) {
                isValid = false;
            }
            max = right.max;
        }
        return new Pair(min, max);
    }


    public class Pair {
        int min;
        int max;
        public Pair(int min, int max) {
            this.min = min;
            this.max = max;
        }
    }

    private boolean isValid;

    public boolean isValidBST(TreeNode root) {
        if (root == null) {
            return true;
        }
        isValid = true;
        travel(root);
        return isValid;
    }


    /**
     * 左子树的最大值需要小于右子树最小值
     *
     * 每次返回子树的最大最小值进行判定
     *
     * 另外一种方法是中序遍历后判断是否是有序的
     */
    private Pair travel(TreeNode root) {
        if (root.left == null && root.right == null) {
            return new Pair(root.val, root.val);
        }
        Pair pair = null;
        if (root.left != null) {
            Pair left = travel(root.left);
            if (left.max >= root.val) {
                isValid = false;
            }
            left.max = root.val;
            pair = left;
        }
        if (!isValid) {
            return pair;
        }
        if (root.right != null) {
            Pair right = travel(root.right);
            if (right.min <= root.val) {
                isValid = false;
            }
            right.min = root.val;

            if (pair == null) {
                pair = right;
            } else {
                pair.max = right.max;
            }
        }
        return pair;
    }
}