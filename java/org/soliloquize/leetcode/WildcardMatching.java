package org.soliloquize.leetcode;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;

public class WildcardMatching {
    /**
     http://oj.leetcode.com/problems/wildcard-matching/

     Implement wildcard pattern matching with support for '?' and '*'.

     '?' Matches any single character.
     '*' Matches any sequence of characters (including the empty sequence).

     The matching should cover the entire input string (not partial).

     The function prototype should be:
     bool isMatch(const char *s, const char *p)

     Some examples:
     isMatch("aa","a") → false
     isMatch("aa","aa") → true
     isMatch("aaa","aa") → false
     isMatch("aa", "*") → true
     isMatch("aa", "a*") → true
     isMatch("ab", "?*") → true
     isMatch("aab", "c*a*b") → false
     */

    public static void main(String[] args) throws Exception {
        WildcardMatching s = new WildcardMatching();
        System.out.println(s.isMatch("aa", "a"));
    }

    public boolean isMatch(String s, String p) {
        return isMatch(s, 0, p, 0);
    }

    private boolean isMatch(String s, int sindex, String p, int pindex) {
        if (sindex == s.length()) {
            if (pindex == p.length()) {
                return true;
            }
            for (int i = pindex; i < p.length(); i++) {
                if (p.charAt(i) != '*') {
                    return false;
                }
            }
            return true;
        }
        if (sindex != s.length() && pindex == p.length()) {
            return false;
        }

        boolean result;
        char c = p.charAt(pindex);
        if (c == '*') {
            result = isMatch(s, sindex + 1, p, pindex + 1) || isMatch(s, sindex + 1, p, pindex) || isMatch(s, sindex, p, pindex + 1);
        } else {
            if (c == '?' || c == s.charAt(sindex)) {
                result = isMatch(s, sindex + 1, p, pindex + 1);
            } else {
                result = false;
            }
        }
        return result;
    }
}
