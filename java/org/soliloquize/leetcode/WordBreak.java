package org.soliloquize.leetcode;

import java.util.HashSet;
import java.util.Set;

public class WordBreak {
    /**
     http://oj.leetcode.com/problems/word-break/

     Given a string s and a dictionary of words dict, determine if s can be segmented into a space-separated sequence of one or more dictionary words.

     For example, given
     s = "leetcode",
     dict = ["leet", "code"].

     Return true because "leetcode" can be segmented as "leet code".
     */
    public static void main(String[] args) {
        WordBreak s = new WordBreak();
        Set<String> set = new HashSet<String>();
        set.add("leet");
        set.add("code");
        System.out.println(s.wordBreak("leetcode", set));
        System.out.println(s.wordBreak2("leetcode", set));
    }

    public boolean wordBreak2(String s, Set<String> dict) {
        if (s.length() == 0) {
            return false;
        }
        char[] chars = s.toCharArray();
        boolean[] p = new boolean[s.length()];
        for (int i = 0; i < p.length; i++) {
            p[i] = dict.contains(new String(chars, 0, i+1));
            if (p[i]) {
                continue;
            }
            for (int j = i - 1; j >= 0; j--) {
                if (p[j] && dict.contains(new String(chars, j+1, i - j))) {
                    p[i] = true;
                    break;
                }
            }
        }
        return p[s.length()-1];
    }


    public boolean wordBreak(String s, Set<String> dict) {
        if (escapse(s, dict)) {
            return false;
        }
        return travel(s, 0, dict, new StringBuilder());
    }

    private boolean travel(String s, int index, Set<String> dict, StringBuilder current) {
        if (index >= s.length()) {
            return true;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = index; i < s.length(); i++) {
            builder.append(s.charAt(i));
            String tmp = builder.toString();
            if (dict.contains(tmp)) {
                if (current.length() == 0) {
                    boolean flag = travel(s, i + 1, dict, current.append(tmp));
                    if (flag) {
                        return flag;
                    }
                    current.delete(current.length()-tmp.length(), current.length());
                } else {
                    boolean flag = travel(s, i + 1, dict, current.append(" ").append(tmp));
                    if (flag) {
                        return flag;
                    }
                    current.delete(current.length()-tmp.length()-" ".length(), current.length());
                }
            }
        }
        return false;
    }

    private boolean escapse(String s, Set<String> dict) {
        Set<Character> set = new HashSet<Character>();
        for (String str : dict) {
            for (int i = 0; i < str.length(); i++) {
                set.add(str.charAt(i));
            }
        }
        for (int i = 0; i < s.length(); i++) {
            if (!set.contains(s.charAt(i))) {
                return true;
            }
        }
        return false;
    }
}
