package org.soliloquize.leetcode;

import java.util.*;

public class WordBreakII {
    /**
     http://oj.leetcode.com/problems/word-break-ii/

     Given a string s and a dictionary of words dict, add spaces in s to construct a sentence where each word is a valid dictionary word.

     Return all such possible sentences.

     For example, given
     s = "catsanddog",
     dict = ["cat", "cats", "and", "sand", "dog"].

     A solution is ["cats and dog", "cat sand dog"].
     */
    public ArrayList<String> wordBreak(String s, Set<String> dict) {
        if (escapse(s, dict)) {
            return new ArrayList<String>();
        }
        ArrayList<String> result = new ArrayList<String>();
        travel(s, 0, dict, new StringBuilder(), result);
        return result;
    }

    private void travel(String s, int index, Set<String> dict, StringBuilder current, ArrayList<String> result) {
        if (index >= s.length()) {
            result.add(current.toString());
            return;
        }
        StringBuilder builder = new StringBuilder();
        for (int i = index; i < s.length(); i++) {
            builder.append(s.charAt(i));
            String tmp = builder.toString();
            if (dict.contains(tmp)) {
                if (current.length() == 0) {
                    travel(s, i + 1, dict, current.append(tmp), result);
                    current.delete(current.length() - tmp.length(), current.length());
                } else {
                    travel(s, i + 1, dict, current.append(" ").append(tmp), result);
                    current.delete(current.length() - tmp.length() - " ".length(), current.length());
                }
            }
        }
    }

    private boolean escapse(String s, Set<String> dict) {
        Set<Character> set = new HashSet<Character>();
        for (String str : dict) {
            for (int i = 0; i < str.length(); i++) {
                set.add(str.charAt(i));
            }
        }
        for (int i = 0; i < s.length(); i++) {
            if (!set.contains(s.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<String> wordBreak2(String s, Set<String> dict) {
        ArrayList<String> result = new ArrayList<String>();
        search(dict, s.toCharArray(), 0, new StringBuilder(), result);
        return result;
    }

    private void search(Set<String> dict, char[] chars, int index, StringBuilder current, ArrayList<String> result) {
        if (index == chars.length) {
            result.add(current.toString().trim());
            return;
        }
        int length = current.length();
        StringBuilder tmp = new StringBuilder();
        for (int i = index; i < chars.length; i++) {
            tmp.append(chars[i]);
            if (!dict.contains(tmp.toString())) {
                continue;
            }
            search(dict, chars, i + 1, current.append(" " + tmp.toString()), result);
            current.delete(length, current.length());
        }
    }


    public static void main(String[] args) throws Exception {
        String str = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab";
        Set<String> dict = new HashSet<String>(Arrays.asList("a","aa","aaa","aaaa","aaaaa","aaaaaa","aaaaaaa","aaaaaaaa","aaaaaaaaa","aaaaaaaaaa"));
        WordBreakII s = new WordBreakII();

        str = "leetcode";
        dict = new HashSet<String>(Arrays.asList("leet", "code"));
        System.out.println(s.wordBreak(str, dict));
        System.out.println(s.wordBreak2(str, dict));
    }
}
