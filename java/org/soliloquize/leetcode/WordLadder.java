package org.soliloquize.leetcode;

import java.util.HashSet;
import java.util.LinkedList;

public class WordLadder {
    /**
     http://leetcode.com/oldoj#question_127

     Given two words (start and end), and a dictionary, find the length of shortest transformation sequence from start to end, such that:

     •Only one letter can be changed at a time
     •Each intermediate word must exist in the dictionary
     For example,

     Given:
     start = "hit"
     end = "cog"
     dict = ["hot","dot","dog","lot","log"]


     As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
     return its length 5.

     Note:


     •Return 0 if there is no such transformation sequence.
     •All words have the same length.
     •All words contain only lowercase alphabetic characters.
     */

    public class Item {
        char[] c;
        int total;

        public Item(char[] c, int total) {
            this.c = c;
            this.total = total;
        }
    }

    public int ladderLength(String start, String end, HashSet<String> dict) {
        LinkedList<Item> deque = new LinkedList<Item>();
        deque.add(new Item(start.toCharArray(), 1));
        while (deque.size() > 0) {
            Item item = deque.poll();
            for (int i = 0; i < item.c.length; i++) {
                // 枚举可能的变化，在dict元素很多时效率更高
                for (char a = 'a'; a <= 'z'; a++) {
                    char t = item.c[i];
                    if (a == item.c[i]) {
                        continue;
                    }
                    item.c[i] = a;
                    StringBuilder builder = new StringBuilder();
                    for (int j = 0; j < item.c.length; j++) {
                        builder.append(item.c[j]);
                    }
                    String tmp = builder.toString();
                    if (tmp.equals(end)) {
                        return item.total + 1;
                    }
                    if (dict.contains(tmp)) {
                        deque.add(new Item(tmp.toCharArray(), item.total + 1));
                        // 使用过之后的词从dict中移出，保证一个词只使用一次
                        dict.remove(tmp);
                    }
                    item.c[i] = t;
                 }
            }
        }
        return 0;
    }
}
