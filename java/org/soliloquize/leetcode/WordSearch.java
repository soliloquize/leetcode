package org.soliloquize.leetcode;

public class WordSearch {
    /**
     http://oj.leetcode.com/problems/word-search/

     Given a 2D board and a word, find if the word exists in the grid.

     The word can be constructed from letters of sequentially adjacent cell, where "adjacent" cells are those horizontally or vertically neighboring. The same letter cell may not be used more than once.

     For example,
     Given board =

     [
     ["ABCE"],
     ["SFCS"],
     ["ADEE"]
     ]
     word = "ABCCED", -> returns true,
     word = "SEE", -> returns true,
     word = "ABCB", -> returns false.
     */
    public boolean exist(char[][] board, String word) {
        if (board.length == 0) {
            return false;
        }
        if (word.length() == 0) {
            return false;
        }
        boolean[][] visited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == word.charAt(0)) {
                    boolean flag = search(visited, board, i, j, word, 0);
                    if (flag) {
                        return flag;
                    }
                }
            }
        }
        return false;
    }

    private boolean search(boolean[][] visited, char[][] board, int x, int y, String word, int index) {
        if (index >= word.length()) {
            return true;
        }
        if (board[x][y] != word.charAt(index)) {
            return false;
        }
        visited[x][y] = true;
        if (x - 1 >= 0 && !visited[x-1][y]) {
            boolean flag = search(visited, board, x - 1, y, word, index + 1);
            if (flag) {
                return true;
            }
        }
        if (x + 1 < board.length&& !visited[x+1][y]) {
            boolean flag = search(visited, board, x + 1, y, word, index + 1);
            if (flag) {
                return true;
            }
        }
        if (y - 1 >= 0 && !visited[x][y-1]) {
            boolean flag = search(visited, board, x, y - 1, word, index + 1);
            if (flag) {
                return true;
            }
        }
        if (y + 1 < board[0].length && !visited[x][y+1]) {
            boolean flag = search(visited, board, x, y + 1, word, index + 1);
            if (flag) {
                return true;
            }
        }
        visited[x][y] = false;
        return index == word.length() - 1;
    }

    public boolean exist2(char[][] board, String word) {
        if (board.length == 0 || word.length() == 0) {
            return false;
        }
        boolean[][] visited = new boolean[board.length][board[0].length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == word.charAt(0)) {
                    boolean flag = search(board, visited, i, j, word, 0);
                    if (flag) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean search(char[][] board, boolean[][] visited, int x, int y, String word, int index) {
        if (index >= word.length()) {
            return true;
        }
        if ((x < 0 || x >= board.length) || (y < 0 || y >= board[0].length) || visited[x][y]) {
            return false;
        }
        if (word.charAt(index) != board[x][y]) {
            return false;
        }
        visited[x][y] = true;

        boolean flag = search(board, visited, x - 1, y, word, index + 1);
        if (flag) {
            return true;
        }

        flag = search(board, visited, x, y - 1, word, index + 1);
        if (flag) {
            return true;
        }

        flag = search(board, visited, x, y + 1, word, index + 1);
        if (flag) {
            return true;
        }

        flag = search(board, visited, x + 1, y, word, index + 1);
        if (flag) {
            return true;
        }
        visited[x][y] = false;
        return false;
    }


    public static void main(String[] args) {
        WordSearch s = new WordSearch();
        char[][] board = new char[][] {
          new char[] {'a', 'a'},
        };
        System.out.println(s.exist(board, "aa"));
        System.out.println(s.exist2(board, "aa"));

        board = new char[][] {
            "ABCE".toCharArray(),
            "SFCS".toCharArray(),
            "ADEE".toCharArray(),
        };

        System.out.println(s.exist(board, "ABCCED"));
        System.out.println(s.exist(board, "SEE"));
        System.out.println(s.exist(board, "ABCB"));
        System.out.println(s.exist2(board, "ABCCED"));
        System.out.println(s.exist2(board, "SEE"));
        System.out.println(s.exist2(board, "ABCB"));
    }
}
