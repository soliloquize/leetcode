package org.soliloquize.leetcode;

import java.util.Arrays;

public class ZigZagConversion {

    /**
     http://leetcode.com/onlinejudge#question_6

     The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
     (you may want to display this pattern in a fixed font for better legibility)

     P   A   H   N
     A P L S I I G
     Y   I   R
     And then read line by line: "PAHNAPLSIIGYIR"
     Write the code that will take a string and make this conversion given a number of rows:

     string convert(string text, int nRows);
     convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
     */
    public static void main(String[] args) {
        ZigZagConversion s = new ZigZagConversion();
        System.out.println(s.convert("PAYPALISHIRING", 1));
        System.out.println(s.convert("PAYPALISHIRING", 2));
        System.out.println(s.convert("PAYPALISHIRING", 3));
        System.out.println(s.convert("PAYPALISHIRING", 4));

        System.out.println("->");
        System.out.println(s.convert2("PAYPALISHIRING", 1));
        System.out.println(s.convert2("PAYPALISHIRING", 2));
        System.out.println(s.convert2("PAYPALISHIRING", 3));
        System.out.println(s.convert2("PAYPALISHIRING", 4));

    }

    public String convert2(String s, int nRows) {
        if (nRows == 1) {
            return s;
        }
        char[] chars = new char[s.length()];
        int index = 0;
        for (int i = 0; i < nRows; i++) {
            for (int j = i; j < chars.length; j += nRows * 2 - 2) {
                chars[index++] = s.charAt(j);
                if (i > 0 && i < nRows - 1) {
                    int next = j + nRows * 2 - 2 - i * 2;
                    if (next < chars.length) {
                        chars[index++] = s.charAt(next);
                    }
                }
            }
        }
        return new String(chars);
    }

    public String convert(String s, int nRows) {
        if (nRows == 1) {
            return s;
        }
        char[][] matrix = new char[nRows][s.length()];
        for (int i = 0; i < matrix.length; i++) {
            Arrays.fill(matrix[i], ' ');
        }
        int status = 0;
        int i = 0;
        int j = 0;
        for (int k = 0; k < s.length(); ) {
            if (status == 0) {
                for (; i < nRows && k < s.length(); i++, k++) {
                    matrix[i][j] = s.charAt(k);
                }
                status = 1;
                i -= 2;
                j += 1;
            } else if (status == 1) {
                for (; i >= 0 && j < s.length() && k < s.length(); i--, j++, k++) {
                    matrix[i][j] = s.charAt(k);
                }
                status = 0;
                i += 2;
            }
        }
        StringBuilder builder = new StringBuilder();
        for (i = 0; i < nRows; i++) {
            for (j = 0; j < s.length(); j++) {
                if (matrix[i][j] != ' ') {
                    builder.append(matrix[i][j]);
                }
            }
        }
        return builder.toString();
    }
}
