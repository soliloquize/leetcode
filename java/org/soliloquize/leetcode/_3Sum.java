package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class _3Sum {

    /**
     http://leetcode.com/onlinejudge#question_15

     Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? Find all unique
     triplets in the array which gives the sum of zero.

     Note:

     Elements in a triplet (a,b,c) must be in non-descending order. (ie, a ? b ? c)
     The solution set must not contain duplicate triplets.
     For example, given array S = {-1 0 1 2 -1 -4},

     A solution set is:
     (-1, 0, 1)
     (-1, -1, 2)
     */

    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {
        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

        Arrays.sort(num);
        for (int k = 0; k < num.length; k++) {
            int target = 0 - num[k];
            for (int i = k + 1, j = num.length-1; i < j; ) {
                int sum = num[i] + num[j];
                if (sum == target) {
                    result.add(new ArrayList<Integer>(Arrays.asList(num[k], num[i], num[j])));
                    while (i+1 < num.length && num[i+1] == num[i]) {
                        i += 1;
                    }
                    while (j-1 >= 0 && num[j-1] == num[j]) {
                        j -= 1;
                    }
                    i += 1;
                    j -= 1;
                } else if (sum < target) {
                    i += 1;
                } else {
                    j -= 1;
                }
            }
            while (k+1 < num.length && num[k+1] == num[k]) {
                k += 1;
            }
        }

        return result;
    }


    public static void main(String[] args) {
        _3Sum s = new _3Sum();
        System.out.println(s.threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
        System.out.println(s.threeSum(new int[]{-1, -1, 0, 1, 1, 2, 2, -1, -4}));
    }
}
