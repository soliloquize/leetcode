package org.soliloquize.leetcode;

import java.util.Arrays;

public class _3SumClosest {
    /**
     http://oj.leetcode.com/problems/3sum-closest/

     Given an array S of n integers, find three integers in S such that the sum is closest to a given number, target. Return the sum of the three integers. You may assume that each input would have exactly one solution.

     For example, given array S = {-1 2 1 -4}, and target = 1.

     The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
     */

    public int threeSumClosest2(int[] num, int target) {
        Arrays.sort(num);

        int res = 0;
        int closet = Integer.MAX_VALUE;
        for (int k = 0; k < num.length; k++) {
            int left = target - num[k];
            for (int i = k + 1, j = num.length-1; i < j; ) {
                int tmp = num[i] + num[j];
                int diff = Math.abs(tmp + num[k] - target);
                if (diff < closet) {
                    closet = diff;
                    res = tmp + num[k];
                }
                if (tmp == left) {
                    return target;
                } else if (tmp < left) {
                    i += 1;
                } else {
                    j -= 1;
                }
            }
        }
        return res;
    }

    public int threeSumClosest(int[] num, int target) {
        Arrays.sort(num);

        int result = 0;
        int closest = Integer.MAX_VALUE;
        for (int i = 0; i < num.length; i++) {
            int left = target - num[i];
            for (int x = i + 1, y = num.length - 1; x < y; ) {
                int tmp = num[x] + num[y];
                if (tmp == left) {
                    return num[i] + tmp;
                }
                int diff = Math.abs(target - (num[i] + num[x] + num[y]));
                if (closest > diff) {
                    result = num[i] + tmp;
                    closest = diff;
                }
                if (tmp < left) {
                    x += 1;
                } else if (tmp > left) {
                    y -= 1;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        _3SumClosest s = new _3SumClosest();
        System.out.println(s.threeSumClosest(new int[]{1, 1, 1, 0}, 100));
        System.out.println(s.threeSumClosest2(new int[]{1, 1, 1, 0}, 100));
    }
}
