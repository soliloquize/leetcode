package org.soliloquize.leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class _4Sum {
    /**
     http://leetcode.com/onlinejudge#question_18

     Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
     Find all unique quadruplets in the array which gives the sum of target.

     Note:

     Elements in a quadruplet (a,b,c,d) must be in non-descending order. (ie, a ? b ? c ? d)
     The solution set must not contain duplicate quadruplets.
     For example, given array S = {1 0 -1 0 -2 2}, and target = 0.

     A solution set is:
     (-1,  0, 0, 1)
     (-2, -1, 1, 2)
     (-2,  0, 0, 2)
     */
    public ArrayList<ArrayList<Integer>> fourSum2(int[] num, int target) {
        Arrays.sort(num);

        ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < num.length; ) {
            int a = num[i];
            int target3 = target - a;

            for (int j = i + 1; j < num.length; ) {
                int b = num[j];
                int target2 = target3 - b;

                for (int x = j + 1, y = num.length - 1; x < y; ) {
                    int c = num[x];
                    int d = num[y];

                    int tmp = c + d;
                    if (tmp == target2) {
                        res.add(new ArrayList<Integer>(Arrays.asList(a, b, c, d)));
                        while (x < num.length && num[x] == c) {
                            x += 1;
                        }
                        while (y >= 0 && num[y] == d) {
                            y -= 1;
                        }
                    } else if (tmp < target2) {
                        x += 1;
                    } else {
                        y -= 1;
                    }
                }

                while (j < num.length && num[j] == b) {
                    j += 1;
                }
            }

            while (i < num.length && num[i] == a) {
                i += 1;
            }
        }
        return res;
    }


    public ArrayList<ArrayList<Integer>> fourSum(int[] num, int target) {
        Arrays.sort(num);

        ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
        for (int i = 0; i < num.length; i++) {
            int left1 = target - num[i];
            if (result.size() > 0) {
                if (num[i] == result.get(result.size()-1).get(0)) {
                    continue;
                }
            }
            for (int j = i + 1; j < num.length; j++) {
                int left2 = left1 - num[j];
                if (result.size() > 0) {
                    if (num[i] == result.get(result.size()-1).get(0) && num[j] == result.get(result.size()-1).get(1)) {
                        continue;
                    }
                }
                for (int x = j + 1, y = num.length-1; x < y; ) {
                    int tmp = num[x] + num[y];
                    if (tmp > left2) {
                        y -= 1;
                    } else if (tmp < left2) {
                        x += 1;
                    } else {
                        ArrayList<Integer> item = new ArrayList<Integer>(Arrays.asList(new Integer[] {num[i], num[j], num[x], num[y]}));
                        if (result.size() == 0) {
                            result.add(item);
                        } else {
                            ArrayList<Integer> last = result.get(result.size()-1);
                            boolean flag = false;
                            for (int k = 0; k < item.size(); k++) {
                                int a = last.get(k);
                                int b = item.get(k);
                                if (a != b) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (flag) {
                                result.add(item);
                            }
                        }
                        x += 1;
                    }
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        _4Sum s = new _4Sum();

//        ArrayList<ArrayList<Integer>> res = s.fourSum(new int[] {-5,-4,-3,-2,-1,0,0,1,2,3,4,5}, 0);
        System.out.println(s.fourSum(new int[] {-495,-477,-464,-424,-411,-409,-363,-337,-328,-328,-325,-320,-310,-285,-278,-235,-208,-151,-149,-147,-144,-132,-115,-107,-101,-98,-83,-58,-58,-56,-51,-46,-45,-7,0,4,4,21,51,52,57,60,104,109,124,141,158,205,206,241,278,278,291,314,379,416,437,447,452,496}, -1371));
        System.out.println(s.fourSum2(new int[] {-495,-477,-464,-424,-411,-409,-363,-337,-328,-328,-325,-320,-310,-285,-278,-235,-208,-151,-149,-147,-144,-132,-115,-107,-101,-98,-83,-58,-58,-56,-51,-46,-45,-7,0,4,4,21,51,52,57,60,104,109,124,141,158,205,206,241,278,278,291,314,379,416,437,447,452,496}, -1371));

    }
}
