package org.soliloquize.leetcode.common;

import org.soliloquize.leetcode.BinaryTreeLevelOrderTraversal;

import java.util.ArrayDeque;
import java.util.Deque;

public class Constructor {


    /***
     * 创建二叉树，输入格式为[0, 1, 2, ...], MIN_VALUE代表空值
     *
     * @param numbers
     * @return
     */
    public static TreeNode buildTree(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            return null;
        }
        TreeNode root = new TreeNode(numbers[0]);
        Deque<TreeNode> deque = new ArrayDeque<TreeNode>();
        deque.add(root);
        int index = 1;
        while (!deque.isEmpty()) {
            TreeNode p = deque.poll();
            if (index < numbers.length && numbers[index] != Integer.MIN_VALUE) {
                p.left = new TreeNode(numbers[index]);
                deque.add(p.left);
            }
            index += 1;
            if (index < numbers.length && numbers[index] != Integer.MIN_VALUE) {
                p.right = new TreeNode(numbers[index]);
                deque.add(p.right);
            }
            index += 1;
        }
        return root;
    }


    public static void main(String[] args) {
        int [] numbers = new int[] {5, 4, 8, 11, Integer.MIN_VALUE, 13, 4, 7, 2, Integer.MIN_VALUE, 1};
        TreeNode node = Constructor.buildTree(numbers);
        BinaryTreeLevelOrderTraversal s = new BinaryTreeLevelOrderTraversal();
        System.out.println(s.levelOrder(node));
    }
}
