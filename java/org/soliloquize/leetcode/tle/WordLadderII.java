package org.soliloquize.leetcode.tle;

import java.util.*;

public class WordLadderII {
    /**
     http://leetcode.com/oldoj#question_126

     Given two words (start and end), and a dictionary, find all shortest transformation sequence(s) from start to end, such that:

     •Only one letter can be changed at a time
     •Each intermediate word must exist in the dictionary
     For example,

     Given:
     start = "hit"
     end = "cog"
     dict = ["hot","dot","dog","lot","log"]

     Return

     [
     ["hit","hot","dot","dog","cog"],
     ["hit","hot","lot","log","cog"]
     ]

     Note:

     •All words have the same length.
     •All words contain only lowercase alphabetic characters.
     */

    public class Item implements Comparable<Item> {

        String c;
        int length;
        Item previous;


        public Item(String c) {
            this.c = c;
            previous = null;
            length = 1;
        }

        public Item(String c, Item previous) {
            this.c = c;
            this.previous = previous;
            this.length = previous.length + 1;
        }

        @Override
        public int compareTo(Item o) {
            return length - o.length;
        }

        public String last() {
            return c;
        }

        public ArrayList<String> toStringPath() {
            ArrayList<String> res = new ArrayList<String>();
            res.add(c);
            Item item = previous;
            while (item != null) {
                res.add(item.c);
                item = item.previous;
            }
            Collections.reverse(res);
            return res;
        }

        public int length() {
            return length;
        }
    }

    public ArrayList<ArrayList<String>> findLadders(String start, String end, HashSet<String> dict) {
        ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
        dict.add(start);
        dict.add(end);

        LinkedList<Item> queue = new LinkedList<Item>();
        queue.add(new Item(start));
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put(start, 0);
        int length = -1;
        while (queue.size() > 0) {
            Item item = queue.poll();

            // 找到一种可能
            if (item.last().equals(end)) {
                ArrayList<String> path = item.toStringPath();
                result.add(path);

                // 更新最短路的长度
                if (length == -1) {
                    length = path.size();
                }
                continue;
            }

            // 已经超过了最短路径
            if (length != -1 && item.length() >= length) {
                continue;
            }

            StringBuilder builder = new StringBuilder(item.last());
            for (int i = 0; i < builder.length(); i++) {
                char old = item.last().charAt(i);
                for (char k = 'a'; k < 'z'; k++) {
                    if (k == old) {
                        continue;
                    }
                    builder.setCharAt(i, k);
                    String tmp = builder.toString();
                    if (!dict.contains(tmp)) {
                        continue;
                    }
                    if (!map.containsKey(tmp) || (map.get(tmp) >= item.length() + 1)) {
                        queue.add(new Item(tmp, item));
                        map.put(tmp, item.length() + 1);
                    }
                }
                builder.setCharAt(i, old);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Map<Integer, Integer> a = new HashMap<Integer, Integer>();
        a.put(1, 2);
        System.out.println(a.containsKey(1));
        WordLadderII s = new WordLadderII();
        HashSet<String> dict = new HashSet<String>();
        dict.addAll(Arrays.asList("hot","dot","dog","lot","log"));
        System.out.println(s.findLadders("hit", "cog", dict));
        List<String> list = Arrays.asList("kid","tag","pup","ail","tun","woo","erg","luz","brr","gay","sip","kay","per","val","mes","ohs","now","boa","cet","pal","bar","die","war","hay","eco","pub","lob","rue","fry","lit","rex","jan","cot","bid","ali","pay","col","gum","ger","row","won","dan","rum","fad","tut","sag","yip","sui","ark","has","zip","fez","own","ump","dis","ads","max","jaw","out","btu","ana","gap","cry","led","abe","box","ore","pig","fie","toy","fat","cal","lie","noh","sew","ono","tam","flu","mgm","ply","awe","pry","tit","tie","yet","too","tax","jim","san","pan","map","ski","ova","wed","non","wac","nut","why","bye","lye","oct","old","fin","feb","chi","sap","owl","log","tod","dot","bow","fob","for","joe","ivy","fan","age","fax","hip","jib","mel","hus","sob","ifs","tab","ara","dab","jag","jar","arm","lot","tom","sax","tex","yum","pei","wen","wry","ire","irk","far","mew","wit","doe","gas","rte","ian","pot","ask","wag","hag","amy","nag","ron","soy","gin","don","tug","fay","vic","boo","nam","ave","buy","sop","but","orb","fen","paw","his","sub","bob","yea","oft","inn","rod","yam","pew","web","hod","hun","gyp","wei","wis","rob","gad","pie","mon","dog","bib","rub","ere","dig","era","cat","fox","bee","mod","day","apr","vie","nev","jam","pam","new","aye","ani","and","ibm","yap","can","pyx","tar","kin","fog","hum","pip","cup","dye","lyx","jog","nun","par","wan","fey","bus","oak","bad","ats","set","qom","vat","eat","pus","rev","axe","ion","six","ila","lao","mom","mas","pro","few","opt","poe","art","ash","oar","cap","lop","may","shy","rid","bat","sum","rim","fee","bmw","sky","maj","hue","thy","ava","rap","den","fla","auk","cox","ibo","hey","saw","vim","sec","ltd","you","its","tat","dew","eva","tog","ram","let","see","zit","maw","nix","ate","gig","rep","owe","ind","hog","eve","sam","zoo","any","dow","cod","bed","vet","ham","sis","hex","via","fir","nod","mao","aug","mum","hoe","bah","hal","keg","hew","zed","tow","gog","ass","dem","who","bet","gos","son","ear","spy","kit","boy","due","sen","oaf","mix","hep","fur","ada","bin","nil","mia","ewe","hit","fix","sad","rib","eye","hop","haw","wax","mid","tad","ken","wad","rye","pap","bog","gut","ito","woe","our","ado","sin","mad","ray","hon","roy","dip","hen","iva","lug","asp","hui","yak","bay","poi","yep","bun","try","lad","elm","nat","wyo","gym","dug","toe","dee","wig","sly","rip","geo","cog","pas","zen","odd","nan","lay","pod","fit","hem","joy","bum","rio","yon","dec","leg","put","sue","dim","pet","yaw","nub","bit","bur","sid","sun","oil","red","doc","moe","caw","eel","dix","cub","end","gem","off","yew","hug","pop","tub","sgt","lid","pun","ton","sol","din","yup","jab","pea","bug","gag","mil","jig","hub","low","did","tin","get","gte","sox","lei","mig","fig","lon","use","ban","flo","nov","jut","bag","mir","sty","lap","two","ins","con","ant","net","tux","ode","stu","mug","cad","nap","gun","fop","tot","sow","sal","sic","ted","wot","del","imp","cob","way","ann","tan","mci","job","wet","ism","err","him","all","pad","hah","hie","aim","ike","jed","ego","mac","baa","min","com","ill","was","cab","ago","ina","big","ilk","gal","tap","duh","ola","ran","lab","top","gob","hot","ora","tia","kip","han","met","hut","she","sac","fed","goo","tee","ell","not","act","gil","rut","ala","ape","rig","cid","god","duo","lin","aid","gel","awl","lag","elf","liz","ref","aha","fib","oho","tho","her","nor","ace","adz","fun","ned","coo","win","tao","coy","van","man","pit","guy","foe","hid","mai","sup","jay","hob","mow","jot","are","pol","arc","lax","aft","alb","len","air","pug","pox","vow","got","meg","zoe","amp","ale","bud","gee","pin","dun","pat","ten","mob");
        System.out.println(list.size());
        dict.addAll(list);
        System.out.println(s.findLadders("cet", "ism", dict));
//        List<Integer> a = new ArrayList<Integer>();
//        a.add(1);
//        System.out.println(a.contains(1));

//        PriorityQueue<Integer> a = new PriorityQueue<Integer>();
//        a.add(1);
//        a.add(2);
//        System.out.println(a.poll());
    }
}
