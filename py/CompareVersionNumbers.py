# -*- encoding:utf-8 -*-

# https://oj.leetcode.com/problems/compare-version-numbers/
#
# Compare two version numbers version1 and version1.
# If version1 > version2 return 1, if version1 < version2 return -1, otherwise return 0.
#
# You may assume that the version strings are non-empty and contain only digits and the . character.
# The . character does not represent a decimal point and is used to separate number sequences.
# For instance, 2.5 is not "two and a half" or "half way to version three", it is the fifth second-level revision of the second first-level revision.
#
# Here is an example of version numbers ordering:
#
# 0.1 < 1.1 < 1.2 < 13.37

class Solution:
    def compareVersion(self, version1, version2):
        nums0 = [int(x) for x in version1.split('.')]
        nums1 = [int(x) for x in version2.split('.')]

        len0 = len(nums0)
        len1 = len(nums1)
        x = 0
        y = 0
        while x < len0 and y < len1:
            diff = nums0[x] - nums1[y]
            if diff > 0:
                return 1
            elif diff < 0:
                return -1
            else:
                x += 1
                y += 1

        left1 = self.sum_left(nums0, x)
        left2 = self.sum_left(nums1, y)
        if left1 > left2:
            return 1
        elif left1 < left2:
            return -1
        else:
            return 0

    def sum_left(self, nums, index):
        return sum(nums[index:])


def main():
    s = Solution()
    print s.compareVersion("1.0", "1")
    print s.compareVersion("1", "1.1")
    print s.compareVersion("0.1", "1.1")
    print s.compareVersion("1.1", "1.2")
    print s.compareVersion("13.37", "1.2")


if __name__ == '__main__':
    main()