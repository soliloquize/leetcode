# -*- encoding:utf-8 -*-

# https://oj.leetcode.com/problems/excel-sheet-column-number/
#
# Given a column title as appear in an Excel sheet, return its corresponding column number.
#
# For example:
#
#     A -> 1
#     B -> 2
#     C -> 3
#     ...
#     Z -> 26
#     AA -> 27
#     AB -> 28


class Solution:
    def titleToNumber(self, s):
        result = 0
        for x in s:
            result *= 26
            result += ord(x) - 65 + 1
        return result


def main():
    s = Solution()
    print s.titleToNumber('AB')
    print s.titleToNumber('B')
    print s.titleToNumber('AZ')


if __name__ == '__main__':
    main()