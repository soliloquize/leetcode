# -*- encoding:utf-8 -*-

# https://oj.leetcode.com/problems/excel-sheet-column-title/
#
# Given a positive integer, return its corresponding column title as appear in an Excel sheet.
#
# For example:
#
#     1 -> A
#     2 -> B
#     3 -> C
#     ...
#     26 -> Z
#     27 -> AA
#     28 -> AB


class Solution:
    def convertToTitle(self, num):
        result = []
        while num > 0:
            remainder = num % 26
            result.append(remainder)
            num /= 26
        for idx, x in enumerate(result):
            if x != 0:
                continue
            if idx + 1 < len(result):
                result[idx] = 26
                result[idx+1] -= 1
        result = [x for x in result if x > 0]
        result.reverse()
        result = [chr(x + 65 - 1) for x in result]
        return ''.join(result)


def main():
    s = Solution()
    assert s.convertToTitle(702) == 'ZZ'
    assert s.convertToTitle(701) == 'ZY'
    assert s.convertToTitle(26) == 'Z'
    assert s.convertToTitle(52) == 'AZ'
    assert s.convertToTitle(53) == 'BA'
    assert s.convertToTitle(1) == 'A'


if __name__ == '__main__':
    main()