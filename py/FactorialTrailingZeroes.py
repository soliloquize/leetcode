# -*- encoding:utf-8 -*-

# https://oj.leetcode.com/problems/factorial-trailing-zeroes/
#
# Given an integer n, return the number of trailing zeroes in n!.
#
# Note: Your solution should be in logarithmic time complexity.

import math


class Solution:
    def trailingZeroes(self, n):
        result = 0
        x = 1
        while True:
            factor = int(math.pow(5, x))
            result += n / factor
            if factor > n:
                break
            x += 1
        return result


def main():
    s = Solution()
    print s.trailingZeroes(1)
    print s.trailingZeroes(10)
    print s.trailingZeroes(100)


if __name__ == '__main__':
    main()