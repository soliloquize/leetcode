# -*- encoding:utf-8 -*-

# https://oj.leetcode.com/problems/majority-element/
#
# Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
#
# You may assume that the array is non-empty and the majority element always exist in the array.


class Solution:
    def majorityElement(self, nums):
        num = 0
        count = 0
        for x in nums:
            if count == 0:
                num = x
            if num == x:
                count += 1
            else:
                count -= 1
        return num


def main():
    s = Solution()
    print s.majorityElement([1, 1, 2, 2, 2, 3, 3, 2, 2])


if __name__ == '__main__':
    main()